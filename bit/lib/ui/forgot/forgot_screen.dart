import 'package:flutter/material.dart';
import 'package:localbit/ui/forgot/forgot_card.dart';
import 'package:localbit/widgets/common_background.dart';
import 'package:localbit/widgets/logo_app.dart';
import 'package:localbit/widgets/toolbar.dart';

class ForgotPassScreen extends StatefulWidget {
  ForgotPassScreen({Key key}) : super(key: key);

  _ForgotPassScreenState createState() => _ForgotPassScreenState();
}

class _ForgotPassScreenState extends State<ForgotPassScreen> {
  ForgotCard forgotCard;
  @override
  void initState() {
    // TODO: implement initState
    super.initState();
    forgotCard = ForgotCard();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      resizeToAvoidBottomInset: false,
      body: Container(
        child: Stack(
          children: <Widget>[_buildBackground(), _buildBodyContent()],
        ),
      ),
    );
  }

  Widget _buildBackground() {
    return LoadingBackground();
  }

  Widget _buildBodyContent() {
    return SingleChildScrollView(
      child: ConstrainedBox(
        constraints: BoxConstraints(),
        child: Column(
          children: <Widget>[
            CustomizeAppbar(),
            LogoApp(),
            SizedBox(
              height: 40,
            ),
            forgotCard
          ],
        ),
      ),
    );
  }
}
