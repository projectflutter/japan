import 'dart:async';

import 'package:flutter/material.dart';
import 'package:localbit/%08models/forgot_request.dart';
import 'package:localbit/%08models/forgot_response.dart';
import 'package:localbit/blocs/forgot_bloc.dart';
import 'package:localbit/datasources/network/network_service_response.dart';
import 'package:localbit/utils/api_subscription.dart';
import 'package:localbit/utils/common_dialogs.dart';
import 'package:localbit/utils/constant.dart';
import 'package:localbit/utils/fetch_process.dart';
import 'package:localbit/utils/uidata.dart';
import 'package:localbit/widgets/button.dart';
import 'package:localbit/widgets/button_circle_close.dart';
import 'package:localbit/widgets/card.dart';

class ForgotCard extends StatefulWidget {
  ForgotCard({Key key}) : super(key: key);

  _ForgotCardState createState() => _ForgotCardState();
}

class _ForgotCardState extends State<ForgotCard> {
  ForgotPasswordBloc _forgotPasswordBloc;
  String email;
  bool isValidateEmail = false;
  final _emailController = TextEditingController();
  StreamSubscription<FetchProcess> apiStreamSubscription;

  @override
  void initState() {
    super.initState();
    _forgotPasswordBloc = ForgotPasswordBloc();
     apiStreamSubscription = apiSubscription(_forgotPasswordBloc.apiResult, context);
  }

  @override
  Widget build(BuildContext context) {
    return CardCS(
      form: Column(
        children: <Widget>[ButtonClose(), loginForm()],
      ),
    );
  }

  Widget loginForm() {
    return StreamBuilder(
        stream: _forgotPasswordBloc.forgotPassStream,
        builder: (context, snapshot) {
          if (snapshot.hasData && snapshot.data.status != -1) {
            handleResults(snapshot.data);
          }
          return Padding(
            padding: EdgeInsets.only(left: 30.0, right: 30.0),
            child: Column(
              children: <Widget>[
                Center(
                  child: Text(
                    "Forgot Password",
                    style: TextStyle(
                        color: Colors.white,
                        fontSize: 18.0,
                        fontWeight: FontWeight.bold),
                  ),
                ),
                SizedBox(height: 20.0),
                Align(
                  alignment: Alignment.centerLeft,
                  child: Text(
                    "Email",
                    style: TextStyle(color: Colors.white),
                  ),
                ),
                SizedBox(height: 10.0),
                Column(
                  crossAxisAlignment: CrossAxisAlignment.center,
                  mainAxisAlignment: MainAxisAlignment.center,
                  children: <Widget>[
                    TextField(
                      controller: _emailController,
                      decoration: InputDecoration(
                        contentPadding: const EdgeInsets.all(10.0),
                        hintStyle: TextStyle(color: Colors.blue),
                        border: OutlineInputBorder(
                            borderRadius: BorderRadius.circular(3.0),
                            borderSide: BorderSide.none),
                        filled: true,
                        fillColor: Colors.white.withOpacity(1),
                        errorText: isValidateEmail ? 'Email not empty!' : null,
                      ),
                    ),
                  ],
                ),
                SizedBox(height: 10.0),
                Button(
                  color: UIData.colorYellow(),
                  onPress: () {
                    _onPressForgotPassword();
                  },
                  text: "Submit",
                ),
              ],
            ),
          );
        });
  }

  void _onPressForgotPassword() {
    if (_emailController.text.isNotEmpty) {
      ForgotPassRequest forgotPassRequest = ForgotPassRequest();
      forgotPassRequest.email = _emailController.text;
      _forgotPasswordBloc.callLogin(forgotPassRequest);
    }
    {
      ForgotPassResponse temp = ForgotPassResponse();
      temp.status = -1;
      _forgotPasswordBloc.forgotPassController.add(temp);
      setState(() {
        _emailController.text.isEmpty
            ? isValidateEmail = true
            : isValidateEmail = false;
      });
    }
  }

  void handleResults(ForgotPassResponse data) {
    if (data.status == Constant.SUCESS_CODE) {
      WidgetsBinding.instance.addPostFrameCallback((_) => goToHome(context));
    } else {
      WidgetsBinding.instance
          .addPostFrameCallback((_) => showErrorLogin(data, context));
    }
  }

  goToHome(BuildContext context) {
    Navigator.pushNamedAndRemoveUntil(
        context, '/home', (Route<dynamic> route) => false);
  }

  showErrorLogin(ForgotPassResponse data, BuildContext context) {
    fetchApiResult(
        context, NetworkServiceResponse(success: true, message: data.message));
  }

  @override
  void dispose() {
    super.dispose();
    _emailController?.dispose();
    _forgotPasswordBloc?.dispose();
    apiStreamSubscription?.cancel();
  }
}
