import 'dart:async';

import 'package:flutter/material.dart';
import 'package:localbit/%08models/confirm_transfer.dart';
import 'package:localbit/%08models/fiats_response.dart';
import 'package:localbit/%08models/recipient_response.dart';
import 'package:localbit/%08models/transfer_response.dart';
import 'package:localbit/blocs/transfer_bloc.dart';
import 'package:localbit/ui/parent_provider.dart';
import 'package:localbit/ui/transfer/view_provider.dart';
import 'package:localbit/ui/transfer/widgets/data_amount.dart';
import 'package:localbit/ui/transfer/widgets/enum_page.dart';
import 'package:localbit/ui/transfer/widgets/transfer_amount_page.dart';
import 'package:localbit/ui/transfer/widgets/transfer_init_page.dart';
import 'package:localbit/ui/transfer/widgets/transfer_otp_page.dart';
import 'package:localbit/ui/transfer/widgets/transfer_success_page.dart';
import 'package:localbit/utils/api_subscription.dart';
import 'package:localbit/utils/constant.dart';
import 'package:localbit/utils/fetch_process.dart';

class TransferScreen extends StatefulWidget {
  TransferScreen({Key key}) : super(key: key);

  _TransferScreenState createState() => _TransferScreenState();
}

class _TransferScreenState extends State<TransferScreen>
    with AutomaticKeepAliveClientMixin<TransferScreen> {
  TransferBloc _transferBloc;
  //databackup
  Fiats _currentFiats;
  TransferConfirmation _transferConfirmation;
  DataAmount _dataAmount;
  ConfirmTransferResponse _confirmResponse;
  TRANSFERRESULT _resultType;
  String _titleContentResult = "";
  List<Recipients> _listRecipient;
  int _recipientId;
  StreamSubscription<FetchProcess> apiStreamSubscription;
  @override
  void initState() {
    super.initState();
    _transferBloc = TransferBloc();
    apiStreamSubscription = apiSubscription(_transferBloc.apiResult, context);
  }

  @override
  Widget build(BuildContext context) {
    print('setstateTransfer');
    return ViewTransferProvider(
      nextpage: _onHandleNextPage,
      goToAddfund: _onHandleAddfund,
      goToHome: _onHandleGoToHome,
      child: Container(
        child: StreamBuilder(
            stream: _transferBloc.getNavigation,
            initialData: _transferBloc.navigationProvider.currentNavigation,
            builder: (context, snapshot) {
              if (_transferBloc.navigationProvider.currentNavigation ==
                  TransferPageEnum.PAGE_INIT) {
                return TransferInItPage(
                  fiats: _currentFiats,
                  dataAmount: _dataAmount,
                  transferBloc: _transferBloc,
                );
              }
              if (_transferBloc.navigationProvider.currentNavigation ==
                  TransferPageEnum.PAGE_AMOUNT) {
                return TransferAmountPage(
                  fiats: _currentFiats,
                  transferBloc: _transferBloc,
                  dataAmount: _dataAmount,
                  listRecipient: _listRecipient,
                );
              }
              if (_transferBloc.navigationProvider.currentNavigation ==
                  TransferPageEnum.PAGE_OTP) {
                return TransferOTPPage(
                  transferConfirmation: _transferConfirmation,
                  dataAmount: _dataAmount,
                  transferBloc: _transferBloc,
                  listRecipient: _listRecipient,
                );
              }
              if (_transferBloc.navigationProvider.currentNavigation ==
                  TransferPageEnum.PAGE_RESULT) {
                return TransferResultPage(
                  transferResultPage: _resultType,
                  textContent: _titleContentResult,
                  recipientId: _recipientId,
                  transferBloc: _transferBloc,
                  parrentContext: context,
                  
                );
              }
            }),
      ),
    );
  }

  @override
  // TODO: implement wantKeepAlive
  bool get wantKeepAlive => false;

  void _onHandleNextPage(
      TransferPageEnum enumPage,
      Fiats fiats,
      TransferConfirmation transferResponse,
      DataAmount dataAmount,
      ConfirmTransferResponse confirmResponse,
      List<Recipients> listRecipient,
      int recipientId) {
    if (enumPage == TransferPageEnum.PAGE_INIT) {
      _currentFiats = fiats;
      _dataAmount = dataAmount;
    } else if (enumPage == TransferPageEnum.PAGE_AMOUNT) {
      _currentFiats = fiats;
      _dataAmount = dataAmount;
      _listRecipient = listRecipient;
    } else if (enumPage == TransferPageEnum.PAGE_OTP) {
      _transferConfirmation = transferResponse;
      _dataAmount = dataAmount;
      _listRecipient = listRecipient;
    } else if (enumPage == TransferPageEnum.PAGE_RESULT) {
      _confirmResponse = confirmResponse;
      if (_confirmResponse.status == Constant.SUCESS_CODE) {
        //handle with result
        _resultType = TRANSFERRESULT.SUCCESS;
        _titleContentResult = _confirmResponse.message;
        _recipientId = recipientId;
      } else {
        _resultType = TRANSFERRESULT.ERROR;
        _titleContentResult = _confirmResponse.message;
      }
    }
    _transferBloc.updateNavigation(enumPage);
  }

  void _onHandleAddfund() {
    ParentProvider.of(context).listener();
  }

  void _onHandleGoToHome() {
    ParentProvider.of(context).reloadwallet();
  }

  @override
  void dispose() {
    super.dispose();
    apiStreamSubscription?.cancel();
  }
}
