import 'package:flutter/material.dart';
import 'package:localbit/%08models/confirm_transfer.dart';
import 'package:localbit/%08models/fiats_response.dart';
import 'package:localbit/%08models/recipient_response.dart';
import 'package:localbit/%08models/transfer_response.dart';
import 'package:localbit/ui/transfer/widgets/data_amount.dart';
import 'package:localbit/ui/transfer/widgets/enum_page.dart';

class ViewTransferProvider extends InheritedWidget {
  final Function nextpage;
  final Function goToAddfund;
  final Function goToHome;
  ViewTransferProvider({
    this.nextpage(
        TransferPageEnum enumPage,
        Fiats fiats,
        TransferConfirmation transferResponse,
        DataAmount dataAmount,
        ConfirmTransferResponse confirmResponse,
        List<Recipients> listRecipient,
        int recipientId),
    Key key,
    this.child,
    this.goToAddfund,
    this.goToHome,
  }) : super(key: key, child: child);

  final Widget child;

  static ViewTransferProvider of(BuildContext context) {
    return (context.inheritFromWidgetOfExactType(ViewTransferProvider)
        as ViewTransferProvider);
  }

  @override
  bool updateShouldNotify(ViewTransferProvider oldWidget) {
    return true;
  }
}
