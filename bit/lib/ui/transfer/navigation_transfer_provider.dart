import 'package:flutter/material.dart';
import 'package:localbit/ui/transfer/widgets/enum_page.dart';

class NavigationTransferProvider extends InheritedWidget {
  TransferPageEnum currentNavigation;
  NavigationTransferProvider(
      {Key key,
      this.child,
      this.currentNavigation = TransferPageEnum.PAGE_INIT})
      : super(key: key, child: child);

  final Widget child;

  static NavigationTransferProvider of(BuildContext context) {
    return (context.inheritFromWidgetOfExactType(NavigationTransferProvider)
        as NavigationTransferProvider);
  }

  @override
  bool updateShouldNotify(NavigationTransferProvider oldWidget) {
    return true;
  }

  void updateNavigation(TransferPageEnum navigation) {
    currentNavigation = navigation;
  }
}
