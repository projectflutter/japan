import 'package:localbit/%08models/fiats_response.dart';
import 'package:localbit/%08models/transfer_request.dart';

class DataAmount{
  TransferRequest transferRequest;
  int radioValue;
  Fiats fiats;
  DataAmount({this.transferRequest,this.fiats,this.radioValue});
}