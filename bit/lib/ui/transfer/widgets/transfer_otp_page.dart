import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:localbit/%08models/confirm_transfer.dart';
import 'package:localbit/%08models/key_value_model.dart';
import 'package:localbit/%08models/recipient_response.dart';
import 'package:localbit/%08models/transfer_response.dart';
import 'package:localbit/blocs/transfer_bloc.dart';
import 'package:localbit/datasources/network/network_service_response.dart';
import 'package:localbit/ui/exchange/widget/button.dart';
import 'package:localbit/ui/transfer/widgets/data_amount.dart';
import 'package:localbit/utils/common_dialogs.dart';
import 'package:localbit/utils/constant.dart';
import 'package:localbit/utils/uidata.dart';
import 'package:localbit/widgets/card.dart';
import 'package:localbit/widgets/text_header.dart';

import '../view_provider.dart';
import 'enum_page.dart';

class TransferOTPPage extends StatefulWidget {
  final TransferConfirmation transferConfirmation;
  final DataAmount dataAmount;
  final TransferBloc transferBloc;
  final List<Recipients> listRecipient;
  TransferOTPPage(
      {Key key,
      @required this.transferConfirmation,
      this.dataAmount,
      @required this.transferBloc,
      this.listRecipient})
      : super(key: key);

  _TransferOTPPageState createState() => _TransferOTPPageState();
}

class _TransferOTPPageState extends State<TransferOTPPage> {
  List<KeyValueModel> list;
  String _otp = '';
  @override
  void initState() {
    // TODO: implement initState
    super.initState();
    list = [];
    list.add(KeyValueModel(
        tilteName: 'Account',
        value: '${widget.transferConfirmation.account}',
        isBoldText: false));
    list.add(KeyValueModel(
        tilteName: 'Name',
        value: '${widget.transferConfirmation.name}'.toUpperCase(),
        isBoldText: true));
    list.add(KeyValueModel(
        tilteName: 'Amount',
        value: '${widget.transferConfirmation.amount}',
        isBoldText: false));
    list.add(KeyValueModel(
        tilteName: 'Currency',
        value: '${widget.transferConfirmation.currency}'.toUpperCase(),
        isBoldText: false));
    list.add(KeyValueModel(
        tilteName: 'Fee',
        value: '${widget.transferConfirmation.fee}',
        isBoldText: false));
  }

  @override
  Widget build(BuildContext context) {
    return SingleChildScrollView(
      child: CardCS(
          form: Column(
        children: <Widget>[
          _buildFormTransfer(),
          _buildBody(),
          _buildFieldOTP(),
          _buttonNext()
        ],
      )),
    );
  }

  Widget _buildFormTransfer() {
    return Container(
      padding: EdgeInsets.all(4.0),
      child: Row(
        mainAxisAlignment: MainAxisAlignment.spaceBetween,
        children: <Widget>[
          TextHeader(
            text: "Transfer",
          ),
        ],
      ),
    );
  }

  Widget _buildBody() {
    return Container(
      height: 200,
      child: ListView.builder(
        itemBuilder: (BuildContext context, int index) {
          return _buildItemList(list[index]);
        },
        itemCount: list.length,
      ),
    );
  }

  Widget _buildItemList(KeyValueModel itemlist) {
    return Padding(
      padding: const EdgeInsets.all(8.0),
      child: Row(
        children: <Widget>[
          Container(
            margin: EdgeInsets.only(left: 20),
            width: 100,
            child: Text(
              itemlist.tilteName,
              style: TextStyle(
                  color: Colors.white,
                  fontWeight:
                      itemlist.isBoldText ? FontWeight.bold : FontWeight.normal,
                  fontSize: 12),
            ),
          ),
          Container(
            margin: EdgeInsets.only(right: 20),
            child: Text(
              itemlist.value,
              style: TextStyle(
                  color: Colors.white,
                  fontWeight:
                      itemlist.isBoldText ? FontWeight.bold : FontWeight.normal,
                  fontSize: 12),
            ),
          )
        ],
      ),
    );
  }

  Widget _buildFieldOTP() {
    return Container(
      padding: const EdgeInsets.all(8.0),
      child: Row(
        mainAxisAlignment: MainAxisAlignment.spaceBetween,
        children: <Widget>[
          Container(
            margin: EdgeInsets.only(left: 20),
            width: 100,
            child: Text(
              'OTP',
              style: TextStyle(
                  color: Colors.white,
                  fontWeight: FontWeight.bold,
                  fontSize: 12),
            ),
          ),
          Expanded(
            child: Container(
                margin: EdgeInsets.only(right: 20),
                child: TextField(
                  inputFormatters: [
                    WhitelistingTextInputFormatter.digitsOnly,
                    LengthLimitingTextInputFormatter(6), //max 6 digits with otp
                  ],
                  keyboardType: TextInputType.number,
                  // controller: controller,
                  onChanged: (value) => _otp = value,
                  decoration: InputDecoration(
                    contentPadding: const EdgeInsets.all(10.0),
                    hintStyle: TextStyle(color: Colors.blue),
                    border: OutlineInputBorder(
                        borderRadius: BorderRadius.circular(3.0),
                        borderSide: BorderSide.none),
                    filled: true,
                    fillColor: Colors.white.withOpacity(1),
                    hintText: "",
                  ),
                )),
          )
        ],
      ),
    );
  }

  Widget _buttonNext() {
    return StreamBuilder<ConfirmTransferResponse>(
        stream: widget.transferBloc.confirmStream,
        builder: (context, snapshot) {
          if (snapshot.hasData) {
            if (snapshot.data.status == Constant.SUCESS_CODE) {
              ViewTransferProvider.of(context).nextpage(
                  TransferPageEnum.PAGE_RESULT,
                  null,
                  null,
                  null,
                  snapshot.data,
                  widget.listRecipient,
                  snapshot.data.data.recipientId);
            } else {
              WidgetsBinding.instance.addPostFrameCallback(
                  (_) => showErrorDialog(snapshot.data, context));
              // ViewTransferProvider.of(context).nextpage(
              //     TransferPageEnum.PAGE_RESULT,
              //     null,
              //     null,
              //     null,
              //     snapshot.data,
              //     widget.listRecipient,
              //     null);
            }
          }
          return Row(
            crossAxisAlignment: CrossAxisAlignment.center,
            mainAxisAlignment: MainAxisAlignment.center,
            children: <Widget>[
              ButtonBorder(
                color: Colors.white,
                onPress: () {
                  _gotoBack(TransferPageEnum.PAGE_AMOUNT);
                },
                text: "Back",
                textColor: Colors.blue,
              ),
              ButtonBorder(
                color: UIData.colorItemBottom3(),
                onPress: () {
                  _gotoStep(TransferPageEnum.PAGE_RESULT);
                },
                text: "Next",
              ),
            ],
          );
        });
  }

  void _gotoBack(TransferPageEnum pageEnum) {
    ViewTransferProvider.of(context).nextpage(pageEnum, null, null,
        widget.dataAmount, null, widget.listRecipient, null);
  }

  showErrorDialog(ConfirmTransferResponse data, BuildContext context) {
    fetchApiResult(
        context, NetworkServiceResponse(success: true, message: data.message));
    widget.transferBloc.confirmTransferController.add(null);
  }

  void _gotoStep(TransferPageEnum pageEnum) {
    print('transferid ${widget.transferConfirmation.transferId}');
    print('OTP $_otp');
    widget.transferBloc
        .confirmTransfer(widget.transferConfirmation.transferId, _otp);
  }
}
