import 'package:flutter/material.dart';
import 'package:localbit/%08models/recipient_response.dart';

typedef OnGetValue(Recipients value);

class DropdownRecipient extends StatefulWidget {
  final OnGetValue onGetValue;
  String hintText;
  List<Recipients> listRecipient;
  DropdownRecipient({
    Key key,
    this.onGetValue,
    this.hintText,
    this.listRecipient,
  }) : super(key: key);

  _DropdownRecipientState createState() => _DropdownRecipientState();
}

class _DropdownRecipientState extends State<DropdownRecipient> {
  Color selectedColor;
  int _currentIndex = -1;
  @override
  void initState() {
    super.initState();
    if (widget.hintText == '') {
      widget.hintText = 'Choose recipient available';
    } else {}
  }

  @override
  Widget build(BuildContext context) {
    Widget _itemListDropDown(Recipients recipients, Color colorText) {
      return Container(
        child: new Row(
          crossAxisAlignment: CrossAxisAlignment.center,
          mainAxisAlignment: MainAxisAlignment.center,
          children: <Widget>[
            Container(
              padding: const EdgeInsets.only(left: 8.0),
              child: Text(
                recipients != null ? recipients.recipientEmail : widget.hintText,
                style: TextStyle(color: colorText),
              ),
            )
          ],
        ),
      );
    }

    return Container(
      height: 100,
      margin: EdgeInsets.only(bottom: 1),
      width: double.infinity,
      decoration: BoxDecoration(
        color: Colors.white,
        borderRadius: BorderRadius.circular(3.0),
      ),
      child: DropdownButtonHideUnderline(
        child: ButtonTheme(  alignedDropdown: true,
            child: DropdownButton( isDense: true,
          style: TextStyle(color: Colors.white),
          hint: _itemListDropDown(
              widget.listRecipient.length > 0 && _currentIndex != -1
                  ? widget.listRecipient[_currentIndex]
                  : null,
              Colors.black),
          iconEnabledColor: Colors.black,
          items: widget.listRecipient.map((Recipients recipients) {
            return DropdownMenuItem(
                value: recipients,
                child: _itemListDropDown(recipients, Colors.black));
          }).toList(),
          onChanged: (Recipients value) {
            for (var i = 0; i < widget.listRecipient.length; i++) {
              if (value == widget.listRecipient[i]) {
                setState(() {
                  _currentIndex = i;
                  widget.onGetValue(widget.listRecipient[i]);
                });
                return;
              }
            }
          },
        )),
      ),
    );
  }
}
