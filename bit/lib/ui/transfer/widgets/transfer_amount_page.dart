import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:localbit/%08models/fiats_response.dart';
import 'package:localbit/%08models/recipient_response.dart';
import 'package:localbit/%08models/transfer_request.dart';
import 'package:localbit/%08models/transfer_response.dart';
import 'package:localbit/blocs/transfer_bloc.dart';
import 'package:localbit/datasources/network/network_service_response.dart';
import 'package:localbit/ui/exchange/widget/button.dart';
import 'package:localbit/utils/common_dialogs.dart';
import 'package:localbit/utils/constant.dart';
import 'package:localbit/utils/uidata.dart';
import 'package:localbit/widgets/card.dart';
import 'package:localbit/widgets/text_header.dart';

import '../view_provider.dart';
import 'data_amount.dart';
import 'dropdown_recipient.dart';
import 'enum_page.dart';

class TransferAmountPage extends StatefulWidget {
  Fiats fiats;
  final TransferBloc transferBloc;
  final DataAmount dataAmount;
  final List<Recipients> listRecipient;
  TransferAmountPage(
      {Key key,
      @required this.fiats,
      @required this.transferBloc,
      @required this.dataAmount,
      this.listRecipient})
      : super(key: key);

  _TransferAmountPageState createState() => _TransferAmountPageState();
}

class _TransferAmountPageState extends State<TransferAmountPage> {
  int _radioValue = 0;
  String _recipient = "";
  double _amount = 0;
  String hintTextDropDown = '';
  TransferRequest transferRequest;

  var amountController = TextEditingController();
  var recipientController = TextEditingController();
  @override
  void initState() {
    super.initState();
    if (widget.dataAmount != null) {
      //reload data
      _radioValue = widget.dataAmount.radioValue;
      if (_radioValue == 0) {
        recipientController.text = widget.dataAmount.transferRequest.recipient;
      } else {
        hintTextDropDown = widget.dataAmount.transferRequest.recipient;
      }
      _recipient = widget.dataAmount.transferRequest.recipient;
      widget.fiats = widget.dataAmount.fiats;
      amountController.text = '${widget.dataAmount.transferRequest.amount}';
      _amount = widget.dataAmount.transferRequest.amount;
    }
  }

  @override
  Widget build(BuildContext context) {
    return SingleChildScrollView(
      child: CardCS(
          form: Column(
        children: <Widget>[
          _buildFormTransfer(),
          _buildBody(),
          _buttonNext(),
          SizedBox(
            height: 10,
          ),
        ],
      )),
    );
  }

  Widget _buildFormTransfer() {
    return Container(
      padding: EdgeInsets.all(4.0),
      child: Row(
        mainAxisAlignment: MainAxisAlignment.spaceBetween,
        children: <Widget>[
          TextHeader(
            text: "Transfer",
          ),
        ],
      ),
    );
  }

  Widget _buildBody() {
    return Container(
      child: Column(
        children: <Widget>[_buildGroupRadioButton(), _buildAmount()],
      ),
    );
  }

  Widget _buildGroupRadioButton() {
    return Theme(
      data: ThemeData.dark(),
      child: Row(
        mainAxisAlignment: MainAxisAlignment.center,
        children: <Widget>[
          new Radio(
            activeColor: Colors.blue,
            value: 0,
            groupValue: _radioValue,
            onChanged: _handleRadioValueChange,
          ),
          InkWell(
            onTap: () => _handleRadioValueChange(0),
            child: new Text(
              'New Recipient',
              style: new TextStyle(fontSize: 16.0, color: Colors.white),
            ),
          ),
          new Radio(
            activeColor: Colors.blue,
            value: 1,
            groupValue: _radioValue,
            onChanged: _handleRadioValueChange,
          ),
          InkWell(
            onTap: () => _handleRadioValueChange(1),
            child: new Text(
              'List Available',
              style: new TextStyle(fontSize: 16.0, color: Colors.white),
            ),
          ),
        ],
      ),
    );
  }

  Widget _buildAmount() {
    return Container( margin: EdgeInsets.symmetric(horizontal: 20),
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        children: <Widget>[
          Text(
            'Recipient',
            style: TextStyle(color: Colors.white),
          ),
          SizedBox(
            height: 10,
          ),
          _radioValue == 0
              ? Container(
                  height: 40,
                  child: TextField(
                    controller: recipientController,
                    onChanged: (value) {
                      _recipient = value;
                    },
                    decoration: InputDecoration(
                      contentPadding: const EdgeInsets.all(10.0),
                      hintStyle: TextStyle(color: Colors.blue),
                      border: OutlineInputBorder(
                          borderRadius: BorderRadius.circular(3.0),
                          gapPadding: 10,
                          borderSide: BorderSide.none),
                      filled: true,
                      fillColor: Colors.white.withOpacity(1),
                      hintText: "",
                    ),
                  ))
              : DropdownRecipient(
                  listRecipient: widget.listRecipient,
                  hintText: hintTextDropDown,
                  onGetValue: (value) {
                    _recipient = value.recipientEmail;
                  },
                ),
          SizedBox(
            height: 10,
          ),
          Text(
            'Amount',
            style: TextStyle(color: Colors.white),
          ),
          SizedBox(
            height: 10,
          ),
          TextField(
            inputFormatters: [WhitelistingTextInputFormatter.digitsOnly],
            controller: amountController,
            onChanged: (value) {
              _amount = double.parse(value);
            },
            decoration: InputDecoration(
              contentPadding: const EdgeInsets.all(10.0),
              hintStyle: TextStyle(color: Colors.blue),
              border: OutlineInputBorder(
                  borderRadius: BorderRadius.circular(3.0),
                  borderSide: BorderSide.none),
              filled: true,
              fillColor: Colors.white.withOpacity(1),
            ),
            keyboardType: TextInputType.number,
          ),
          SizedBox(
            height: 10,
          ),
        ],
      ),
    );
  }

  Widget _buttonNext() {
    return StreamBuilder<TransferResponse>(
        stream: widget.transferBloc.createStream,
        builder: (context, snapshot) {
          if (snapshot.hasData) {
            if (snapshot.data.status == Constant.SUCESS_CODE) {
              ViewTransferProvider.of(context).nextpage(
                  TransferPageEnum.PAGE_OTP,
                  null,
                  snapshot.data.data.transferConfirmation,
                  DataAmount(
                      radioValue: _radioValue,
                      transferRequest: transferRequest,
                      fiats: widget.fiats),
                  null,
                  widget.listRecipient,
                  null);
            } else if (snapshot.data.status != -1) {
              WidgetsBinding.instance.addPostFrameCallback(
                  (_) => showErrorDialog(snapshot.data, context));
            }
          }
          return Row(
            crossAxisAlignment: CrossAxisAlignment.center,
            mainAxisAlignment: MainAxisAlignment.center,
            children: <Widget>[
              ButtonBorder(
                color: Colors.white,
                onPress: () {
                  _gotoBack(TransferPageEnum.PAGE_INIT);
                },
                text: "Back",
                textColor: Colors.blue,
              ),
              ButtonBorder(
                color: UIData.colorItemBottom3(),
                onPress: () {
                  _gotoStep(TransferPageEnum.PAGE_OTP);
                },
                text: "Next",
              ),
            ],
          );
        });
  }

  void _gotoBack(TransferPageEnum pageEnum) {
    ViewTransferProvider.of(context).nextpage(
        pageEnum, widget.fiats, null, widget.dataAmount, null, null, null);
  }

  void _gotoStep(TransferPageEnum pageEnum) {
    transferRequest = TransferRequest(
        recipient: _recipient, amount: _amount, symbol: widget.fiats.name);
    print(transferRequest.toJson());
    widget.transferBloc.createTransfer(transferRequest);
  }

  void _handleRadioValueChange(int value) {
    TransferResponse temp = TransferResponse();
    temp.status = -1;
    widget.transferBloc.createTransferController.add(temp);
    setState(() {
      _radioValue = value;
    });
  }

  showErrorDialog(TransferResponse data, BuildContext context) {
    fetchApiResult(
        context, NetworkServiceResponse(success: true, message: data.message));
        widget.transferBloc.createTransferController.add(null);
  }
}
