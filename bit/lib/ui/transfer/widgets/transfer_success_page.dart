import 'package:flutter/material.dart';
import 'package:flutter/widgets.dart';
import 'package:localbit/%08models/base_response.dart';
import 'package:localbit/blocs/transfer_bloc.dart';
import 'package:localbit/ui/exchange/widget/button.dart';
import 'package:localbit/utils/constant.dart';
import 'package:localbit/utils/uidata.dart';
import 'package:localbit/widgets/card.dart';
import 'package:localbit/widgets/text_header.dart';

import '../view_provider.dart';

enum TRANSFERRESULT { SUCCESS, ERROR }

class TransferResultPage extends StatefulWidget {
  final TRANSFERRESULT transferResultPage;
  final String textContent;
  final int recipientId;
  final TransferBloc transferBloc;
  final BuildContext parrentContext;

  TransferResultPage(
      {Key key,
      @required this.transferResultPage,
      @required this.textContent,
      this.recipientId,
      this.transferBloc,
      this.parrentContext})
      : super(key: key);

  _TransferResultPageState createState() => _TransferResultPageState();
}

class _TransferResultPageState extends State<TransferResultPage> {
  @override
  Widget build(BuildContext context) {
    return SingleChildScrollView(
      child: CardCS(
          form: Column(
        children: <Widget>[_buildFormTransfer(), _buildBody(), _buttonNext()],
      )),
    );
  }

  Widget _buildFormTransfer() {
    return Container(
      padding: EdgeInsets.all(4.0),
      child: Row(
        mainAxisAlignment: MainAxisAlignment.spaceBetween,
        children: <Widget>[
          TextHeader(
            text: "Transfer",
          ),
        ],
      ),
    );
  }

  Widget _buildBody() {
    return Container(
      child: Column(
        children: <Widget>[
          Row(
            mainAxisAlignment: MainAxisAlignment.center,
            children: <Widget>[
              Image.asset(
                widget.transferResultPage == TRANSFERRESULT.SUCCESS
                    ? 'assets/images/ic_success.png'
                    : 'assets/images/ic_error.png',
                scale: 4,
              ),
              Container(
                  margin: EdgeInsets.symmetric(horizontal: 10),
                  child: Text(
                    widget.transferResultPage == TRANSFERRESULT.SUCCESS
                        ? 'Success'.toUpperCase()
                        : "Failed".toUpperCase(),
                    style: TextStyle(
                        color: Colors.white,
                        fontWeight: FontWeight.bold,
                        fontSize: 18),
                  ))
            ],
          ),
          SizedBox(
            height: 10,
          ),
          Container(
            margin: EdgeInsets.symmetric(horizontal: 10),
            child: Text(
              '${widget.textContent}',
              style: TextStyle(color: Colors.white),
              textAlign: TextAlign.center,
            ),
          ),
          SizedBox(
            height: 10,
          ),
        ],
      ),
    );
  }

  Widget _buttonNext() {
    return Column(
      mainAxisAlignment: MainAxisAlignment.center,
      crossAxisAlignment: CrossAxisAlignment.center,
      children: <Widget>[
        StreamBuilder<BaseResponse>(
            stream: widget.transferBloc.saveRecipientStream,
            builder: (context, snapshot) {
              if (snapshot.hasData) {
                 print("gotoHome");
                    WidgetsBinding.instance.addPostFrameCallback(
                      (_) =>  showDialogSave(snapshot.data, context));
                  
              }
              return widget.transferResultPage == TRANSFERRESULT.SUCCESS
                  ? ButtonBorder(
                      width: 250,
                      color: Colors.white,
                      textColor: UIData.colorYellow(),
                      onPress: () {
                        _saveRecipient();
                      },
                      text: "Lưu vào danh sách Recipients",
                    )
                  : Container();
            }),
        ButtonBorder(
          width: 250,
          iconLeft: Icon(Icons.reply, color: Colors.white),
          color: UIData.colorItemBottom3(),
          onPress: () {
            _gotoStep();
          },
          text: "Home",
        ),
      ],
    );
  }

  void _gotoStep() {
    ViewTransferProvider.of(context).goToHome();
  }

  void _saveRecipient() {
    print(widget.recipientId);
    widget.transferBloc.saveRecipient(widget.recipientId);
  }

  @override
  void didChangeDependencies() {
    super.didChangeDependencies();
    print("didchangeOTP");
  }

  showDialogSave(BaseResponse data, BuildContext context) {
    showDialog(
      context: context,
      builder: (context) => AlertDialog(
        title: Text(UIData.error),
        content: Text(data.message),
        actions: <Widget>[
          FlatButton(
            child: Text(UIData.ok),
            onPressed: () {
              Navigator.of(context).pop();
              _gotoStep();
            },
          )
        ],
      ),
    );
  }
}
