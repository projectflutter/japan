import 'package:flutter/material.dart';
import 'package:localbit/%08models/fiats_response.dart';
import 'package:localbit/%08models/recipient_response.dart';
import 'package:localbit/%08models/wallet_balance_response.dart';
import 'package:localbit/blocs/exchange_bloc.dart';
import 'package:localbit/blocs/generic_bloc_provider.dart';
import 'package:localbit/blocs/home_page_bloc.dart';
import 'package:localbit/blocs/transfer_bloc.dart';
import 'package:localbit/datasources/network/network_service_response.dart';
import 'package:localbit/ui/exchange/widget/button.dart';
import 'package:localbit/ui/transfer/view_provider.dart';
import 'package:localbit/ui/transfer/widgets/data_amount.dart';
import 'package:localbit/ui/transfer/widgets/enum_page.dart';
import 'package:localbit/utils/common_dialogs.dart';
import 'package:localbit/utils/constant.dart';
import 'package:localbit/utils/uidata.dart';
import 'package:localbit/utils/utilmath.dart';
import 'package:localbit/widgets/card.dart';
import 'package:localbit/widgets/combobox.dart';
import 'package:localbit/widgets/text_header.dart';
import 'package:localbit/widgets/text_yellow_wallet.dart';

class TransferInItPage extends StatefulWidget {
  final Fiats fiats;
  final DataAmount dataAmount;
  final TransferBloc transferBloc;
  TransferInItPage({Key key, this.fiats, this.dataAmount, this.transferBloc})
      : super(key: key);

  _TransferInItPageState createState() => _TransferInItPageState();
}

class _TransferInItPageState extends State<TransferInItPage> {
  List<Fiats> listFiat;
  ExchangeBloc _exchangeBloc;
  HomePageBloc _homePageBloc;
  Fiats _currentFiats;
  List<Balance> _currentListBalance;

  Balance _currentBalance;
  @override
  void initState() {
    super.initState();
    _exchangeBloc = ExchangeBloc();
    _homePageBloc = BlocProvider.of(context);
    if (widget.fiats == null) {
      _currentFiats = _homePageBloc.getCurrentFiats();
    } else {
      _currentFiats = widget.fiats;
    }

    _currentListBalance = widget.transferBloc.getListBalance();
    listFiat = _exchangeBloc.getListFiats();
    _currentBalance = getCurrentBalance();
  }

  Balance getCurrentBalance() {
    print(_currentListBalance.length);
    try {
      for (var item in _currentListBalance) {
        if (_currentFiats.name.toUpperCase() == item.symbol.toUpperCase()) {
          return Balance(symbol: _currentFiats.symbol, balance: item.balance);
        }
      }
      return Balance(
          symbol: _currentFiats.symbol,
          balance: _currentListBalance[0].balance);
    } catch (e, stacktrace) {
      print(stacktrace);
    }
    return Balance(balance: 0.000, symbol: '\$');
  }

  @override
  Widget build(BuildContext context) {
    return SingleChildScrollView(
      child: CardCS(
        form: Column(
          children: <Widget>[
            _buildFormTransfer(),
            _buldForm2(),
            _buttonNext(),
          ],
        ),
      ),
    );
  }

  Widget _buildFormTransfer() {
    return Container(
      padding: EdgeInsets.all(4.0),
      child: Row(
        mainAxisAlignment: MainAxisAlignment.spaceBetween,
        children: <Widget>[
          TextHeader(
            text: "Transfer",
          ),
        ],
      ),
    );
  }

  Widget _buttonNext() {
    return StreamBuilder<ListRecipientResponse>(
        stream: widget.transferBloc.getListRecipientStream,
        builder: (context, snapshot) {
          if (snapshot.hasData) {
            if (snapshot.data.status == Constant.SUCESS_CODE) {
              ViewTransferProvider.of(context).nextpage(
                  TransferPageEnum.PAGE_AMOUNT,
                  _currentFiats,
                  null,
                  widget.dataAmount,
                  null,
                  snapshot.data.data.recipients,null);
            } else {
              WidgetsBinding.instance.addPostFrameCallback(
                  (_) => showErrorDialog(snapshot.data, context));
            }
          }
          return Row(
            crossAxisAlignment: CrossAxisAlignment.center,
            mainAxisAlignment: MainAxisAlignment.center,
            children: <Widget>[
              ButtonBorder(
                color: Colors.green,
                onPress: () {
                  _gotoAddfund();
                },
                text: "Add fund",
              ),
              Container(
                child: ButtonBorder(
                  color: UIData.colorItemBottom3(),
                  onPress: () {
                    _gotoStep();
                  },
                  text: "Next",
                ),
              )
            ],
          );
        });
  }

  Widget _buldForm2() {
    return Column(
      children: <Widget>[
        Text('My wallet',
            style: TextStyle(color: Colors.white, fontSize: 18.0)),
        SizedBox(
          height: 10,
        ),
        Combobox(
          listDropdownField: listFiat,
          currentIndex: getCurentFiat(listFiat, _currentFiats),
          onGetValue: (Fiats value) {
            setState(() {
              _currentFiats = value;
              _currentBalance = getCurrentBalance();
            });
          },
        ),
        SizedBox(
          height: 10,
        ),
        Text(
          "Wallet Balance",
          style: TextStyle(color: Colors.white, fontSize: 18.0),
        ),
        SizedBox(
          height: 10,
        ),
        TextYellowWallet(
          text:
              '${_currentBalance.symbol} ${getNumberFormat(_currentBalance.balance)}',
        ),
        SizedBox(
          height: 20,
        ),
      ],
    );
  }

  String getNumberFormat(double price) {
    return formatNumber(price);
  }

  void _gotoStep() {
    widget.transferBloc.getListRecipient();
  }

  @override
  void dispose() {
    super.dispose();
    _exchangeBloc.dispose();
    _homePageBloc.dispose();
  }

  void _gotoAddfund() {
    ViewTransferProvider.of(context).goToAddfund();
  }

  showErrorDialog(ListRecipientResponse data, BuildContext context) {
    fetchApiResult(
        context, NetworkServiceResponse(success: true, message: data.message));
  }
}
