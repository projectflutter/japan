import 'package:flutter/material.dart';

typedef OnGetValue(String value);

class DropdownSymbol extends StatefulWidget {
  final OnGetValue onGetValue;
  String hintText;
  List<String> list;
  DropdownSymbol({
    Key key,
    this.onGetValue,
    this.hintText,
    this.list,
  }) : super(key: key);

  _DropdownSymbolState createState() => _DropdownSymbolState();
}

class _DropdownSymbolState extends State<DropdownSymbol> {
  Color selectedColor;
  int _currentIndex = -1;
  @override
  void initState() {
    super.initState();
    if (widget.hintText == '') {
      widget.hintText = 'Choose recipient available';
    } else {}
  }

  @override
  Widget build(BuildContext context) {
    Widget _itemListDropDown(String recipients, Color colorText) {
      return Container(
        child: new Row(
          crossAxisAlignment: CrossAxisAlignment.center,
          mainAxisAlignment: MainAxisAlignment.center,
          children: <Widget>[
            Container(
              padding: const EdgeInsets.only(left: 8.0),
              child: Text(
                recipients != null ? recipients : widget.hintText,
                style: TextStyle(color: colorText),
              ),
            )
          ],
        ),
      );
    }

    return Container(
      height: 100,
      margin: EdgeInsets.only(bottom: 1),
      width: double.infinity,
      decoration: BoxDecoration(
        color: Colors.white,
        borderRadius: BorderRadius.circular(3.0),
      ),
      child: DropdownButtonHideUnderline(
        child: ButtonTheme( 
            child: DropdownButton( isExpanded:false,
          style: TextStyle(color: Colors.white),
          hint: _itemListDropDown(
              widget.list.length > 0 && _currentIndex != -1
                  ? widget.list[_currentIndex]
                  : null,
              Colors.black),
          iconEnabledColor: Colors.black,
          items: widget.list.map((String recipients) {
            return DropdownMenuItem( 
                value: recipients,
                child: _itemListDropDown(recipients, Colors.black));
          }).toList(),
          onChanged: (String value) {
            for (var i = 0; i < widget.list.length; i++) {
              if (value == widget.list[i]) {
                setState(() {
                  _currentIndex = i;
                  widget.onGetValue(widget.list[i]);
                });
                return;
              }
            }
          },
        )),
      ),
    );
  }
}
