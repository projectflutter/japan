import 'package:flutter/material.dart';
import 'package:localbit/%08models/qrcode_generation_response.dart';

class QRcodeProvider extends InheritedWidget {
  final Function nextpage;
  final Function backpage;

  QRcodeProvider({
    this.backpage,
    this.nextpage(String qrcodeText, QrRaw qrRaw),
    Key key,
    this.child,
  }) : super(key: key, child: child);

  final Widget child;

  static QRcodeProvider of(BuildContext context) {
    return (context.inheritFromWidgetOfExactType(QRcodeProvider)
        as QRcodeProvider);
  }

  @override
  bool updateShouldNotify(QRcodeProvider oldWidget) {
    return true;
  }
}
