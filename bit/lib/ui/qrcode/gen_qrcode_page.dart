import 'dart:async';

import 'package:flutter/material.dart';
import 'package:localbit/%08models/qrcode_generation_request.dart';
import 'package:localbit/%08models/qrcode_generation_response.dart';
import 'package:localbit/blocs/generic_bloc_provider.dart';
import 'package:localbit/blocs/home_page_bloc.dart';
import 'package:localbit/blocs/qrcode_bloc.dart';
import 'package:localbit/datasources/network/network_service_response.dart';
import 'package:localbit/ui/exchange/widget/button.dart';
import 'package:localbit/ui/qrcode/widgets/dropdown_symbol.dart';
import 'package:localbit/ui/qrcode/widgets/qrcode_provider.dart';
import 'package:localbit/utils/api_subscription.dart';
import 'package:localbit/utils/common_dialogs.dart';
import 'package:localbit/utils/constant.dart';
import 'package:localbit/utils/fetch_process.dart';
import 'package:localbit/utils/uidata.dart';
import 'package:localbit/widgets/card.dart';
import 'package:localbit/widgets/text_header.dart';

class GenerationQRCodePage extends StatefulWidget {
  GenerationQRCodePage({Key key}) : super(key: key);

  _GenerationQRCodePageState createState() => _GenerationQRCodePageState();
}

class _GenerationQRCodePageState extends State<GenerationQRCodePage> {
  List<String> listSymbol;
  HomePageBloc _homePageBloc;
  String nameSymbolChoose = '';
  QRCodeBloc _qrCodeBloc;
  final amountController = TextEditingController();
  final descriptionController = TextEditingController();
  StreamSubscription<FetchProcess> apiStreamSubscription;
  StreamSubscription resultStreamSubscription;
  @override
  void initState() {
    super.initState();
    _qrCodeBloc = BlocProvider.of(context);
    _homePageBloc = BlocProvider.of(context);
    listSymbol = _homePageBloc.getListSymbolFiats();
    print('qrcode ${listSymbol.length}');
    apiStreamSubscription = apiSubscription(_qrCodeBloc.apiResult, context);
    listener();
  }

  void listener() {
    resultStreamSubscription = _qrCodeBloc.genCodeStream.listen((onData) {
      if (onData.status == Constant.SUCESS_CODE && onData.data.qrRaw != null) {
        print(onData.data.qrText);
        QRcodeProvider.of(context)
            .nextpage(onData.data.qrText, onData.data.qrRaw);
      } else {
        showValidateDialog(onData.message);
      }
    });
  }

  @override
  Widget build(BuildContext context) {
    return SingleChildScrollView(
        child: CardCS(
      form: Column(
        children: <Widget>[
          TextHeader(
            text: UIData.qrcodetitle,
          ),
          _buildBody()
        ],
      ),
    ));
  }

  Widget _buildBody() {
    return Container(
      margin: EdgeInsets.symmetric(horizontal: 32.0, vertical: 16.0),
      child: Column(
        children: <Widget>[
          _buildDropdown(),
          _buildFormInput(),
          _buildButtonGenerate()
        ],
      ),
    );
  }

  Widget _buildDropdown() {
    return Container(
      child: DropdownSymbol(
        list: listSymbol,
        hintText: "symbol",
        onGetValue: (value) {
          print(value);
          setState(() {
            nameSymbolChoose = value;
          });
        },
      ),
    );
  }

  Widget _buildFormInput() {
    return Container(
      child: Column(
        children: <Widget>[
          SizedBox(
            height: 10,
          ),
          Align(
            alignment: Alignment.centerLeft,
            child: Text(
              'Enter Payment fields',
              style: TextStyle(fontSize: 17, color: Colors.white),
            ),
          ),
          SizedBox(
            height: 10,
          ),
          TextField(
            controller: amountController,
            keyboardType: TextInputType.number,
            style: TextStyle(color: Colors.white),
            decoration: new InputDecoration(
                contentPadding: EdgeInsets.all(10),
                hintText: 'Amount in $nameSymbolChoose',
                hintStyle: TextStyle(
                    color: Colors.white,
                    fontStyle: FontStyle.italic,
                    fontSize: 13),
                enabledBorder: new OutlineInputBorder(
                  borderSide: new BorderSide(color: Colors.white, width: 1.0),
                  borderRadius: const BorderRadius.all(
                    const Radius.circular(4.0),
                  ),
                ),
                focusedBorder: new OutlineInputBorder(
                  borderSide: new BorderSide(color: Colors.blue, width: 1.0),
                  borderRadius: const BorderRadius.all(
                    const Radius.circular(4.0),
                  ),
                )),
          ),
          SizedBox(
            height: 10,
          ),
          TextField(
            controller: descriptionController,
            textInputAction: TextInputAction.done,
            maxLines: null,
            style: TextStyle(color: Colors.white),
            keyboardType: TextInputType.multiline,
            decoration: new InputDecoration(
                hintText: 'Description',
                hintStyle: TextStyle(
                    color: Colors.white,
                    fontStyle: FontStyle.italic,
                    fontSize: 13),
                enabledBorder: new OutlineInputBorder(
                  borderSide: new BorderSide(color: Colors.white, width: 1.0),
                  borderRadius: const BorderRadius.all(
                    const Radius.circular(4.0),
                  ),
                ),
                focusedBorder: new OutlineInputBorder(
                  borderSide: new BorderSide(color: Colors.blue, width: 1.0),
                  borderRadius: const BorderRadius.all(
                    const Radius.circular(4.0),
                  ),
                )),
          ),
          SizedBox(
            height: 10,
          ),
        ],
      ),
    );
  }

  Widget _buildButtonGenerate() {
    return StreamBuilder<QRCodeGenResponse>(
        stream: _qrCodeBloc.genCodeStream,
        builder: (context, snapshot) {
          return Container(
            child: ButtonBorder(
              width: 200,
              color: UIData.colorItemBottom3(),
              onPress: () {
                _gotoStep();
              },
              text: "GENERATE QR CODE",
            ),
          );
        });
  }

  void _gotoStep() {
    if (nameSymbolChoose.isEmpty) {
      WidgetsBinding.instance.addPostFrameCallback((_) {
        showValidateDialog('Vui lòng chọn symbol');
      });
      return;
    }
    if (amountController.text.isEmpty) {
      WidgetsBinding.instance.addPostFrameCallback((_) {
        showValidateDialog('Vui lòng chọn amount');
      });
      return;
    }
    QRCodeGenRequest qrCodeGenRequest = QRCodeGenRequest();
    qrCodeGenRequest.symbol = nameSymbolChoose.toUpperCase();
    qrCodeGenRequest.description = descriptionController.text;
    qrCodeGenRequest.amount = amountController.text;
    _qrCodeBloc.generation(qrCodeGenRequest);
  }

  void showValidateDialog(String text) {
    fetchApiResult(
        context, NetworkServiceResponse(success: true, message: text));
  }

  @override
  void dispose() {
    super.dispose();
    apiStreamSubscription?.cancel();
  }
}
