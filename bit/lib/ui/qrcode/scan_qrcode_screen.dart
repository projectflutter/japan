import 'package:flutter/material.dart';

class ScanQRCodeScreen extends StatefulWidget {

final String barcode;

  ScanQRCodeScreen({Key key, this.barcode}) : super(key: key);

  _ScanQRCodeScreenState createState() => _ScanQRCodeScreenState();
}

class _ScanQRCodeScreenState extends State<ScanQRCodeScreen> {
  @override
  void initState() {
    // TODO: implement initState
    super.initState();
  }

 

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: Container(
        child: Center(child: Text('${widget.barcode}')),
      ),
    );
  }
}
