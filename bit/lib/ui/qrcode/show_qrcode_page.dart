import 'dart:io';
import 'dart:typed_data';
import 'dart:ui';

import 'package:flutter/material.dart';
import 'package:flutter/rendering.dart';
import 'package:flutter/services.dart';
import 'package:localbit/%08models/qrcode_generation_response.dart';
import 'package:localbit/ui/exchange/widget/button.dart';
import 'package:localbit/ui/qrcode/widgets/qrcode_provider.dart';
import 'package:localbit/utils/uidata.dart';
import 'package:localbit/widgets/card.dart';
import 'package:localbit/widgets/text_header.dart';

import 'widgets/qr_image.dart';
import 'dart:async';
import 'package:path_provider/path_provider.dart';

class ShowQRcode extends StatefulWidget {
  String qrcodeText;
  QrRaw rawQrData;
  ShowQRcode({Key key, this.qrcodeText, this.rawQrData}) : super(key: key);

  _ShowQRcodeState createState() => _ShowQRcodeState();
}

class _ShowQRcodeState extends State<ShowQRcode> {
  GlobalKey globalKey = new GlobalKey();
  @override
  void initState() {
    // TODO: implement initState
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return SingleChildScrollView(
        child: CardCS(
      form: Column(
        children: <Widget>[
          TextHeader(
            text: "QR code",
          ),
          _buildBody()
        ],
      ),
    ));
  }

  Widget _buildBody() {
    return Container(
      child: Column(
        children: <Widget>[
          SizedBox(
            height: 20,
          ),
          _buildImageQRCode(),
          SizedBox(
            height: 10,
          ),
          Container(
            margin: EdgeInsets.symmetric(horizontal: 16),
            height: 1.0,
            width: double.infinity,
            color: Colors.white,
          ),
          SizedBox(
            height: 20,
          ),
          _buildBodyQrRaw(),
          _buildButtonBottom()
        ],
      ),
    );
  }

  Widget _buildImageQRCode() {
    return Container(
      width: 169,
      height: 169,
      color: Colors.white,
      child: RepaintBoundary(
        key: globalKey,
        child: QrImage(
          size: 169,
          data: widget.qrcodeText,
          version: 5,
        ),
      ),
    );
  }

  Widget _buildBodyQrRaw() {
    return Container(
      child: Column(
        children: <Widget>[
          TextLine(
            leftText: 'Amount',
            rightText: '${widget.rawQrData.amount}',
          ),
          TextLine(leftText: 'symbol', rightText: '${widget.rawQrData.symbol}'),
          TextLine(
              leftText: 'Created', rightText: '${widget.rawQrData.createdAt}'),
          TextLine(leftText: 'Status', rightText: '${widget.rawQrData.status}'),
          TextLine(
              leftText: 'Transfer number',
              rightText: '${widget.rawQrData.requestNumber}'),
        ],
      ),
    );
  }

  Widget _buildButtonBottom() {
    return Container(
      child: Column(
        children: <Widget>[
          Container(
            child: ButtonBorder(
              width: 220,
              color: UIData.colorItemBottom3(),
              onPress: () {
                _shareUrl();
              },
              text: "Share URL to pay",
            ),
          ),
          Container(
            child: ButtonBorder(
              width: 220,
              color: Colors.green,
              onPress: () {
                _genNewCode();
              },
              text: "Generate new QR code",
            ),
          )
        ],
      ),
    );
  }

  void _shareUrl() {}

  void _genNewCode() {
    QRcodeProvider.of(context).backpage();
  }

  Future<void> _captureAndSharePng() async {
    try {
      RenderRepaintBoundary boundary =
          globalKey.currentContext.findRenderObject();
      var image = await boundary.toImage();
      ByteData byteData = await image.toByteData(format: ImageByteFormat.png);
      Uint8List pngBytes = byteData.buffer.asUint8List();
      final tempDir = await getTemporaryDirectory();
      final file = await new File('${tempDir.path}/image.png').create();
      await file.writeAsBytes(pngBytes);
      final channel = const MethodChannel('channel:me.alfian.share/share');
      channel.invokeMethod('shareFile', 'image.png');
    } catch (e) {
      print(e.toString());
    }
  }
}

class TextLine extends StatelessWidget {
  String leftText;
  String rightText;
  TextLine({@required this.leftText, @required this.rightText});
  @override
  Widget build(BuildContext context) {
    return Container(
      margin: EdgeInsets.symmetric(vertical: 4, horizontal: 16),
      child: Row(
        mainAxisAlignment: MainAxisAlignment.spaceBetween,
        children: <Widget>[
          Text(
            leftText,
            style: TextStyle(color: Colors.white, fontSize: 13),
          ),
          Text(
            rightText,
            style: TextStyle(
                color: Colors.white, fontSize: 15, fontWeight: FontWeight.bold),
          )
        ],
      ),
    );
  }
}
