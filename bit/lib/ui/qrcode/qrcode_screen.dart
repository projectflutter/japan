import 'package:flutter/material.dart';
import 'package:localbit/%08models/qrcode_generation_response.dart';
import 'package:localbit/blocs/generic_bloc_provider.dart';
import 'package:localbit/blocs/qrcode_bloc.dart';
import 'package:localbit/ui/qrcode/gen_qrcode_page.dart';
import 'package:localbit/ui/qrcode/show_qrcode_page.dart';

import 'widgets/qrcode_provider.dart';

class QRCodeScreen extends StatefulWidget {
  QRCodeScreen({Key key}) : super(key: key);

  _QRCodeScreenState createState() => _QRCodeScreenState();
}

class _QRCodeScreenState extends State<QRCodeScreen> {
  bool nextpage = false;
  String qrcodeText = "";
  QrRaw rawQrData;
  @override
  Widget build(BuildContext context) {
    return BlocProvider<QRCodeBloc>(
      bloc: QRCodeBloc(),
      child: QRcodeProvider(
        nextpage: _handleNextPage,
        backpage: _handleBackpage,
        child: Container(
          child: !nextpage
              ? GenerationQRCodePage()
              : ShowQRcode(qrcodeText: qrcodeText, rawQrData: rawQrData),
        ),
      ),
    );
  }

  _handleNextPage(String qrcodeText, QrRaw qrRaw) {
    setState(() {
      nextpage = true;
      this.qrcodeText = qrcodeText;
      rawQrData = qrRaw;
    });
  }

  _handleBackpage() {
    setState(() {
      nextpage = false;
    });
  }
}
