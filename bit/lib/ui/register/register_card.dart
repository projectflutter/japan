import 'dart:async';
import 'dart:math';

import 'package:flutter/material.dart';
import 'package:localbit/%08models/model_field.dart';
import 'package:localbit/%08models/register_request.dart';
import 'package:localbit/%08models/register_response.dart';
import 'package:localbit/blocs/register_bloc.dart';
import 'package:localbit/datasources/network/network_service_response.dart';
import 'package:localbit/ui/register/widgets/field_register.dart';
import 'package:localbit/utils/api_subscription.dart';
import 'package:localbit/utils/common_dialogs.dart';
import 'package:localbit/utils/constant.dart';
import 'package:localbit/utils/fetch_process.dart';
import 'package:localbit/utils/uidata.dart';
import 'package:localbit/widgets/button.dart';
import 'package:localbit/widgets/button_circle_close.dart';
import 'package:localbit/widgets/card.dart';

class RegisterCard extends StatefulWidget {
  RegisterCard({Key key}) : super(key: key);

  _RegisterCardState createState() => _RegisterCardState();
}

class _RegisterCardState extends State<RegisterCard> {
  List<CustomTextField> listField = [];
  RegisterBloc _registerBloc;
  StreamSubscription<FetchProcess> apiStreamSubscription;
  @override
  void initState() {
    super.initState();
    listField = RegisterListField.getlist();
    _registerBloc = RegisterBloc();
    apiStreamSubscription = apiSubscription(_registerBloc.apiResult, context);
  }

  @override
  Widget build(BuildContext context) {
    return CardCS(
      form: Column(
        children: <Widget>[ButtonClose(), loginForm()],
      ),
    );
  }

  Widget loginForm() {
    return Padding(
      padding: EdgeInsets.only(left: 30.0, right: 30.0),
      child: StreamBuilder(
          stream: _registerBloc.registerStream,
          builder: (context, snapshot) {
            if (snapshot.hasData && snapshot.data.status != -1) {
              handleResults(snapshot.data);
            }
            return Column(
              children: <Widget>[
                Center(
                  child: Text(
                    "Create new account",
                    style: TextStyle(
                        color: Colors.white,
                        fontSize: 18.0,
                        fontWeight: FontWeight.bold),
                  ),
                ),
                Column(
                  children: listField,
                ),
                Button(
                  color: UIData.colorYellow(),
                  onPress: () {
                    _onPressRegister();
                  },
                  text: "Create new account",
                ),
                SizedBox(height: 10.0),
              ],
            );
          }),
    );
  }

  void _onPressRegister() {
    RegisterRequest registerRequest = RegisterRequest(
        listField[0].controller.text,
        listField[1].controller.text,
        listField[2].controller.text,
        listField[3].controller.text,
        listField[4].controller.text,
        listField[5].controller.text,
        listField[6].controller.text,
        listField[7].controller.text);
    _registerBloc.callLogin(registerRequest);
  }

  void handleResults(RegisterResponse data) {
    if (data.status == Constant.SUCESS_CODE) {
      WidgetsBinding.instance.addPostFrameCallback((_) => goToHome(context));
    } else {
      WidgetsBinding.instance
          .addPostFrameCallback((_) => showErrorLogin(data, context));
    }
  }

  goToHome(BuildContext context) {
    Navigator.pushNamedAndRemoveUntil(
        context, '/home', (Route<dynamic> route) => false);
  }

  showErrorLogin(RegisterResponse data, BuildContext context) {
    fetchApiResult(
        context, NetworkServiceResponse(success: true, message: data.message));
  }

  @override
  void dispose() {
    super.dispose();
    _registerBloc.dispose();
  }
}
