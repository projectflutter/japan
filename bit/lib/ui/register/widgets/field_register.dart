import 'package:flutter/material.dart';

class CustomTextField extends StatelessWidget {
  final String textTopField;
  final TextEditingController controller;
  bool obscureText = false;
  TextInputType keyboardType;

  CustomTextField(
      {Key key,
      this.textTopField,
      this.controller,
      this.obscureText,
      this.keyboardType})
      : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Column(
      children: <Widget>[
        SizedBox(height: 10.0),
        Align(
          alignment: Alignment.centerLeft,
          child: Text(
            textTopField,
            style: TextStyle(color: Colors.white),
          ),
        ),
        SizedBox(height: 10.0),
        TextField(
          controller: controller,
          obscureText: obscureText != null ? true : false,
          decoration: InputDecoration(
            contentPadding: const EdgeInsets.all(10.0),
            hintStyle: TextStyle(color: Colors.blue),
            border: OutlineInputBorder(
                borderRadius: BorderRadius.circular(3.0),
                borderSide: BorderSide.none),
            filled: true,
            fillColor: Colors.white.withOpacity(1),
          ),
          keyboardType: keyboardType,
        ),
      ],
    );
  }
}
