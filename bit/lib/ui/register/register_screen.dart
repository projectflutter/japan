import 'package:flutter/material.dart';
import 'package:localbit/ui/register/register_card.dart';
import 'package:localbit/utils/uidata.dart';
import 'package:localbit/widgets/%1Bgradient_background.dart';
import 'package:localbit/widgets/common_background.dart';
import 'package:localbit/widgets/logo_app.dart';
import 'package:localbit/widgets/toolbar.dart';

class RegisterScreen extends StatefulWidget {
  RegisterScreen({Key key}) : super(key: key);

  _RegisterScreenState createState() => _RegisterScreenState();
}

class _RegisterScreenState extends State<RegisterScreen> {
  RegisterCard registerCard;
  @override
  void initState() {
    // TODO: implement initState
    super.initState();
    registerCard = RegisterCard();
  }

  @override
  Widget build(BuildContext context) {
    final bottomText = new Container(
      padding: EdgeInsets.all(UIData.paddingNormal),
      child: Text(
        UIData.copyright,
        style: TextStyle(color: Colors.white),
      ),
    );
    return Scaffold(
      resizeToAvoidBottomInset: false,
      body: SingleChildScrollView(
        child: Stack(
          alignment: Alignment.bottomCenter,
          children: <Widget>[
            _buildBackground(),
            Align(alignment: Alignment.bottomCenter, child: bottomText),
            _buildBodyContent()
          ],
        ),
      ),
      appBar: PreferredSize(
        child: CustomizeAppbar(),
        preferredSize: Size.fromHeight(50.0),
      ),
    );
  }

  Widget _buildBackground() {
    return Container(
        width: double.infinity,
        child: Image.asset(
          UIData.imageBottomLoading,
          fit: BoxFit.fill,
        ));
  }

  Widget _buildBodyContent() {
    return Column(
      children: <Widget>[
        LogoApp(),
        SizedBox(
          height: 40,
        ),
        registerCard,
        SizedBox(
          height: 128,
        ),
      ],
    );
  }
}
