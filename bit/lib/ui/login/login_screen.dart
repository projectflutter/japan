import 'package:flutter/material.dart';
import 'package:localbit/ui/login/login_card.dart';
import 'package:localbit/widgets/common_background.dart';
import 'package:localbit/widgets/logo_app.dart';
import 'package:localbit/widgets/toolbar.dart';

class LoginScreen extends StatefulWidget {
  LoginScreen({Key key}) : super(key: key);

  _LoginScreenState createState() => _LoginScreenState();
}

class _LoginScreenState extends State<LoginScreen> {
  LoginCard loginCard;
  @override
  void initState() {
    // TODO: implement initState
    super.initState();
    loginCard = LoginCard();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      resizeToAvoidBottomInset: false,
      body: Container(
        child: Stack(
          children: <Widget>[_buildBackground(), _buildBodyContent()],
        ),
      ),
    );
  }

  Widget _buildBackground() {
    return LoadingBackground();
  }

  Widget _buildBodyContent() {
    return SingleChildScrollView(
      child: ConstrainedBox(
        constraints: BoxConstraints(),
        child: Column(
          children: <Widget>[
            CustomizeAppbar(),
            LogoApp(),
            SizedBox(
              height: 40,
            ),
            loginCard,
          ],
        ),
      ),
    );
  }
}
