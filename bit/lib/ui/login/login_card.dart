import 'dart:async';

import 'package:flutter/material.dart';
import 'package:localbit/%08models/login_request.dart';
import 'package:localbit/%08models/login_response.dart';
import 'package:localbit/blocs/login_bloc.dart';
import 'package:localbit/datasources/network/network_service_response.dart';
import 'package:localbit/utils/api_subscription.dart';
import 'package:localbit/utils/common_dialogs.dart';
import 'package:localbit/utils/constant.dart';
import 'package:localbit/utils/fetch_process.dart';
import 'package:localbit/utils/uidata.dart';
import 'package:localbit/widgets/button.dart';
import 'package:localbit/widgets/button_circle_close.dart';
import 'package:localbit/widgets/card.dart';

class LoginCard extends StatefulWidget {
  LoginCard({Key key}) : super(key: key);

  _LoginCardState createState() => _LoginCardState();
}

class _LoginCardState extends State<LoginCard> {
  LoginBloc loginBloc;
  String email, password;
  StreamSubscription<FetchProcess> apiStreamSubscription;
  final _usernameController = TextEditingController();
  final _passwordController = TextEditingController();
  bool isValidateEmail = false;
  bool isValidatePass = false;
  @override
  void initState() {
    super.initState();
    loginBloc = new LoginBloc();
    apiStreamSubscription = apiSubscription(loginBloc.apiResult, context);
  }

  @override
  Widget build(BuildContext context) {
    return CardCS(
      form: Column(
        children: <Widget>[ButtonClose(), loginForm()],
      ),
    );
  }

  Widget loginForm() {
    return StreamBuilder<LoginResponse>(
        stream: loginBloc.loginStream,
        builder: (context, snapshot) {
          print('setstate');
          if (snapshot.hasData && snapshot.data.status != -1) {
            handleResults(snapshot.data);
          }

          return Padding(
            padding: EdgeInsets.only(left: 30.0, right: 30.0),
            child: Column(
              children: <Widget>[
                Center(
                  child: Text(
                    "Login",
                    style: TextStyle(
                        color: Colors.white,
                        fontSize: 18.0,
                        fontWeight: FontWeight.bold),
                  ),
                ),
                SizedBox(height: 10.0),
                Align(
                  alignment: Alignment.centerLeft,
                  child: Text(
                    "Email",
                    style: TextStyle(color: Colors.white),
                  ),
                ),
                SizedBox(height: 10.0),
                TextFormField(
                  controller: _usernameController,
                  decoration: InputDecoration(
                    contentPadding: const EdgeInsets.all(10.0),
                    hintStyle: TextStyle(color: Colors.blue),
                    border: OutlineInputBorder(
                        borderRadius: BorderRadius.circular(3.0),
                        borderSide: BorderSide.none),
                    filled: true,
                    fillColor: Colors.white.withOpacity(1),
                    errorText: isValidateEmail ? 'Email not empty!' : null,
                  ),
                ),
                SizedBox(height: 10.0),
                Align(
                    alignment: Alignment.centerLeft,
                    child: Text("Password",
                        style: TextStyle(color: Colors.white))),
                SizedBox(height: 10.0),
                TextField(
                  controller: _passwordController,
                  obscureText: true,
                  decoration: InputDecoration(
                    contentPadding: const EdgeInsets.all(10.0),
                    hintStyle: TextStyle(color: Colors.blue),
                    border: OutlineInputBorder(
                        borderRadius: BorderRadius.circular(3.0),
                        borderSide: BorderSide.none),
                    filled: true,
                    fillColor: Colors.white.withOpacity(1),
                    errorText: isValidatePass ? 'Password not empty!' : null,
                  ),
                ),
                SizedBox(height: 10.0),
                Button(
                  color: UIData.colorYellow(),
                  onPress: () {
                    _onPressLogin();
                  },
                  text: "Login",
                ),
                Container(
                    padding:
                        EdgeInsets.symmetric(vertical: 5.0, horizontal: 0.0),
                    width: double.infinity,
                    child: Center(
                        child: Text(
                      'Forgot password',
                      style: TextStyle(
                          fontStyle: FontStyle.italic, color: Colors.white70),
                    ))),
                SizedBox(height: 10.0),
              ],
            ),
          );
        });
  }

  void _onPressLogin() {
    if (!_usernameController.text.isEmpty &&
        !_passwordController.text.isEmpty) {
      LoginRequest loginRequest = LoginRequest(
          email: _usernameController.text, password: _passwordController.text);
      loginBloc.callLogin(loginRequest);
    }
    {
      LoginResponse temp = LoginResponse();
      temp.status = -1;
      loginBloc.loginController.add(temp);
      setState(() {
        _usernameController.text.isEmpty
            ? isValidateEmail = true
            : isValidateEmail = false;
        _passwordController.text.isEmpty
            ? isValidatePass = true
            : isValidatePass = false;
      });
    }
  }

  @override
  void dispose() {
    // TODO: implement dispose
    super.dispose();
    apiStreamSubscription?.cancel();
    loginBloc?.dispose();
  }

  void handleResults(LoginResponse data) {
    if (data.status == Constant.SUCESS_CODE) {
      WidgetsBinding.instance.addPostFrameCallback((_) => goToHome(context));
    } else {
      WidgetsBinding.instance
          .addPostFrameCallback((_) => showErrorLogin(data, context));
    }
  }

  goToHome(BuildContext context) {
    Navigator.pushNamedAndRemoveUntil(
        context, '/home', (Route<dynamic> route) => false);
  }

  showErrorLogin(LoginResponse data, BuildContext context) {
    fetchApiResult(
        context, NetworkServiceResponse(success: true, message: data.message));
  }
}
