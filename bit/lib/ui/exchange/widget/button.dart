import 'package:flutter/material.dart';

typedef void OnPress();

class ButtonBorder extends StatelessWidget {
  final String text;
  final OnPress onPress;
  final Color color;
  final Color textColor;
  final Widget iconLeft;
  final double width;

  ButtonBorder(
      {Key key,
      @required this.text,
      @required this.onPress,
      @required this.color,
      this.textColor, this.iconLeft, this.width})
      : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Container( width: width!=null?width:120,
      padding: EdgeInsets.symmetric(vertical: 4.0, horizontal: 8.0),
      // decoration: BoxDecoration(
      //   border: Border.all(color: Colors.white, width: 1.0),
      // ),
      child: RaisedButton.icon( 
        // padding: EdgeInsets.all(12.0), 
        shape: new RoundedRectangleBorder(
            borderRadius: new BorderRadius.circular(
              4.0,
            ),
            side: BorderSide(color: Colors.white, width: 0.5)),
        label: Text(
          text,
          style: TextStyle(color: textColor == null ? Colors.white : textColor),
        ),
        color: color,
        onPressed: () {
          onPress();
        }, icon: iconLeft!=null?iconLeft:Container(),
      ),
    );
  }
}
