import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter/painting.dart';
import 'package:localbit/%08models/fiats_response.dart';
import 'package:localbit/widgets/combobox.dart';
import 'package:localbit/widgets/text_yellow_wallet.dart';

class ExchangeWidget extends StatelessWidget {
  final List<Fiats> listDropdownField;
  ExchangeWidget({Key key, this.listDropdownField}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Container(
      padding: const EdgeInsets.symmetric(horizontal: 4.0),
      child: Column(
        children: <Widget>[
          Combobox(listDropdownField: listDropdownField),
          SizedBox(
            height: 10,
          ),
          Text(
            "Wallet Balance",
            style: TextStyle(color: Colors.white, fontSize: 13),
          ),
          SizedBox(
            height: 10,
          ),
          TextYellowWallet(
            text: '\$ 300.25',
          ),
          SizedBox(
            height: 30,
          ),
          Text(
            "Amount (USD)",
            style: TextStyle(color: Colors.white, fontSize: 15),
          ),
          SizedBox(
            height: 5,
          ),
          Container(
            width: MediaQuery.of(context).size.width * 0.36,
            color: Colors.white,
            child: Padding(
              padding: const EdgeInsets.all(2.0),
              child: TextField(
                // controller: controller,
                decoration: InputDecoration(
                  contentPadding: const EdgeInsets.all(10.0),
                  hintText: "\"Tự sinh ra khi nguời dùng nhập số lượng BTC\"",
                  hintMaxLines: 1,
                  hintStyle: TextStyle(
                      color: Colors.grey,
                      fontSize: 9,
                      fontStyle: FontStyle.italic),
                  border: OutlineInputBorder(
                      borderRadius: BorderRadius.circular(3.0),
                      borderSide: BorderSide.none),
                  filled: true,
                  fillColor: Colors.white.withOpacity(1),
                ),
                keyboardType: TextInputType.number, 
              ),
            ),
          )
        ],
      ),
    );
  }
}
