import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:localbit/%08models/fiats_response.dart';
import 'package:localbit/blocs/exchange_bloc.dart';
import 'package:localbit/ui/exchange/widget/button.dart';
import 'package:localbit/ui/exchange/widget/exchange_widget.dart';
import 'package:localbit/utils/uidata.dart';
import 'package:localbit/widgets/card.dart';
import 'package:localbit/widgets/text_header.dart';

class ExchangeScreen extends StatefulWidget {
  ExchangeScreen({Key key}) : super(key: key);

  _ExchangeScreenState createState() => _ExchangeScreenState();
}

class _ExchangeScreenState extends State<ExchangeScreen>
    with AutomaticKeepAliveClientMixin<ExchangeScreen> {
  ExchangeBloc _exchangeBloc;
  List<Fiats> originListFiats = [];

  @override
  void initState() {
    super.initState();
    _exchangeBloc = ExchangeBloc();
    originListFiats = _exchangeBloc.getListFiats();
    print(originListFiats.length);
  }

  @override
  Widget build(BuildContext context) {
    print("buildUI");
    return SingleChildScrollView(
      child: ConstrainedBox(
        constraints: BoxConstraints(),
        child: CardCS(
          form: _buildFormExchange(),
        ),
      ),
    );
  }

  @override
  // TODO: implement wantKeepAlive
  bool get wantKeepAlive => true;

  Widget _buildFormExchange() {
    return Column(
      children: <Widget>[
        _buildForm1(),
        SizedBox(
          height: 30,
        ),
        _buildForm2(),
        _buildForm3(),
        SizedBox(
          height: 20,
        ),
        _buildButton()
      ],
    );
  }

  Widget _buildForm1() {
    return TextHeader(
      text: "Exchange",
    );
  }

  Widget _buildForm2() {
    return Row(
      crossAxisAlignment: CrossAxisAlignment.start,
      mainAxisAlignment: MainAxisAlignment.spaceBetween,
      children: <Widget>[
        ExchangeWidget(
          listDropdownField: originListFiats,
        ),
        Column(
          children: <Widget>[
            Text(
              'Rate',
              style: TextStyle(
                  color: Colors.white,
                  fontSize: 11,
                  fontStyle: FontStyle.italic),
            ),
            Text('10,794',
                style: TextStyle(
                    color: Colors.white,
                    fontSize: 11,
                    fontWeight: FontWeight.bold)),
            Text('USD/BTC',
                style: TextStyle(
                    color: Colors.white,
                    fontSize: 11,
                    fontStyle: FontStyle.italic))
          ],
        ),
        ExchangeWidget(
          listDropdownField: originListFiats,
        ),
      ],
    );
  }

  Widget _buildForm3() {
    return Column(
      children: <Widget>[
        SizedBox(
          height: 20,
        ),
        Padding(
          padding: const EdgeInsets.symmetric(horizontal: 8.0),
          child: Row(
            mainAxisAlignment: MainAxisAlignment.spaceBetween,
            children: <Widget>[
              Text(
                'Total fee:',
                style: TextStyle(color: Colors.white),
              ),
              Text(
                '3.4534 USD',
                style: TextStyle(color: Colors.white),
              )
            ],
          ),
        ),
        SizedBox(
          height: 10,
        ),
        Padding(
          padding: const EdgeInsets.symmetric(horizontal: 8.0),
          child: Row(
            mainAxisAlignment: MainAxisAlignment.spaceBetween,
            children: <Widget>[
              Text(
                'Total payment:',
                style:
                    TextStyle(color: Colors.white, fontWeight: FontWeight.bold),
              ),
              Text(
                '13.4534 USD',
                style:
                    TextStyle(color: Colors.white, fontWeight: FontWeight.bold),
              )
            ],
          ),
        )
      ],
    );
  }

  Widget _buildButton() {
    return Row(
      crossAxisAlignment: CrossAxisAlignment.center,
      mainAxisAlignment: MainAxisAlignment.center,
      children: <Widget>[
        ButtonBorder(
          color: Colors.green,
          onPress: () {},
          text: "Add fund",
        ),
        Container(
          child: ButtonBorder(
            color: UIData.colorItemBottom3(),
            onPress: () {},
            text: "Next",
          ),
        )
      ],
    );
  }
}
