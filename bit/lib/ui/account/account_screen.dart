import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:localbit/%08models/profile_response.dart';
import 'package:localbit/blocs/home_screen_bloc.dart';
import 'package:localbit/ui/account/widget_text.dart';
import 'package:localbit/widgets/card.dart';

class AccountScreen extends StatefulWidget {
  AccountScreen({Key key}) : super(key: key);

  _AccountScreenState createState() => _AccountScreenState();
}

class _AccountScreenState extends State<AccountScreen>
    with
        AutomaticKeepAliveClientMixin<AccountScreen>,
        SingleTickerProviderStateMixin {
  AnimationController _controller;
  HomeScreenBloc _homeScreenBloc;
  @override
  void initState() {
    super.initState();
    _controller = AnimationController(
        vsync: this, duration: Duration(milliseconds: 1780));

    _controller.forward();
    _homeScreenBloc = HomeScreenBloc();
    _homeScreenBloc.getProfile();
  }

  @override
  Widget build(BuildContext context) {
    Size deviceSize = MediaQuery.of(context).size;

    Widget profileHeader(Profile profile) => Container(
          height: deviceSize.height / 4,
          width: double.infinity,
          color: Colors.transparent,
          child: Padding(
            padding: const EdgeInsets.all(10.0),
            child: Container(
              color: Colors.transparent,
              child: FittedBox(
                child: Column(
                  mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                  children: <Widget>[
                    Container(
                      decoration: BoxDecoration(
                          borderRadius: BorderRadius.circular(50.0),
                          border: Border.all(width: 2.0, color: Colors.white)),
                      child: CircleAvatar(
                        radius: 40.0,
                        backgroundImage: 
                        profile != null
                            ? NetworkImage(
                                "${profile.avatar}")
                            : 
                            AssetImage("assets/images/bg_reside_menu.png"),
                      ),
                    ),
                    Text(
                      profile != null
                          ? "${profile.firstName} ${profile.lastName}"
                          : "User name",
                      style: TextStyle(color: Colors.white, fontSize: 20.0),
                    ),
                  ],
                ),
              ),
            ),
          ),
        );

    Widget bodyContent(Profile profile) {
      return Container(
        margin: EdgeInsets.symmetric(horizontal: 20.0),
        width: double.infinity,
        child: Column(
          crossAxisAlignment: CrossAxisAlignment.start,
          children: <Widget>[
            WidgetText(
              text: "Email:",
              value: profile!=null?'${profile.email}':'email@gmail.com',
            ),
            Container(
              height: 1.0,
              color: Colors.grey,
            ),
            WidgetText(
              text: "Phone:",
              value: profile!=null?'${profile.phone}':'xxxxxxxxxx',
            ),
            Container(
              height: 1.0,
              color: Colors.grey,
            ),
            WidgetText(
              text: "Country:",
              value: profile!=null?'${profile.country}':'America',
            ),
            Container(
              height: 1.0,
              color: Colors.grey,
            ),
            WidgetText(
                text: "Address:",
                value: profile!=null?'${profile.address}':"Address"),
          ],
        ),
      );
    }

    return StreamBuilder<ProfileResponse>(
        stream: _homeScreenBloc.profileStream,
        builder: (context, snapshot) {
          return SingleChildScrollView(
              child: CardCS(
            form: Column(
              children: <Widget>[
                snapshot.hasData
                    ? profileHeader(snapshot.data.data.profile)
                    : profileHeader(null),
                Container(
                  margin:
                      EdgeInsets.symmetric(vertical: 20.0, horizontal: 20.0),
                  height: 1.0,
                  color: Colors.blue,
                ),
                snapshot.hasData
                    ? bodyContent(snapshot.data.data.profile)
                    : bodyContent(null),
                SizedBox(
                  height: 20,
                )
              ],
            ),
          ));
        });
  }

  @override
  // TODO: implement wantKeepAlive
  bool get wantKeepAlive => true;
  @override
  void dispose() {
    _controller.dispose();
    super.dispose();
  }
}
