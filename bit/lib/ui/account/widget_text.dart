import 'package:flutter/material.dart';

class WidgetText extends StatelessWidget {
  final String text;
  final String value;
  WidgetText({Key key, this.text, this.value}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Container(
      padding: EdgeInsets.symmetric(vertical: 10),
      child: Row(
        mainAxisAlignment: MainAxisAlignment.spaceBetween,
        children: <Widget>[
          Text(text,style: TextStyle(color: Colors.white)),
          Flexible( 
              child: Text( 
            value != null ? value : "",textAlign: TextAlign.end, style: TextStyle(color: Colors.white)
          )),
        ],
      ),
    );
  }
}
