import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:localbit/utils/uidata.dart';

class ItemListHistory extends StatelessWidget {
  const ItemListHistory({Key key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Padding(
      padding: const EdgeInsets.all(8.0),
      child: Row(
        children: <Widget>[
          Expanded(
            child: Column(
              crossAxisAlignment: CrossAxisAlignment.start,
              children: <Widget>[
                Text('Revieve from DANG LE NGUYEN',
                    style: TextStyle(
                        fontWeight: FontWeight.bold,
                        color: Colors.black87.withOpacity(0.7))),
                Padding(
                  padding: const EdgeInsets.symmetric(vertical: 8.0),
                  child: Text(
                    '20/20/2019',
                    style: TextStyle(color: Colors.grey, fontSize: 12),
                  ),
                )
              ],
            ),
          ),
          Align(
              alignment: Alignment.topRight,
              child: Text('+\$ 10,000',
                  style: TextStyle(
                    color: UIData.colorItemBottom1(),
                  )))
        ],
      ),
    );
  }
}
