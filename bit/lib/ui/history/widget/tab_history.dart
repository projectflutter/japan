import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';

typedef OnTabSelectd(int index);

class TabHistory extends StatefulWidget {
  final OnTabSelectd onTabSelectd;
  var _selectedIndex = 0;
  List<ItemSelect> listTitleTab;
  TabHistory({Key key, this.listTitleTab, this.onTabSelectd})
      : super(key: key);

  _TabHistoryState createState() =>
      _TabHistoryState(listTitleTab: listTitleTab);
}

class _TabHistoryState extends State<TabHistory> {
  List<ItemSelect> listTitleTab;
  _TabHistoryState({this.listTitleTab});

  @override
  Widget build(BuildContext context) {
    Widget itemTab(ItemSelect item) {
      return Center(
        child: Container(
          padding: EdgeInsets.all(8.0),
          margin: EdgeInsets.all(0.5),
          decoration: item.isSelected
              ? new BoxDecoration(
                  color: Colors.white,
                  borderRadius: BorderRadius.circular(2.0),
                    border: Border.all(color: Colors.white, width: 1.0),
                )
              : new BoxDecoration(
                  borderRadius: BorderRadius.circular(2.0),
                  border: Border.all(color: Colors.white, width: 1.0),
                ),
          child: Text(
            item.title,
            style:
                TextStyle(color: item.isSelected ? Colors.blue : Colors.white),
          ),
        ),
      );
    }

    return Container(
        height: 64,
        child: Center(
          child: ListView.builder(
            shrinkWrap: true,
            scrollDirection: Axis.horizontal,
            itemCount: listTitleTab.length,
            itemBuilder: (BuildContext context, int index) {
              return InkWell(
                  onTap: () {
                    widget.onTabSelectd(index);
                    setState(() {
                      for (var i = 0; i < listTitleTab.length; i++) {
                        if (i == index) {
                          listTitleTab[i].isSelected = true;
                        } else {
                          listTitleTab[i].isSelected = false;
                        }
                      }
                    });
                  },
                  child: itemTab(widget.listTitleTab[index]));
            },
          ),
        ));
  }
}

class ItemSelect {
  String title;
  bool isSelected;
  ItemSelect({this.title, this.isSelected});
}
