import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:localbit/ui/history/widget/item_list_history.dart';
import 'package:localbit/widgets/card.dart';
import 'package:localbit/widgets/text_header.dart';

import 'widget/date_widget.dart';
import 'widget/tab_history.dart';

class HistoryScreen extends StatefulWidget {
  HistoryScreen({Key key}) : super(key: key);

  _HistoryScreenState createState() => _HistoryScreenState();
}

class _HistoryScreenState extends State<HistoryScreen>
    with AutomaticKeepAliveClientMixin<HistoryScreen> {
  @override
  Widget build(BuildContext context) {
    return Container(
      child: CardCS(form: _buildFormHistory()),
    );
  }

  @override
  bool get wantKeepAlive => true;

  Widget _buildFormHistory() {
    return Column(
      children: <Widget>[_buildForm1(), _buildForm2(), _buildContentBody()],
    );
  }

  Widget _buildForm1() {
    return Container(
      padding: EdgeInsets.all(4.0),
      child: Row(
        mainAxisAlignment: MainAxisAlignment.spaceBetween,
        children: <Widget>[
          TextHeader(
            text: "Activity",
          ),
          Align(
              alignment: Alignment.bottomRight,
              child: Container(
                decoration: BoxDecoration(
                  borderRadius: BorderRadius.circular(2.0),
                  border: Border.all(color: Colors.white, width: 1.0),
                ),
                child: Padding(
                  padding: const EdgeInsets.only(left: 8.0, right: 2.0),
                  child: Row(
                    children: <Widget>[
                      Text(
                        "All",
                        style: TextStyle(color: Colors.white),
                      ),
                      RotatedBox(
                        child: Icon(
                          Icons.play_arrow,
                          color: Colors.white,
                        ),
                        quarterTurns: 1,
                      )
                    ],
                  ),
                ),
              ))
        ],
      ),
    );
  }

  Widget _buildForm2() {
    return Row(
      mainAxisAlignment: MainAxisAlignment.end,
      children: <Widget>[
        Padding(
          padding: const EdgeInsets.all(8.0),
          child: Text(
            "Date:",
            style: TextStyle(color: Colors.white),
          ),
        ),
        DateWidget(),
        Padding(
          padding: const EdgeInsets.all(8.0),
          child: Text(
            "to",
            style: TextStyle(color: Colors.white),
          ),
        ),
        DateWidget(),
      ],
    );
  }

  Widget _buildContentBody() {
    List<ItemSelect> list = [];
    list.add(ItemSelect(isSelected: true, title: "All"));
    list.add(ItemSelect(isSelected: false, title: "Incoming"));
    list.add(ItemSelect(isSelected: false, title: "Outcoming"));
    return Expanded(
      child: Column(
        children: <Widget>[
          TabHistory(
            listTitleTab: list,
            onTabSelectd: (index) {
              print(index);
            },
          ),
          Expanded(child: _buildList())
        ],
      ),
    );
  }

  Widget _buildList() {
    return Container(
      child: MediaQuery.removePadding(
        removeTop: true,
        child: ListView.builder(
          scrollDirection: Axis.vertical,
          itemCount: 20,
          itemBuilder: (BuildContext context, int index) {
            return itemList();
          },
        ),
        context: context,
      ),
    );
  }

  Widget itemList() {
    return Container(
      margin: EdgeInsets.symmetric(vertical: 2.0, horizontal: 10.0),
      width: double.infinity,
      decoration: new BoxDecoration(
        color: Colors.white,
        borderRadius: BorderRadius.circular(2.0),
        border: Border.all(color: Colors.white, width: 1.0),
      ),
      child: ItemListHistory(),
    );
  }
}
