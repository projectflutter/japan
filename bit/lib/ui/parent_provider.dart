import 'package:flutter/material.dart';

class ParentProvider extends InheritedWidget {
  final Function listener;
  final Function reloadwallet;
  final Function clearFlag;
  ParentProvider({
    this.listener,
    Key key,
    this.child,
    this.reloadwallet,
    this.clearFlag,
  }) : super(key: key, child: child);

  final Widget child;

  static ParentProvider of(BuildContext context) {
    return (context.inheritFromWidgetOfExactType(ParentProvider)
        as ParentProvider);
  }

  @override
  bool updateShouldNotify(ParentProvider oldWidget) {
    return true;
  }
}
