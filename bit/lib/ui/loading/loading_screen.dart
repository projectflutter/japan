import 'package:flutter/material.dart';
import 'package:localbit/widgets/common_background.dart';
import 'package:localbit/widgets/logo_app.dart';

class LoadingScreen extends StatefulWidget {
  LoadingScreen({Key key}) : super(key: key);

  _LoadingScreenState createState() => _LoadingScreenState();
}

class _LoadingScreenState extends State<LoadingScreen> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
          body: Container(
        child: Stack(
          children: <Widget>[_buildbackground(), _buildBodyContent()],
        ),
      ),
    );
  }

  Widget _buildbackground() {
    return LoadingBackground();
  }

  Widget _buildBodyContent() {
    final formLoading = new Expanded(
        child: Container(
      padding: EdgeInsets.only(left: 36.0, right: 36.0),
      child: Column(
        children: <Widget>[
          Container(
            padding: EdgeInsets.symmetric(vertical: 4.0, horizontal: 30.0),
            width: double.infinity,
            child: RaisedButton(
              padding: EdgeInsets.all(12.0),
              shape: new RoundedRectangleBorder(
                  borderRadius: new BorderRadius.circular(10.0)),
              child: Text(
                "Sign in",
                style: TextStyle(color: Colors.white),
              ),
              color: Colors.blue,
              onPressed: () {
                _onPressSignIn();
              },
            ),
          ),
          Container(
            padding: EdgeInsets.symmetric(horizontal: 30.0),
            width: double.infinity,
            child: RaisedButton(
              padding: EdgeInsets.all(12.0),
              shape: new RoundedRectangleBorder(
                  borderRadius: new BorderRadius.circular(10.0)),
              child: Text(
                "Create new account",
                style: TextStyle(color: Colors.white),
              ),
              color: Colors.blue,
              onPressed: () {
                _onPressCreateNew();
              },
            ),
          ),
          InkWell(
            onTap: () {
              _onPressForgotPass();
            },
            child: Container(
                padding: EdgeInsets.symmetric(vertical: 10.0, horizontal: 0.0),
                width: double.infinity,
                child: Center(
                    child: Text(
                  'Forgot password',
                  style: TextStyle(fontStyle: FontStyle.italic),
                ))),
          )
        ],
      ),
    ));

    return Column(
      children: <Widget>[
        Expanded(
          flex: 1,
          child: Container(),
        ),
        Expanded(
          flex: 4,
          child: Column(
            children: <Widget>[LogoApp(), formLoading],
          ),
        ),
      ],
    );
  }

  void _onPressSignIn() {
    Navigator.pushNamed(context, "/login");
  }

  void _onPressCreateNew() {
    Navigator.pushNamed(context, "/register");
  }

  void _onPressForgotPass() {
     Navigator.pushNamed(context, "/forgotpassword");
  }
}
