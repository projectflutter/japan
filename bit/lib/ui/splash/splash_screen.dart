import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:localbit/blocs/splash_bloc.dart';
import 'package:localbit/widgets/%1Bgradient_background.dart';

class SplashScreen extends StatefulWidget {
  SplashScreen({Key key}) : super(key: key);

  _SplashScreenState createState() => _SplashScreenState();
}

class _SplashScreenState extends State<SplashScreen> {
  AuthenticationBloc _authenticationBloc;

  @override
  void initState() {
    // TODO: implement initState
    super.initState();
    _authenticationBloc = AuthenticationBloc();
    _authenticationBloc.checkSignIned();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      
      body: StreamBuilder<bool>(
          stream: _authenticationBloc.authenStream,
          initialData: false,
          builder: (context, snapshot) {
            print(snapshot.hasData);
            print(snapshot.connectionState);
            if (snapshot.hasData) {
              
              if (snapshot.data) {
                WidgetsBinding.instance.addPostFrameCallback((_) => {
                      Future.delayed(const Duration(seconds: 2), () {
                        Navigator.pushNamedAndRemoveUntil(
                            context, '/home', (Route<dynamic> route) => false);
                      })
                    });
              } else {
                WidgetsBinding.instance.addPostFrameCallback((_) => {
                      Future.delayed(const Duration(seconds: 2), () {
                        Navigator.pushNamedAndRemoveUntil(context, '/loading',
                            (Route<dynamic> route) => false);
                      })
                    }); 
              }
            }

            return GradientBackground();
          }),
    );
  }
}
