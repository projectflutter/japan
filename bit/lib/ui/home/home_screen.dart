import 'package:barcode_scan/barcode_scan.dart';
import 'package:flutter/material.dart';
import 'package:localbit/%08models/profile_response.dart';
import 'package:localbit/blocs/generic_bloc_provider.dart';
import 'package:localbit/blocs/home_page_bloc.dart';
import 'package:localbit/blocs/home_screen_bloc.dart';
import 'package:localbit/ui/account/account_screen.dart';
import 'package:localbit/ui/addfuns/addfuns_screen.dart';
import 'package:localbit/ui/exchange/exchange_screen.dart';
import 'package:localbit/ui/home/widget/header_menu.dart';
import 'package:localbit/ui/parent_provider.dart';
import 'package:localbit/ui/qrcode/qrcode_screen.dart';
import 'package:localbit/ui/qrcode/scan_qrcode_screen.dart';
import 'package:localbit/ui/transfer/transfer_screen.dart';
import 'package:localbit/utils/uidata.dart';
import 'package:localbit/widgets/%1Bgradient_background.dart';
import 'package:localbit/widgets/residemenu.dart';
import 'package:localbit/widgets/yesno_dialog.dart';
import 'package:flutter/services.dart';
import 'home_page.dart';
import 'widget/card_bottom.dart';

class HomeScreen extends StatefulWidget {
  HomeScreen({Key key}) : super(key: key);

  _HomeScreenState createState() => _HomeScreenState();
}

class _HomeScreenState extends State<HomeScreen> with TickerProviderStateMixin {
  MenuController _menuController;
  int _currentIndex = 2;
  PageController _tabController;
  HomeScreenBloc _homeScreenBloc;
  bool isReload = false;
  bool isFocusHome = true;
  bool isReloadHome = false;
  String barcode = "";
  @override
  void initState() {
    super.initState();
    _homeScreenBloc = HomeScreenBloc();
    _homeScreenBloc.getProfile();
    _menuController =
        new MenuController(vsync: this, direction: ScrollDirection.LEFT);
    _tabController = new PageController(initialPage: _currentIndex);
  }

  @override
  Widget build(BuildContext context) {
    print('setstateHomescreen');
    Widget buildItem(String msg, String imageasset, int index) {
      return new Material(
        color: Colors.transparent,
        child: new InkWell(
          child: ResideMenuItem(
              title: '$msg',
              titleStyle: TextStyle(color: Colors.white),
              icon: Container(
                height: 40,
                child: Center(
                  child: Image.asset(
                    imageasset,
                    width: 28,
                    height: 28,
                  ),
                ),
              )),
          onTap: () {
            _menuController.closeMenu();
            handleClickItem(index, context);
          },
        ),
      );
    }

    return Scaffold(
      resizeToAvoidBottomPadding: false,
      body: Stack(
        children: <Widget>[
          GradientBackground(),
          StreamBuilder<ProfileResponse>(
              stream: _homeScreenBloc.profileStream,
              builder: (context, snapshot) {
                return ResideMenu.scaffold(
                  enable3dRotate: true,
                  controller: _menuController,
                  leftScaffold: new MenuScaffold(
                    header: _headerMenu(snapshot),
                    children: <Widget>[
                      buildItem("Home", "assets/images/ic_home.png", 0),
                      buildItem("Profile", "assets/images/ic_profile.png", 1),
                      buildItem(
                          "Scan QRCode", "assets/images/ic_qrcode.png", 2),
                      buildItem("Setting", "assets/images/ic_setting.png", 3),
                      buildItem("Activity", "assets/images/ic_activity.png", 4),
                      buildItem("Logout", "assets/images/ic_logout.png", 5)
                    ],
                    footer: Container(
                      padding: EdgeInsets.only(
                          top: MediaQuery.of(context).size.height * 0.14),
                      child: Center(
                          child: Text(UIData.copyright,
                              style: TextStyle(color: Colors.white))),
                    ),
                  ),
                  child: Stack(
                    children: <Widget>[
                      GradientBackground(),
                      Column(
                        children: <Widget>[
                          new AppBar(
                            backgroundColor: Colors.transparent,
                            elevation: 0.0,
                            centerTitle: true,
                            leading: new GestureDetector(
                              child: const Icon(Icons.menu),
                              onTap: () {
                                _menuController.openMenu(true);
                              },
                            ),
                            title: new Image.asset(
                              'assets/images/image_appbar.png',
                              fit: BoxFit.cover,
                              scale: 5,
                            ),
                          ),
                          Container(
                            margin: EdgeInsets.only(left: 5.0, right: 5.0),
                            height: 1.0,
                            color: Colors.white,
                          ),
                          Expanded(child: _buildPageView()),
                          bottomNav(),
                        ],
                      )
                    ],
                  ),
                );
              }),
        ],
      ),
    );
  }

  Widget bottomNav() {
    return Container(
      margin: EdgeInsets.all(8.0),
      child: Row(
        mainAxisAlignment: MainAxisAlignment.spaceBetween,
        crossAxisAlignment: CrossAxisAlignment.end,
        children: <Widget>[
          CardBottom(
            onTapSelected: () {
              onTapSelected(0);
            },
            backgroundColor: UIData.colorItemBottom1(),
            icon: Image.asset(
              'assets/images/ic_transfer.png',
              scale: 6,
            ),
            text: "Transfer",
          ),
          CardBottom(
            onTapSelected: () {
              onTapSelected(1);
            },
            backgroundColor: UIData.colorItemBottom2(),
            icon: Image.asset(
              'assets/images/ic_exchange.png',
              scale: 6,
            ),
            text: "Exchange",
          ),
          Container(
            child: CardBottom(
              onTapSelected: () {
                onTapSelected(2);
              },
              height: 72,
              width: 72,
              backgroundColor: UIData.colorItemBottom3(),
              icon: Image.asset(
                'assets/images/ic_qrcode.png',
                scale: 6,
              ),
              text: "QR Code",
            ),
          ),
          CardBottom(
            onTapSelected: () {
              onTapSelected(3);
            },
            backgroundColor: UIData.colorItemBottom4(),
            icon: Image.asset(
              'assets/images/ic_addfund.png',
              scale: 6,
            ),
            text: "Add fund",
          ),
          CardBottom(
            onTapSelected: () {
              onTapSelected(4);
            },
            backgroundColor: UIData.colorItemBottom5(),
            icon: Image.asset(
              'assets/images/ic_account.png',
              scale: 6,
            ),
            text: "Account",
          ),
        ],
      ),
    );
  }

  Widget _buildPageView() {
    return BlocProvider<HomePageBloc>(
      bloc: HomePageBloc(),
      child: ParentProvider(
        listener: _handleListener,
        reloadwallet: _reloadwallet,
        clearFlag: _clearFlag,
        child: Builder(builder: (context) {
          return DefaultTabController(
            initialIndex: _currentIndex,
            length: 5,
            child: isFocusHome
                ? PageView(
                    physics: NeverScrollableScrollPhysics(),
                    controller: _tabController,
                    children: [
                      TransferScreen(),
                      ExchangeScreen(),
                      HomePage(
                        isReloadWallet: isReload,
                      ),
                      AddfunsScreen(),
                      AccountScreen(),
                    ],
                    onPageChanged: (newPage) {
                      setState(() {
                        this._currentIndex = newPage;
                      });
                    },
                  )
                : QRCodeScreen(),
          );
        }),
      ),
    );
  }

  void onTapSelected(int index) async {
    if (index == 2) {
      setState(() {
        isFocusHome = false;
      });
    } else {
      setState(() {
        isFocusHome = true;
        _currentIndex = index;
      });
      this._tabController.jumpToPage(index);
    }
  }

  void _handleListener() {
    onTapSelected(3);
  }

  void _reloadwallet() async {
    print('re');
    setState(() {
      isReload = true;
      isFocusHome = true;
      _currentIndex = 2;
    });
    this._tabController.jumpToPage(_currentIndex);
  }

  void _clearFlag(bool isFlag) {
    isReload = isFlag;
  }

  void handleClickItem(int index, BuildContext context) {
    switch (index) {
      case 0:
        setState(() {
          isFocusHome = true;
          _currentIndex = 2;
        });
        this._tabController.jumpToPage(_currentIndex);
        break;
      case 1:
        _handleActivity(4);
        break;
      case 2:
        scan();

        break;
      case 3:
        _handleActivity(4);
        break;
      case 4:
        _handleActivity(0);
        break;
      case 5:
        _showdialogLogout(context);
        break;
      default:
    }
  }

  void _showdialogLogout(BuildContext context) {
    showDialog(
        context: context,
        builder: (context) => YesNoDialog(
              message: "Do you want logout app?",
              onCLickYes: () {
                _homeScreenBloc.clearToken();
                Navigator.pushNamedAndRemoveUntil(
                    context, '/loading', (Route<dynamic> route) => false);
              },
              onCLickNo: () {
                Navigator.pop(context);
              },
            ));
  }

  void _handleActivity(int index) async {
    await Future.delayed(Duration(microseconds: 300));
    this._tabController.animateToPage(index,
        duration: const Duration(milliseconds: 200), curve: Curves.easeInOut);
  }

  Widget _headerMenu(AsyncSnapshot<ProfileResponse> snapshot) {
    String avatar = "";
    String username = "";
    if (snapshot.hasData) {
      try {
        avatar = snapshot.data.data.profile.avatar;
        username = snapshot.data.data.profile.firstName +
            " " +
            snapshot.data.data.profile.lastName;
      } catch (e) {}
    }
    return HeaderMenu(
      urlImage: avatar,
      username: username,
    );
  }

  Future scan() async {
    try {
      String barcode = await BarcodeScanner.scan();
      await Future.delayed(Duration(microseconds: 300));
// Navigator.pushNamed(context, '/scanresult',arguments: barcode);
      Navigator.of(context).push(MaterialPageRoute(
          builder: (context) => ScanQRCodeScreen(barcode: barcode)));
      // setState(() {
      //   print('barcode: $barcode');
      //   if(barcode.isNotEmpty){
      //   this.barcode = barcode;

      //   }else{
      //     this.barcode="cancel";
      //   }
      // });
    } on PlatformException catch (e) {
      print("setstateexception");
      if (e.code == BarcodeScanner.CameraAccessDenied) {
        setState(() {
          this.barcode = 'The user did not grant the camera permission!';
        });
      } else {
        setState(() => this.barcode = 'Unknown error: $e');
      }
    } on FormatException {
      print("setstateFormatException");
      // setState(() => this.barcode =
      //     'null (User returned using the "back"-button before scanning anything. Result)');
    } catch (e) {
      print("setstateeeeeexception");
      // setState(() => this.barcode = 'Unknown error: $e');
    }
  }

  @override
  void dispose() {
    super.dispose();
    _menuController.dispose();
    _homeScreenBloc.dispose();
  }
}
