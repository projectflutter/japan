import 'dart:async';

import 'package:flutter/material.dart';
import 'package:localbit/%08models/curencies_response.dart';
import 'package:localbit/%08models/fiats_response.dart';
import 'package:localbit/%08models/wallet_balance_response.dart';
import 'package:localbit/blocs/generic_bloc_provider.dart';
import 'package:localbit/blocs/home_page_bloc.dart';
import 'package:localbit/ui/home/widget/list_coin.dart';
import 'package:localbit/ui/home/widget/wallet.dart';
import 'package:localbit/ui/parent_provider.dart';
import 'package:localbit/utils/api_subscription.dart';
import 'package:localbit/utils/constant.dart';
import 'package:localbit/utils/fetch_process.dart';
import 'package:localbit/utils/utilmath.dart';
import 'package:localbit/widgets/%1Bgradient_background.dart';
import 'package:localbit/widgets/wallet_provider.dart';

import 'widget/snap_top_list.dart';

class HomePage extends StatefulWidget {
  static WalletProvider of(BuildContext context) =>
      context.inheritFromWidgetOfExactType(WalletProvider) as WalletProvider;
  bool isReloadWallet;
  HomePage({
    Key key,
    this.isReloadWallet = false,
  }) : super(key: key);

  _HomePageState createState() => _HomePageState();
}

class _HomePageState extends State<HomePage>
    with AutomaticKeepAliveClientMixin<HomePage> {
  HomePageBloc _homeBloc;
  List<Item> listTitlePrice = [];
  List<Fiats> listFiats = [];
  List<Currencies> listCurrencies = [];
  StreamSubscription<FetchProcess> apiStreamSubscription;
  int _currentIndexFiat = -1;
  int _lastCurrentIndexFiat = 0;
  String symbol = "";
  Fiats _currentFiats;
  @override
  void initState() {
    super.initState();
    print('reinit');
    _homeBloc = BlocProvider.of(context);
    _currentFiats = _homeBloc.getCurrentFiats();
    _homeBloc.getWalletBalance();
    _homeBloc.getFiats();
    apiStreamSubscription = apiSubscription(_homeBloc.apiResult, context);
  }

  @override
  void didChangeDependencies() {
    super.didChangeDependencies();
  }

  @override
  Widget build(BuildContext context) {
    super.build(context);
    print('Rebuild HomePage');

    Widget _buildPriceWallet() {
      print('Rebuild Wallet');
      return StreamBuilder<WalletBalanceResponse>(
          stream: _homeBloc.getWalletBalanceStream,
          builder: (context, snapshot) {
            if (widget.isReloadWallet) {
              widget.isReloadWallet = false;
              ParentProvider.of(context).clearFlag(false);
              _homeBloc.getWalletBalance();
              WalletBalanceResponse temp = WalletBalanceResponse();
              temp.status = -1;
              _homeBloc.getWalletBalanceController.add(temp);
            }
            if (snapshot.hasData && snapshot.data.status != -1) {
              Fiats fiats = _homeBloc.getCurrentFiats();

              return WalletHeader(
                symbol: fiats != null ? fiats.symbol : "",
                balance: snapshot.hasData
                    ? getPrice(snapshot.data.data.balance, fiats)
                    : 0.00,
              );
            } else if (snapshot.hasError) {
              return Center(child: Text("Has error get Wallet!"));
            } else if (snapshot.error != null) {
              return Center(child: Text("Data get Wallet null!"));
            } else {
              return Center(child: CircularProgressIndicator());
            }
          });
    }

    return Stack(children: <Widget>[
      GradientBackground(),
      Column(
        children: <Widget>[
          _buildPriceWallet(),
          Container(
            child: StreamBuilder<FiatsResponse>(
                stream: _homeBloc.getFiatsStream,
                builder: (context, snapshot) {
                  if (snapshot.hasData) {
                    listFiats = snapshot.data.data.fiats;
                    int indexFiats = getCurentFiat(listFiats, _currentFiats);
                    if (_currentIndexFiat == indexFiats ||
                        _currentIndexFiat == -1) {
                      _currentIndexFiat = indexFiats;
                    }
                    if (listTitlePrice.length == 0) {
                      _homeBloc
                          .getCurrencies(listFiats[_currentIndexFiat].name);
                    }
                    listTitlePrice = convertList(listFiats);
                    print("fiat  ${snapshot.data.status}");

                    return Column(
                      children: <Widget>[
                        SnapTopListHome(
                          list: listTitlePrice,
                          currentIndex: _currentIndexFiat,
                          onPress: (index) {
                            _homeBloc.saveCurrentFiat(listFiats[index]);
                            _homeBloc.getCurrencies(listFiats[index].name);
                            _lastCurrentIndexFiat = _currentIndexFiat;
                            setState(() {
                              print(' abc $index');
                              _currentIndexFiat = index;
                            });
                          },
                        ),
                      ],
                    );
                  } else if (snapshot.hasError) {
                    return Center(child: Text("Has error get fiats!"));
                  } else if (snapshot.error != null) {
                    return Center(child: Text("Data get fiats null!"));
                  } else {
                    return Center(child: CircularProgressIndicator());
                  }
                }),
          ),
          Container(
            child: StreamBuilder<CurrenciesResponse>(
                stream: _homeBloc.getCurrenciesStream,
                builder: (context, snapshot) {
                  if (snapshot.hasData) {
                    if (snapshot.data.status == Constant.SUCESS_CODE) {
                      int curentfiat;
                      if (snapshot.data.data.currencies.isNotEmpty &&
                          listCurrencies.isNotEmpty &&
                          listCurrencies[0].rateUsd !=
                              snapshot.data.data.currencies[0].rateUsd) {
                        curentfiat = _currentIndexFiat;
                      } else {
                        curentfiat = _lastCurrentIndexFiat;
                      }
                      listCurrencies = snapshot.data.data.currencies;
                      return Expanded(
                        child: ListLiveCoin(
                          currencies: listCurrencies,
                          currentFiat: curentfiat,
                          listFiats: listFiats,
                          homePageBloc: _homeBloc,
                          onChangeList: (newList) {},
                        ),
                      );
                    } else {
                      return Center(
                        child: Text(
                          "${snapshot.data.message}",
                          style: TextStyle(color: Colors.white),
                        ),
                      );
                    }
                  } else if (snapshot.hasError) {
                    try {
                      int indexFiats = getCurentFiat(listFiats, _currentFiats);
                      _currentIndexFiat = indexFiats;
                    } catch (e) {}

                    return Center(child: Text("Has error get currencies!"));
                  } else if (snapshot.error != null) {
                    return Center(child: Text("Data get currencies null!"));
                  } else {
                    return Center(child: Container());
                  }
                }),
          )
        ],
      ),
    ]);
  }

  List<Item> convertList(List<Fiats> fiats) {
    List<Item> list = [];
    for (var i = 0; i < fiats.length; i++) {
      Item temp = Item();
      temp.index = i;
      if (i == _currentIndexFiat) {
        temp.selected = true;
      } else {
        temp.selected = false;
      }
      temp.title = fiats[i].name;
      list.add(temp);
    }
    return list;
  }

  double getPrice(List<Balance> balance, Fiats fiats) {
    try {
      for (var item in balance) {
        if (fiats.name.toUpperCase() == item.symbol.toUpperCase()) {
          return item.balance;
        }
      }
    } catch (e, stacktrace) {
      return 0.000;
    }
  }

  @override
  void dispose() {
    super.dispose();
    apiStreamSubscription?.cancel();
    _homeBloc.dispose();
  }

  @override
  bool get wantKeepAlive => true;
}
