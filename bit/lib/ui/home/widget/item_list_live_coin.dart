import 'package:flutter/material.dart';
import 'package:localbit/%08models/curencies_response.dart';
import 'package:localbit/%08models/fiats_response.dart';
import 'package:localbit/utils/uidata.dart';
import 'package:localbit/utils/utilmath.dart';

class ItemListLiveCoin extends StatelessWidget {
  final Currencies currenci;
  final Fiats fiat;
  ItemListLiveCoin({Key key, this.currenci, this.fiat}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    Widget itemList(Currencies currenci, Fiats fiat) {
      String symboy = fiat != null ? fiat.symbol : "\$";
      double price = currenci.rateUsd ;
      return Container(
        decoration: BoxDecoration(
          border: Border.all(color: UIData.colorItem(), width: 1.0),
          color: UIData.colorItem(),
          borderRadius: BorderRadius.all(
            Radius.circular(5.0),
          ),
          boxShadow: <BoxShadow>[
            new BoxShadow(
              color: Colors.black54,
              blurRadius: 3.0,
              offset: new Offset(0.0, 3.0),
            ),
          ],
        ),
        margin: EdgeInsets.only(left: 15.0, right: 15.0, top: 5.0, bottom: 5.0),
        height: MediaQuery.of(context).size.height / 10,
        child: Row(
          children: <Widget>[
            Container(
                padding: EdgeInsets.only(
                    top: 14.0, left: 2.0, right: 2.0, bottom: 14.0),
                height: double.infinity,
                child: FadeInImage(
                  image: NetworkImage(
                    '${currenci.logo}',
                  ),
                  placeholder: AssetImage("assets/images/ic_placeholder.png"),
                )),
            Text(
              currenci.name,
              maxLines: 1,
              style: TextStyle(
                color: Colors.white,
              ),
              overflow: TextOverflow.ellipsis,
              softWrap: false,
            ),
            Expanded(
              child: Container(
                child: Row(
                  mainAxisAlignment: MainAxisAlignment.center,
                  children: <Widget>[
                    Text(
                      "24h",
                      style: TextStyle(
                        color: Colors.white,
                      ),
                    ),
                    Padding(
                      padding: const EdgeInsets.symmetric(horizontal: 4.0),
                      child: Text(
                        '${currenci.priceChangePercent}%',
                        style: TextStyle(color: Colors.green), overflow: TextOverflow.ellipsis,
                      ),
                    ),
                  ],
                ),
              ),
            ),
            Padding(
              padding: const EdgeInsets.only(right: 16.0),
              child: Text(
                "$symboy ${getNumberFormat(price)}",
                style: TextStyle(color: Colors.white), overflow: TextOverflow.ellipsis,
              ),
            )
          ],
        ),
      );
    }

    return itemList(currenci, fiat);
  }

  String getNumberFormat(double price) {
    return formatNumber(price);
  }
}
