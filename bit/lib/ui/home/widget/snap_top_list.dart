import 'package:flutter/material.dart';

import 'snapping_list_view.dart';

typedef OnPress(int index);

class SnapTopListHome extends StatefulWidget {
  final List<Item> list;
  final OnPress onPress;
  int currentIndex;
  SnapTopListHome({Key key, this.list, this.onPress, this.currentIndex })
      : super(key: key);

  _SnapTopListHomeState createState() => _SnapTopListHomeState();
}

class _SnapTopListHomeState extends State<SnapTopListHome> {
  ScrollController _scrollController;
  double widthScreen;
  double widthItem;

  @override
  void initState() {
    // TODO: implement initState
    super.initState();
    _scrollController = new ScrollController();
  }

  @override
  Widget build(BuildContext context) {
    widthScreen = MediaQuery.of(context).size.width;
    return Row(
      children: <Widget>[
        GestureDetector(
          onTap: () {
            _decrement();
          },
          child: RotatedBox(
            child: Icon(
              Icons.play_arrow,
              color: Colors.white,
            ),
            quarterTurns: 2,
          ),
        ),
        Expanded(
          child: SizedBox(height: 30, child: _buildList()),
        ),
        GestureDetector(
          onTap: () {
            _increment();
          },
          child: RotatedBox(
            child: Icon(
              Icons.play_arrow,
              color: Colors.white,
            ),
            quarterTurns: 0,
          ),
        )
      ],
    );
  }

  Widget _buildList() {
    widthItem = (widthScreen - 60) / 5;
    return SnappingListView.builder(
      itemCount: widget.list.length,
      controller: _scrollController,
      scrollDirection: Axis.horizontal,
      itemBuilder: (BuildContext context, int index) {
        return InkWell(
            onTap: () {
              _clickSelectedItem(index);
            },
            child: itemList(index));
      },
      itemExtent: widthItem,
    );
  }

  void _clickSelectedItem(int index) {
   
    widget.currentIndex = index;
    setState(() {
      for (int i = 0; i < widget.list.length; i++) {
        if (i == widget.currentIndex) {
          widget.list[i].selected = true;
        } else {
          widget.list[i].selected = false;
        }
      }
       widget.onPress(index);
    });
  }

  Widget itemList(int index) {
    return ItemLayout(item: widget.list[index]);
  }

  void _decrement() {
    setState(() {
      if (widget.currentIndex > 0) {
        widget.currentIndex = widget.currentIndex - 1;
      }
      _clickSelectedItem(widget.currentIndex);
      scroll();
    });
  }

  void _increment() {
    setState(() {
      if (widget.currentIndex < widget.list.length - 1) {
        widget.currentIndex = widget.currentIndex + 1;
      }
      _clickSelectedItem(widget.currentIndex);
      scroll();
    });
  }

  void scroll() {
    for (int i = 0; i < widget.list.length; i++) {
      if (widget.list.elementAt(i).selected) {
        _scrollController.animateTo(i * widthItem,
            duration: new Duration(milliseconds: 600), curve: Curves.ease);
        break;
      }
    }
  }
}

class ItemLayout extends StatelessWidget {
  final double widthItem;
  final Item item;
  ItemLayout({Key key, this.widthItem, this.item}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return item.selected
        ? Container(
            color: Colors.white,
            child: Center(
                child: Text(
              '${item.title}',
              style: TextStyle(color: Colors.blue),
            )),
          )
        : Container(
            child: Center(
                child: Text(
              '${item.title}',
              style: TextStyle(color: Colors.white),
            )),
          );
  }
}

class Item {
  String title;
  bool selected;
  int index;
  Item({this.title, this.selected, this.index});
}
