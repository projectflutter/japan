import 'dart:convert';

import 'package:adhara_socket_io/adhara_socket_io.dart';
import 'package:adhara_socket_io/manager.dart';
import 'package:adhara_socket_io/socket.dart';
import 'package:flutter/material.dart';
import 'package:localbit/%08models/curencies_response.dart';
import 'package:localbit/%08models/fiats_response.dart';
import 'package:localbit/%08models/live_coin_rate.dart';
import 'package:localbit/blocs/home_page_bloc.dart';
import 'package:localbit/ui/home/widget/item_list_live_coin.dart';

typedef OnChangeList(List<Currencies> currencies);

class ListLiveCoin extends StatefulWidget {
  final List<Currencies> currencies;
  final List<Fiats> listFiats;
  final int currentFiat;
  final HomePageBloc homePageBloc;
  final OnChangeList onChangeList;
  ListLiveCoin(
      {Key key,
      this.currencies,
      this.listFiats,
      this.currentFiat,
      this.homePageBloc,
      this.onChangeList})
      : super(key: key);

  _ListLiveCoinState createState() => _ListLiveCoinState();
}

class _ListLiveCoinState extends State<ListLiveCoin> {
  SocketIOManager manager;
  SocketIO socket;
  @override
  void initState() {
    super.initState();
    manager = SocketIOManager();

    initSocketIO();
  }

  initSocketIO() async {
    socket = await manager.createInstance(
      SocketOptions(
        'https://trippay.vutatech.vn',
        enableLogging: true,
      ),
    ); //TODO change the port  accordingly
    socket.onConnect((data) {
      print("connected...");
      print(data);
      socket.emit("message", ["Hello world!"]);
    });
    // socket.onConnectError(pprint("onConnectError"));
    // socket.onConnectTimeout(pprint("onConnectTimeout"));
    // socket.onError(pprint('onError'),);
    // socket.onDisconnect(pprint);
    socket.on("rate:message", (data) {
      try {
        print('rate:message');
        List<LiveCoinRate> list = new List<LiveCoinRate>();
        data.map((item) => list.add(LiveCoinRate.fromJson(item))).toList();
        if (this.mounted) {
          setState(() {
            _handleData(list);
          });
        }
      } catch (e) {
        print('error socket');
        print(e);
      }
    });
    socket.connect();
  }

  pprint(data) {
    if (data is Map) {
      data = json.encode(data);
    }
    // print(data);
  }

  _disconnectSocket() async {
    await manager.clearInstance(socket);
  }

  @override
  Widget build(BuildContext context) {
    return _buildContentList();
  }

  Widget _buildContentList() {
    return Scrollbar(
      child: Padding(
        padding: const EdgeInsets.only(top: 8.0),
        child: MediaQuery.removePadding(
          removeTop: true,
          child: ListView.builder(
            itemCount: widget.currencies.length,
            itemBuilder: (BuildContext context, int index) {
              return InkWell(
                  onTap: () {},
                  child: ItemListLiveCoin(
                      currenci: widget.currencies[index],
                      fiat: widget.listFiats.length > 0
                          ? widget.listFiats[widget.currentFiat]
                          : null));
            },
          ),
          context: context,
        ),
      ),
    );
  }

  @override
  void dispose() {
    super.dispose();
    _disconnectSocket();
  }

  void _handleData(List<LiveCoinRate> listLiveCoin) {
    // spiderman code very amazing;

    for (var i = 0; i < widget.currencies.length; i++) {
      for (var j = 0; j < listLiveCoin.length; j++) {
        if (listLiveCoin[j].symbol.toUpperCase() ==
            widget.currencies[i].symbol.toUpperCase()) {
          //percent
          widget.currencies[i].priceChangePercent =
              listLiveCoin[j].priceChangePercent;

          //price
          double usdPrice = listLiveCoin[j].usdBuy +
              (listLiveCoin[j].usdBuy *
                  widget.listFiats[widget.currentFiat].askPercent /
                  100);
          widget.currencies[i].rateUsd =
              usdPrice * widget.listFiats[widget.currentFiat].usdConvert;
        }
      }
    }
    widget.onChangeList(widget.currencies);
  }
}
