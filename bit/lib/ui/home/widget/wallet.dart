import 'package:flutter/material.dart';
import 'package:localbit/utils/utilmath.dart';

class WalletHeader extends StatelessWidget {
  final String symbol;
  final double balance;
  WalletHeader({Key key, this.symbol, this.balance}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    String getNumberFormat(double price) {
      return formatNumber(price);
    }

    return Container(
      child: Padding(
        padding: const EdgeInsets.all(8.0),
        child: Column(
          mainAxisAlignment: MainAxisAlignment.center,
          children: <Widget>[
            Text(
              "Wallet Balance",
              style: TextStyle(color: Colors.white, fontSize: 18.0),
            ),
            Container(
              height: MediaQuery.of(context).size.height * 0.1,
              child: Align(
                alignment: Alignment.center,
                child: Padding(
                  padding: const EdgeInsets.all(8.0),
                  child: Text(
                    balance != null
                        ? '$symbol ${getNumberFormat(balance)}'
                        : "",
                    style: TextStyle(
                        color: Colors.white,
                        fontSize: 25.0,
                        fontWeight: FontWeight.bold),
                  ),
                ),
              ),
            ),
          ],
        ),
      ),
    );
  }
}
