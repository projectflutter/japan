import 'package:flutter/material.dart';

class HeaderMenu extends StatelessWidget {
  final String urlImage;
  final String username;
  HeaderMenu({Key key, this.urlImage, this.username}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Column(
      children: <Widget>[
        new Container(
            decoration: BoxDecoration(
              color: Colors.red,
              border: Border.all(color: Colors.white, width: 2.0),
              shape: BoxShape.circle,
            ),
            // constraints:
            //     new BoxConstraints(maxHeight: 128.0, maxWidth: 128.0),
            child: CircleAvatar(
              radius: 45,
              backgroundImage: 
              urlImage.isNotEmpty
                  ? NetworkImage(
                      urlImage,
                    )
                  : 
                  AssetImage("assets/images/bg_reside_menu.png"),
            )),
        Padding(
          padding: const EdgeInsets.all(4.0),
          child: Text(
            username.isNotEmpty ? username : 'User name',
            style: TextStyle(
                color: Colors.white,
                fontSize: 20.0,
                fontWeight: FontWeight.bold),
          ),
        )
      ],
    );
  }
}
