import 'package:flutter/material.dart';
typedef void OnTapSelected();

class CardBottom extends StatelessWidget {
  final Widget icon;
  final Color backgroundColor;
  final String text;
  final double width;
  final double height;
  final OnTapSelected onTapSelected;

  CardBottom(
      {Key key,
      @required this.icon,
      @required this.backgroundColor,
      @required this.text,
      this.height,
      this.width,
      this.onTapSelected})
      : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Flexible(
      flex: 1,
      child: InkWell(
        onTap: () {
          onTapSelected();
        },
        child: Container(
            margin: EdgeInsets.all(2.0),
            width: width,
            height: height,
            padding:
                EdgeInsets.only(left: 4.0, right: 4.0, top: 8.0, bottom: 8.0),
            decoration: BoxDecoration(
              color: backgroundColor,
              borderRadius: BorderRadius.all(
                Radius.circular(5.0),
              ),
              boxShadow: <BoxShadow>[
                new BoxShadow(
                  color: Colors.black26,
                  blurRadius: 1.0,
                  offset: new Offset(0.0, 3.0),
                ),
              ],
            ),
            child: Column(
              mainAxisAlignment: MainAxisAlignment.center,
              children: <Widget>[
                icon,
                Center(
                  child: Text(
                    text,
                    style: TextStyle(color: Colors.white, fontSize: 12.0),
                  ),
                )
              ],
            )),
      ),
    );
  }
}
