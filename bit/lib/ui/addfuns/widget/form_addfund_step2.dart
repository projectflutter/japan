import 'package:flutter/material.dart';
import 'package:localbit/%08models/key_value_model.dart';

class FormAddfundStep2 extends StatelessWidget {
  final List<KeyValueModel> list;
  const FormAddfundStep2({Key key, this.list}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Container(
      child: ListView.builder(
        itemBuilder: (BuildContext context, int index) {
          return _buildItemList(list[index]);
        },
        itemCount: list.length,
      ),
    );
  }

  Widget _buildItemList(KeyValueModel itemlist) {
    return Padding(
      padding: const EdgeInsets.all(8.0),
      child: Row(
        mainAxisAlignment: MainAxisAlignment.spaceBetween,
        children: <Widget>[
          Text(
            itemlist.tilteName,
            style: TextStyle(
                color: Colors.white,
                fontWeight:
                    itemlist.isBoldText ? FontWeight.bold : FontWeight.normal,
                fontSize: 12),
          ),
          Text(
            itemlist.value,
            style: TextStyle(
                color: Colors.white,
                fontWeight:
                    itemlist.isBoldText ? FontWeight.bold : FontWeight.normal,
                fontSize: 12),
          )
        ],
      ),
    );
  }
}
