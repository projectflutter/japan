import 'package:flutter/material.dart';
import 'package:localbit/%08models/fiats_response.dart';
import 'package:localbit/widgets/combobox.dart';
import 'package:localbit/widgets/combobox_background_white.dart';
import 'package:localbit/widgets/text_yellow_wallet.dart';

class FormAddfundStep1 extends StatelessWidget {
  final List<Fiats> listFiat;
  FormAddfundStep1({Key key, this.listFiat}) : super(key: key);
  Widget _buldForm2() {
    return Column(
      children: <Widget>[
        Text('My wallet',
            style: TextStyle(color: Colors.white, fontSize: 18.0)),
        SizedBox(
          height: 10,
        ),
        Container(width: 120, child: Combobox(listDropdownField: listFiat,)),
        SizedBox(
          height: 10,
        ),
        Text(
          "Wallet Balance",
          style: TextStyle(color: Colors.white, fontSize: 18.0),
        ),
        SizedBox(
          height: 10,
        ),
        TextYellowWallet(
          text: '\$300.25',
        ),
        SizedBox(
          height: 20,
        ),
        _buildForm3(),
      ],
    );
  }

  Widget _buildForm3() {
    return Container(
      margin: EdgeInsets.symmetric(horizontal: 20),
      child: Column(
        children: <Widget>[
          Row(
            mainAxisAlignment: MainAxisAlignment.spaceBetween,
            children: <Widget>[
              Flexible(
                flex: 4,
                child: Text(
                  "Method",
                  style: TextStyle(color: Colors.white, fontSize: 18.0),
                ),
              ),
              Flexible(flex: 6, child: ComboboxBgWhite()),
            ],
          ),
          SizedBox(
            height: 10,
          ),
          Row(
            mainAxisAlignment: MainAxisAlignment.spaceBetween,
            children: <Widget>[
              Flexible(
                flex: 4,
                child: Text(
                  "Add amount",
                  style: TextStyle(color: Colors.white, fontSize: 18.0),
                ),
              ),
              Flexible(
                flex: 6,
                child: TextField(
                  // controller: controller,
                  decoration: InputDecoration(
                    contentPadding: const EdgeInsets.all(10.0),
                    hintStyle: TextStyle(color: Colors.blue),
                    border: OutlineInputBorder(
                        borderRadius: BorderRadius.circular(3.0),
                        borderSide: BorderSide.none),
                    filled: true,
                    fillColor: Colors.white.withOpacity(1),
                  ),
                  keyboardType: TextInputType.number,
                ),
              ),
            ],
          ),
        ],
      ),
    );
  }

  @override
  Widget build(BuildContext context) {
    return _buldForm2();
  }
}
