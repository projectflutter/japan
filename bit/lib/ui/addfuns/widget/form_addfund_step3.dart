import 'package:flutter/material.dart';

class FormAddfundStep3 extends StatelessWidget {
  const FormAddfundStep3({Key key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    print("screen 3");
    return Container(
      child: Text(
        'Please transfer \$2000 and number \n 2423423423423432 in order to completete the transaction \n Thank you.',
        style: TextStyle(color: Colors.white, fontStyle: FontStyle.italic),
        textAlign: TextAlign.center,
      ),
    );
  }
}
