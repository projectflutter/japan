import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:localbit/%08models/addfund_step2.dart';
import 'package:localbit/%08models/fiats_response.dart';
import 'package:localbit/%08models/key_value_model.dart';
import 'package:localbit/blocs/exchange_bloc.dart';
import 'package:localbit/ui/addfuns/widget/form_addfund_step1.dart';
import 'package:localbit/ui/addfuns/widget/form_addfund_step2.dart';
import 'package:localbit/ui/addfuns/widget/form_addfund_step3.dart';
import 'package:localbit/ui/exchange/widget/button.dart';
import 'package:localbit/utils/uidata.dart';
import 'package:localbit/widgets/card.dart';
import 'package:localbit/widgets/text_header.dart';

enum TypeButtonClick { NEXT, BACK }

class AddfunsScreen extends StatefulWidget {
  AddfunsScreen({Key key}) : super(key: key);

  _AddfunsScreenState createState() => _AddfunsScreenState();
}

class _AddfunsScreenState extends State<AddfunsScreen>
    with AutomaticKeepAliveClientMixin<AddfunsScreen> {
  List<KeyValueModel> listStep2;
  int _currentIndex = 0;
  List<Widget> _children;
  PageController _tabController;
  double _height = 280;
  ExchangeBloc _exchangeBloc;

  @override
  void initState() {
    // TODO: implement initState
    super.initState();
    listStep2 = [];
    listStep2.add(KeyValueModel(
        tilteName: 'Account', value: '23637278384', isBoldText: false));
    listStep2.add(KeyValueModel(
        tilteName: 'Name',
        value: 'Dang Le Nguyen'.toUpperCase(),
        isBoldText: false));
    listStep2.add(KeyValueModel(
        tilteName: 'Add amount', value: '2000.00', isBoldText: false));
    listStep2.add(KeyValueModel(
        tilteName: 'Currency', value: 'USD'.toUpperCase(), isBoldText: false));
    listStep2.add(KeyValueModel(
        tilteName: 'Medthod', value: 'Trip Pay Walllet', isBoldText: false));
    listStep2.add(
        KeyValueModel(tilteName: 'Fee', value: '10 USD', isBoldText: false));
    listStep2.add(KeyValueModel(
        tilteName: 'Totalotal payment', value: '2010 USD', isBoldText: true));
    _tabController = new PageController(initialPage: _currentIndex);
    _exchangeBloc = ExchangeBloc();
    List<Fiats> list = _exchangeBloc.getListFiats();
    print(list.length);
    _children = [
      FormAddfundStep1(
        listFiat: list,
      ),
      FormAddfundStep2(
        list: listStep2,
      ),
      FormAddfundStep3(),
    ];
  }

  @override
  Widget build(BuildContext context) {
    return SingleChildScrollView(
      child: ConstrainedBox(
        child: CardCS(
          form: Column(
            children: <Widget>[
              _buildForm1(),
              _buildPageView(),
              SizedBox(
                height: 10,
              ),
              Row(
                mainAxisAlignment: MainAxisAlignment.center,
                children: <Widget>[
                  _currentIndex > 0
                      ? ButtonBorder(
                          color: Colors.white,
                          onPress: () {
                            _gotoStep(TypeButtonClick.BACK);
                          },
                          text: "Back",
                          textColor: Colors.blue,
                        )
                      : Container(),
                  _currentIndex < 2
                      ? ButtonBorder(
                          color: UIData.colorItemBottom3(),
                          onPress: () {
                            _gotoStep(TypeButtonClick.NEXT);
                          },
                          text: "Next",
                        )
                      : ButtonBorder(
                          color: UIData.colorItemBottom3(),
                          onPress: () {
                            _clickConfirm();
                          },
                          text: "Confirm",
                        )
                ],
              )
            ],
          ),
        ),
        constraints: BoxConstraints(),
      ),
    );
  }

  @override
  // TODO: implement wantKeepAlive
  bool get wantKeepAlive => true;

  Widget _buildForm1() {
    return TextHeader(
      text: "Add fund",
    );
  }

  void _gotoStep(TypeButtonClick type) {
    int index;
    setState(() {
      print(_currentIndex);
      if (type == TypeButtonClick.NEXT) {
        if (_currentIndex <= 2) index = _currentIndex + 1;
      } else {
        if (_currentIndex > 0) {
          index = _currentIndex - 1;
        }
      }
      this._tabController.animateToPage(index,
          duration: const Duration(milliseconds: 200), curve: Curves.easeInOut);
      editeHeightCard(index);
      _currentIndex = index;
    });
  }

  Widget _buildPageView() {
    return SizedBox(
      height: _height,
      child: DefaultTabController(
        initialIndex: 0,
        length: 3,
        child: PageView(
          physics: NeverScrollableScrollPhysics(),
          controller: _tabController,
          children: _children,
          onPageChanged: (newPage) {
            print(newPage);
            setState(() {
              this._currentIndex = newPage;
            });
          },
        ),
      ),
    );
  }

  void editeHeightCard(int index) {
    if (index == 1) {
      _height = 280;
    } else if (index == 2) {
      _height = 100;
    }
  }

  void _clickConfirm() {
    print("confirmclick");
  }
  @override
  void dispose() {
    super.dispose();
    _exchangeBloc.dispose();
  }
}
