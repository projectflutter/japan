import 'package:localbit/datasources/network/api_network_provider.dart';
import 'package:localbit/datasources/network/restclient.dart';

enum Flavor { DEV, PRO }

//Simple DI
class Injector {
  String baseUrlDev = "https://trippay.vutatech.vn";

  static final Injector _singleton = new Injector._internal();
  static Flavor _flavor;

  static void configure(Flavor flavor) async {
    _flavor = flavor;
  }

  factory Injector() => _singleton;

  Injector._internal();

  ApiNetworkProvider get apiService {
    print(_flavor);
    switch (_flavor) {
      case Flavor.DEV:
        return ApiNetworkProvider(new RestClient(baseUrlDev));
      default:
        // return ApiNetworkProvider(new RestClient(baseUrlPro));
    }
  }
}
