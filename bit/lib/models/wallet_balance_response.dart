class WalletBalanceResponse {
  int status;
  String message;
  Data data;

  WalletBalanceResponse({this.status, this.message, this.data});

  WalletBalanceResponse.fromJson(Map<String, dynamic> json) {
    status = json['status'];
    message = json['message'];
    data = json['data'] != null ? new Data.fromJson(json['data']) : null;
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['status'] = this.status;
    data['message'] = this.message;
    if (this.data != null) {
      data['data'] = this.data.toJson();
    }
    return data;
  }
}

class Data {
  List<Balance> balance;

  Data({this.balance});

  Data.fromJson(Map<String, dynamic> json) {
    if (json['balance'] != null) {
      balance = new List<Balance>();
      json['balance'].forEach((v) {
        balance.add(new Balance.fromJson(v));
      });
    }
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    if (this.balance != null) {
      data['balance'] = this.balance.map((v) => v.toJson()).toList();
    }
    return data;
  }
}

class Balance {
  String symbol;
  double balance;

  Balance({this.symbol, this.balance});

  Balance.fromJson(Map<String, dynamic> json) {
    symbol = json['symbol'];
    balance = json['balance'].toDouble();
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['symbol'] = this.symbol;
    data['balance'] = this.balance;
    return data;
  }
}
