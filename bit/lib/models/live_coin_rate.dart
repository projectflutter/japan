class LiveCoinRate {
  double usdBuy;
  double usdSell;
  double priceChangePercent;
  String symbol;
  String level;

  LiveCoinRate(
      {this.usdBuy,
      this.usdSell,
      this.priceChangePercent,
      this.symbol,
      this.level});

  LiveCoinRate.fromJson(Map<String, dynamic> json) {
    usdBuy = json['usd_buy'].toDouble();
    usdSell = json['usd_sell'].toDouble();
    priceChangePercent = json['price_change_percent'].toDouble();
    symbol = json['symbol'];
    level = json['level'];
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['usd_buy'] = this.usdBuy;
    data['usd_sell'] = this.usdSell;
    data['price_change_percent'] = this.priceChangePercent;
    data['symbol'] = this.symbol;
    data['level'] = this.level;
    return data;
  }
}

