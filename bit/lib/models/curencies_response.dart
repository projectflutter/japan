class CurrenciesResponse {
  int status;
  String message;
  Data data;

  CurrenciesResponse({this.status, this.message, this.data});

  CurrenciesResponse.fromJson(Map<String, dynamic> json) {
    status = json['status'];
    message = json['message'];
    data = json['data'] != null ? new Data.fromJson(json['data']) : null;
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['status'] = this.status;
    data['message'] = this.message;
    if (this.data != null) {
      data['data'] = this.data.toJson();
    }
    return data;
  }
}

class Data {
  List<Currencies> currencies;

  Data({this.currencies});

  Data.fromJson(Map<String, dynamic> json) {
    if (json['currencies'] != null) {
      currencies = new List<Currencies>();
      json['currencies'].forEach((v) {
        currencies.add(new Currencies.fromJson(v));
      });
    }
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    if (this.currencies != null) {
      data['currencies'] = this.currencies.map((v) => v.toJson()).toList();
    }
    return data;
  }
}

class Currencies {
  int id;
  String name;
  String symbol;
  int balance;
  String logo;
  String icon;
  String color;
  double actived;
  double depositFee;
  double depositMin;
  double withdrawFee;
  double withdrawMin;
  double usdBuy;
  double usdSell;
  double minBuy;
  double maxBuy;
  double minSell;
  double maxSell;
  double priceChangePercent;
  double rateUsd;

  Currencies(
      {this.id,
      this.name,
      this.symbol,
      this.balance,
      this.logo,
      this.icon,
      this.color,
      this.actived,
      this.depositFee,
      this.depositMin,
      this.withdrawFee,
      this.withdrawMin,
      this.usdBuy,
      this.usdSell,
      this.minBuy,
      this.maxBuy,
      this.minSell,
      this.maxSell,
      this.priceChangePercent,
      this.rateUsd});

  Currencies.fromJson(Map<String, dynamic> json) {
    id = json['id'];
    name = json['name'];
    symbol = json['symbol'];
    balance = json['balance'];
    logo = json['logo'];
    icon = json['icon'];
    color = json['color'];
    actived = json['actived'].toDouble();
    depositFee = json['deposit_fee'].toDouble();
    depositMin = json['deposit_min'].toDouble();
    withdrawFee = json['withdraw_fee'].toDouble();
    withdrawMin = json['withdraw_min'].toDouble();
    usdBuy = json['usd_buy'].toDouble();
    usdSell = json['usd_sell'].toDouble();
    minBuy = json['min_buy'].toDouble();
    maxBuy = json['max_buy'].toDouble();
    minSell = json['min_sell'].toDouble();
    maxSell = json['max_sell'].toDouble();
    priceChangePercent = json['price_change_percent'].toDouble();
    try {
      rateUsd = json['rate_usd'].toDouble();
    } catch (e) {
      rateUsd = 0.0;
    }
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['id'] = this.id;
    data['name'] = this.name;
    data['symbol'] = this.symbol;
    data['balance'] = this.balance;
    data['logo'] = this.logo;
    data['icon'] = this.icon;
    data['color'] = this.color;
    data['actived'] = this.actived;
    data['deposit_fee'] = this.depositFee;
    data['deposit_min'] = this.depositMin;
    data['withdraw_fee'] = this.withdrawFee;
    data['withdraw_min'] = this.withdrawMin;
    data['usd_buy'] = this.usdBuy;
    data['usd_sell'] = this.usdSell;
    data['min_buy'] = this.minBuy;
    data['max_buy'] = this.maxBuy;
    data['min_sell'] = this.minSell;
    data['max_sell'] = this.maxSell;
    data['price_change_percent'] = this.priceChangePercent;
    data['rate_usd'] = this.rateUsd;
    return data;
  }
}
