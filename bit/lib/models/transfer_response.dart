class TransferResponse {
  int status;
  String message;
  DataCreate data;

  TransferResponse({this.status, this.message, this.data});

  TransferResponse.fromJson(Map<String, dynamic> json) {
    status = json['status'];
    message = json['message'];
    data = json['data'] != null ? new DataCreate.fromJson(json['data']) : null;
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['status'] = this.status;
    data['message'] = this.message;
    if (this.data != null) {
      data['data'] = this.data.toJson();
    }
    return data;
  }
}

class DataCreate {
  TransferConfirmation transferConfirmation;

  DataCreate({this.transferConfirmation});

  DataCreate.fromJson(Map<String, dynamic> json) {
    transferConfirmation = json['transfer_confirmation'] != null
        ? new TransferConfirmation.fromJson(json['transfer_confirmation'])
        : null;
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    if (this.transferConfirmation != null) {
      data['transfer_confirmation'] = this.transferConfirmation.toJson();
    }
    return data;
  }
}

class TransferConfirmation {
  String transferId;
  String account;
  String name;
  int amount;
  String currency;
  int fee;

  TransferConfirmation(
      {this.transferId,
      this.account,
      this.name,
      this.amount,
      this.currency,
      this.fee});

  TransferConfirmation.fromJson(Map<String, dynamic> json) {
    transferId = json['transfer_id'];
    account = json['account'];
    name = json['name'];
    amount = json['amount'];
    currency = json['currency'];
    fee = json['fee'];
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['transfer_id'] = this.transferId;
    data['account'] = this.account;
    data['name'] = this.name;
    data['amount'] = this.amount;
    data['currency'] = this.currency;
    data['fee'] = this.fee;
    return data;
  }
}
