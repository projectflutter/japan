class RegisterResponse {
  int status;
  String message;
  DataRegister data;

  RegisterResponse({this.status, this.message, this.data});

  RegisterResponse.fromJson(Map<String, dynamic> json) {
    status = json['status'];
    message = json['message'];
    data =
        json['data'] != null ? new DataRegister.fromJson(json['data']) : null;
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['status'] = this.status;
    data['message'] = this.message;
    if (this.data != null) {
      data['data'] = this.data.toJson();
    }
    return data;
  }
}

class DataRegister {
  String token;
  String firstName;
  String lastName;

  DataRegister({this.token, this.firstName, this.lastName});

  DataRegister.fromJson(Map<String, dynamic> json) {
    token = json['token'];
    firstName = json['first_name'];
    lastName = json['last_name'];
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['token'] = this.token;
    data['first_name'] = this.firstName;
    data['last_name'] = this.lastName;
    return data;
  }
}
