class ConfirmTransferResponse {
  int status;
  String message;
  Data data;

  ConfirmTransferResponse({this.status, this.message, this.data});

  ConfirmTransferResponse.fromJson(Map<String, dynamic> json) {
    status = json['status'];
    message = json['message'];
    data = json['data'] != null ? new Data.fromJson(json['data']) : null;
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['status'] = this.status;
    data['message'] = this.message;
    if (this.data != null) {
      data['data'] = this.data.toJson();
    }
    return data;
  }
}

class Data {
  int recipientId;

  Data({this.recipientId});

  Data.fromJson(Map<String, dynamic> json) {
    recipientId = json['recipient_id'];
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['recipient_id'] = this.recipientId;
    return data;
  }
}

