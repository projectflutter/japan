class FiatsResponse {
  int status;
  String message;
  DataFiats data;

  FiatsResponse({this.status, this.message, this.data});

  FiatsResponse.fromJson(Map<String, dynamic> json) {
    status = json['status'];
    message = json['message'];
    data = json['data'] != null ? new DataFiats.fromJson(json['data']) : null;
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['status'] = this.status;
    data['message'] = this.message;
    if (this.data != null) {
      data['data'] = this.data.toJson();
    }
    return data;
  }
}

class DataFiats {
  List<Fiats> fiats;

  DataFiats({this.fiats});

  DataFiats.fromJson(Map<String, dynamic> json) {
    if (json['fiats'] != null) {
      fiats = new List<Fiats>();
      json['fiats'].forEach((v) {
        fiats.add(new Fiats.fromJson(v));
      });
    }
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    if (this.fiats != null) {
      data['fiats'] = this.fiats.map((v) => v.toJson()).toList();
    }
    return data;
  }
}

class Fiats {
  int id;
  String logo;
  String name;
  String symbol;
  double askPercent;
  double bidPercent;
  double usdConvert;
  String icon;
  int isDefault;
  int isActived;

  Fiats(
      {this.id,
      this.logo,
      this.name,
      this.symbol,
      this.askPercent,
      this.bidPercent,
      this.usdConvert,
      this.icon,
      this.isDefault,
      this.isActived});

  Fiats.fromJson(Map<String, dynamic> json) {
    id = json['id'];
    logo = json['logo'];
    name = json['name'];
    symbol = json['symbol'];
    askPercent = json['ask_percent'].toDouble();
    bidPercent = json['bid_percent'].toDouble();
    usdConvert = json['usd_convert'].toDouble();
    icon = json['icon'];
    isDefault = json['is_default'];
    isActived = json['is_actived'];
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['id'] = this.id;
    data['logo'] = this.logo;
    data['name'] = this.name;
    data['symbol'] = this.symbol;
    data['ask_percent'] = this.askPercent;
    data['bid_percent'] = this.bidPercent;
    data['usd_convert'] = this.usdConvert;
    data['icon'] = this.icon;
    data['is_default'] = this.isDefault;
    data['is_actived'] = this.isActived;
    return data;
  }
}
