class RegisterRequest {
  final String email;
  final String firstName;
  final String lastName;
  final String phone;
  final String address;
  final String country;
  final String password;
  final String confirmPassword;
  RegisterRequest(
    this.email,
    this.firstName,
    this.lastName,
    this.phone,
    this.address,
    this.country,
    this.password,
    this.confirmPassword,
  );
  Map<String, String> toJson() => {
        'email': email,
        'first_name': firstName,
        'last_name': lastName,
        'phone': phone,
        'address': address,
        'country': country,
        'password': password,
        'password_confirmation': confirmPassword,
      };
}
