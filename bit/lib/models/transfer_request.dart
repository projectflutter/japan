class TransferRequest {
  String recipient;
  double amount;
  String symbol;
  TransferRequest({this.recipient, this.amount, this.symbol});
  Map<String, String> toJson() => {
        'recipient': recipient,
        'amount': amount.toString(),
        'symbol': symbol,
      };
}
