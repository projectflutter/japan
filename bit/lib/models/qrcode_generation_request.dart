class QRCodeGenRequest {
  String symbol;
  String amount;
  String description;
  Map<String, String> toJson() => {
        'symbol': symbol,
        'amount': amount,
        'description': description,
      };
}
