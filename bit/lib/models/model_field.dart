import 'package:flutter/material.dart';
import 'package:localbit/ui/register/widgets/field_register.dart';

class RegisterListField {
  static List<CustomTextField> getlist() {
    return [
      CustomTextField(
        textTopField: "Email",
        controller: TextEditingController(),
      ),
      CustomTextField(
        textTopField: "FirstName",   controller: TextEditingController(),
      ),
      CustomTextField(
        textTopField: "LastName",   controller: TextEditingController(),
      ),
      CustomTextField(
        textTopField: "Phone",  controller: TextEditingController(), keyboardType: TextInputType.number
      ),
      CustomTextField(
        textTopField: "Adress",  controller: TextEditingController(),
      ),
      CustomTextField(
        textTopField: "Country",  controller: TextEditingController(),
      ),
      CustomTextField(
        textTopField: "Password",  controller: TextEditingController(), obscureText: true,
      ),
      CustomTextField(
        textTopField: "ConfirmPassword",  controller: TextEditingController(), obscureText: true,
      ),
    ];
  }
}

class TextFieldCS {
  CustomTextField cus;
  TextEditingController controller;
  TextFieldCS({this.cus, this.controller});
}
