class QRCodeGenResponse {
  int status;
  String message;
  Data data;

  QRCodeGenResponse({this.status, this.message, this.data});

  QRCodeGenResponse.fromJson(Map<String, dynamic> json) {
    status = json['status'];
    message = json['message'];
    data = json['data'] != null ? new Data.fromJson(json['data']) : null;
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['status'] = this.status;
    data['message'] = this.message;
    if (this.data != null) {
      data['data'] = this.data.toJson();
    }
    return data;
  }
}

class Data {
  QrRaw qrRaw;
  String qrText;

  Data({this.qrRaw, this.qrText});

  Data.fromJson(Map<String, dynamic> json) {
    qrRaw = json['qr_raw'] != null ? new QrRaw.fromJson(json['qr_raw']) : null;
    qrText = json['qr_text'];
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    if (this.qrRaw != null) {
      data['qr_raw'] = this.qrRaw.toJson();
    }
    data['qr_text'] = this.qrText;
    return data;
  }
}

class QrRaw {
  String amount;
  String symbol;
  String name;
  String userCode;
  String createdAt;
  String status;
  String requestNumber;
  String type;

  QrRaw(
      {this.amount,
      this.symbol,
      this.name,
      this.userCode,
      this.createdAt,
      this.status,
      this.requestNumber,
      this.type});

  QrRaw.fromJson(Map<String, dynamic> json) {
    amount = json['amount'];
    symbol = json['symbol'];
    name = json['name'];
    userCode = json['user_code'];
    createdAt = json['created_at'];
    status = json['status'];
    requestNumber = json['request_number'];
    type = json['type'];
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['amount'] = this.amount;
    data['symbol'] = this.symbol;
    data['name'] = this.name;
    data['user_code'] = this.userCode;
    data['created_at'] = this.createdAt;
    data['status'] = this.status;
    data['request_number'] = this.requestNumber;
    data['type'] = this.type;
    return data;
  }
}

