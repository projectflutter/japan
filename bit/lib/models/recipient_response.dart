class ListRecipientResponse {
  int status;
  String message;
  Data data;

  ListRecipientResponse({this.status, this.message, this.data});

  ListRecipientResponse.fromJson(Map<String, dynamic> json) {
    status = json['status'];
    message = json['message'];
    data = json['data'] != null ? new Data.fromJson(json['data']) : null;
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['status'] = this.status;
    data['message'] = this.message;
    if (this.data != null) {
      data['data'] = this.data.toJson();
    }
    return data;
  }
}

class Data {
  List<Recipients> recipients;

  Data({this.recipients});

  Data.fromJson(Map<String, dynamic> json) {
    if (json['recipients'] != null) {
      recipients = new List<Recipients>();
      json['recipients'].forEach((v) {
        recipients.add(new Recipients.fromJson(v));
      });
    }
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    if (this.recipients != null) {
      data['recipients'] = this.recipients.map((v) => v.toJson()).toList();
    }
    return data;
  }
}

class Recipients {
  int id;
  int userid;
  int recipientId;
  String recipientEmail;
  String recipientCode;
  String displayName;
  String createdAt;
  String updatedAt;

  Recipients(
      {this.id,
      this.userid,
      this.recipientId,
      this.recipientEmail,
      this.recipientCode,
      this.displayName,
      this.createdAt,
      this.updatedAt});

  Recipients.fromJson(Map<String, dynamic> json) {
    id = json['id'];
    userid = json['userid'];
    recipientId = json['recipient_id'];
    recipientEmail = json['recipient_email'];
    recipientCode = json['recipient_code'];
    displayName = json['display_name'];
    createdAt = json['created_at'];
    updatedAt = json['updated_at'];
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['id'] = this.id;
    data['userid'] = this.userid;
    data['recipient_id'] = this.recipientId;
    data['recipient_email'] = this.recipientEmail;
    data['recipient_code'] = this.recipientCode;
    data['display_name'] = this.displayName;
    data['created_at'] = this.createdAt;
    data['updated_at'] = this.updatedAt;
    return data;
  }
}

