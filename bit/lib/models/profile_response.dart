class ProfileResponse {
  int status;
  String message;
  Data data;

  ProfileResponse({this.status, this.message, this.data});

  ProfileResponse.fromJson(Map<String, dynamic> json) {
    status = json['status'];
    message = json['message'];
    data = json['data'] != null ? new Data.fromJson(json['data']) : null;
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['status'] = this.status;
    data['message'] = this.message;
    if (this.data != null) {
      data['data'] = this.data.toJson();
    }
    return data;
  }
}

class Data {
  Profile profile;

  Data({this.profile});

  Data.fromJson(Map<String, dynamic> json) {
    profile =
        json['profile'] != null ? new Profile.fromJson(json['profile']) : null;
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    if (this.profile != null) {
      data['profile'] = this.profile.toJson();
    }
    return data;
  }
}

class Profile {
  int id;
  String name;
  String firstName;
  String lastName;
  String email;
  int phoneCode;
  String phone;
  String country;
  String address;
  String passportId;
  String passportFront;
  String passportBack;
  String selfie;
  int ibanId;
  String avatar;
  String description;
  String roles;
  int adminBarFront;
  int socialId;
  String socialInformation;
  int userStatus;
  int profileActived;
  int infoActived;
  int phoneActived;
  int emailActived;
  int level;
  int newsletter;
  int lastLogin;
  String createdAt;
  String updatedAt;

  Profile(
      {this.id,
      this.name,
      this.firstName,
      this.lastName,
      this.email,
      this.phoneCode,
      this.phone,
      this.country,
      this.address,
      this.passportId,
      this.passportFront,
      this.passportBack,
      this.selfie,
      this.ibanId,
      this.avatar,
      this.description,
      this.roles,
      this.adminBarFront,
      this.socialId,
      this.socialInformation,
      this.userStatus,
      this.profileActived,
      this.infoActived,
      this.phoneActived,
      this.emailActived,
      this.level,
      this.newsletter,
      this.lastLogin,
      this.createdAt,
      this.updatedAt});

  Profile.fromJson(Map<String, dynamic> json) {
    id = json['id'];
    name = json['name'];
    firstName = json['first_name'];
    lastName = json['last_name'];
    email = json['email'];
    phoneCode = json['phone_code'];
    phone = json['phone'];
    country = json['country'];
    address = json['address'];
    passportId = json['passport_id'];
    passportFront = json['passport_front'];
    passportBack = json['passport_back'];
    selfie = json['selfie'];
    ibanId = json['iban_id'];
    avatar = json['avatar'];
    description = json['description'];
    roles = json['roles'];
    adminBarFront = json['admin_bar_front'];
    socialId = json['social_id'];
    socialInformation = json['social_information'];
    userStatus = json['user_status'];
    profileActived = json['profile_actived'];
    infoActived = json['info_actived'];
    phoneActived = json['phone_actived'];
    emailActived = json['email_actived'];
    level = json['level'];
    newsletter = json['newsletter'];
    lastLogin = json['last_login'];
    createdAt = json['created_at'];
    updatedAt = json['updated_at'];
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['id'] = this.id;
    data['name'] = this.name;
    data['first_name'] = this.firstName;
    data['last_name'] = this.lastName;
    data['email'] = this.email;
    data['phone_code'] = this.phoneCode;
    data['phone'] = this.phone;
    data['country'] = this.country;
    data['address'] = this.address;
    data['passport_id'] = this.passportId;
    data['passport_front'] = this.passportFront;
    data['passport_back'] = this.passportBack;
    data['selfie'] = this.selfie;
    data['iban_id'] = this.ibanId;
    data['avatar'] = this.avatar;
    data['description'] = this.description;
    data['roles'] = this.roles;
    data['admin_bar_front'] = this.adminBarFront;
    data['social_id'] = this.socialId;
    data['social_information'] = this.socialInformation;
    data['user_status'] = this.userStatus;
    data['profile_actived'] = this.profileActived;
    data['info_actived'] = this.infoActived;
    data['phone_actived'] = this.phoneActived;
    data['email_actived'] = this.emailActived;
    data['level'] = this.level;
    data['newsletter'] = this.newsletter;
    data['last_login'] = this.lastLogin;
    data['created_at'] = this.createdAt;
    data['updated_at'] = this.updatedAt;
    return data;
  }
}

