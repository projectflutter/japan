import 'dart:async';
import 'dart:convert';
import 'package:localbit/%08models/base_response.dart';
import 'package:localbit/%08models/confirm_transfer.dart';
import 'package:localbit/%08models/curencies_response.dart';
import 'package:localbit/%08models/fiats_response.dart';
import 'package:localbit/%08models/forgot_response.dart';
import 'package:localbit/%08models/login_response.dart';
import 'package:localbit/%08models/profile_response.dart';
import 'package:localbit/%08models/qrcode_generation_request.dart';
import 'package:localbit/%08models/qrcode_generation_response.dart';
import 'package:localbit/%08models/recipient_response.dart';
import 'package:localbit/%08models/register_response.dart';
import 'package:localbit/%08models/transfer_request.dart';
import 'package:localbit/%08models/transfer_response.dart';
import 'package:localbit/%08models/wallet_balance_response.dart';
import 'package:localbit/datasources/network/network_service_response.dart';
import 'package:localbit/di/di.dart';

import 'helper/preferences.dart';
import 'helper/sharepref_name.dart';

class UserRepository {
  UserRepository() {}
  final apiNetworkProvider = Injector().apiService;

  Future<NetworkServiceResponse<LoginResponse>> performLogin(loginRequest) =>
      apiNetworkProvider.performLogin(loginRequest);

  Future<NetworkServiceResponse<RegisterResponse>> performRegister(
          registerRequest) =>
      apiNetworkProvider.performRegister(registerRequest);

  Future<NetworkServiceResponse<ForgotPassResponse>> performForgotPass(
          registerRequest) =>
      apiNetworkProvider.performForgotPass(registerRequest);

  saveToken(String token) async {
    await Preference.load();
    Preference.setString(SharePrefName.TOKEN, token);
  }

  Future<bool> isSignedIn() async {
    await Preference.load();
    final currentUser = Preference.getString(SharePrefName.TOKEN);
    return currentUser != null;
  }

  Future<void> clearToken() async {
    await Preference.load();
    Preference.removeAll();
  }

  Future<NetworkServiceResponse<FiatsResponse>> getFiats() =>
      apiNetworkProvider.getFiats();

  Future<NetworkServiceResponse<CurrenciesResponse>> getCurrencies(
          String nameSymbol) =>
      apiNetworkProvider.getCurrencies(nameSymbol);

  Future<NetworkServiceResponse<ProfileResponse>> getProfile() =>
      apiNetworkProvider.getProfile();
  Future<NetworkServiceResponse<WalletBalanceResponse>> getWalletBalance() =>
      apiNetworkProvider.getWalletBalance();

  saveCurrentFiat(String stringFiat) async {
    await Preference.load();
    Preference.setString(SharePrefName.CURRENT_FIATS, stringFiat);
  }

  Fiats getCurrentFiat() {
    try {
      Preference.load();
      String fiatString = Preference.getString(SharePrefName.CURRENT_FIATS);
      var result = json.decode(fiatString);
      Fiats fiat = Fiats.fromJson(result);
      return fiat;
    } catch (e) {
      return null;
    }
  }

  saveListFiats(List<Fiats> fiats) async {
    await Preference.load();
    Preference.setString(SharePrefName.LIST_FIATS, json.encode(fiats));
  }

  List<Fiats> getListFiats() {
    Preference.load();
    String fiatString = Preference.getString(SharePrefName.LIST_FIATS);
    var result = json.decode(fiatString);
    List<Fiats> fiats = [];
    result.map((item) => fiats.add(Fiats.fromJson(item))).toList();
    return fiats;
  }

  saveListBalance(List<Balance> listbalance) async {
    await Preference.load();
    Preference.setString(SharePrefName.MY_WALLET, json.encode(listbalance));
  }

  List<Balance> getListBalance() {
    Preference.load();
    String fiatString = Preference.getString(SharePrefName.MY_WALLET);
    var result = json.decode(fiatString);
    List<Balance> fiats = [];
    result.map((item) => fiats.add(Balance.fromJson(item))).toList();
    return fiats;
  }

  Future<NetworkServiceResponse<TransferResponse>> createTransfer(
          TransferRequest transferRequest) =>
      apiNetworkProvider.createTransfer(transferRequest);

  Future<NetworkServiceResponse<ConfirmTransferResponse>> confirmTransfer(
          String transferId, String otp) =>
      apiNetworkProvider.confirmTransfer(transferId, otp);

  Future<NetworkServiceResponse<ListRecipientResponse>> getListRecipient() =>
      apiNetworkProvider.getListRecipient();
  Future<NetworkServiceResponse<BaseResponse>> saveRecipient(int recipientId) =>
      apiNetworkProvider.saveRecipient(recipientId);

  saveSymbolCurrencies(List<Currencies> currencies) async {
    await Preference.load();
    Preference.setString(
        SharePrefName.LIST_SYMBOL_CURRENCIES, json.encode(currencies));
  }

  List<Currencies> getSymbolCurrencies() {
    Preference.load();
    String currenciesString =
        Preference.getString(SharePrefName.LIST_SYMBOL_CURRENCIES);
    var result = json.decode(currenciesString);
    List<Currencies> currencies = [];
    result.map((item) => currencies.add(Currencies.fromJson(item))).toList();
    return currencies;
  }

  Future<NetworkServiceResponse<QRCodeGenResponse>> genCode(
          QRCodeGenRequest request) =>
      apiNetworkProvider.genCode(request);
}
