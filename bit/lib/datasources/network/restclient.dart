import 'dart:async';
import 'dart:convert';
import 'package:http/http.dart' as http;
import 'package:localbit/datasources/helper/preferences.dart';
import 'package:localbit/datasources/helper/sharepref_name.dart';

import 'network_service_response.dart';

class RestClient {
  String baseUrl;
  RestClient(String baseurl) {
    this.baseUrl = baseurl;
  }

  Map<String, String> headers = {
    "Accept": "application/json",
    "Content-Type": "application/x-www-form-urlencoded"
  };

  Future<MappedNetworkServiceResponse<T>> getAsync<T>(
      String resourcePath) async {
    print(baseUrl + resourcePath);
    var response = await http
        .get(baseUrl + resourcePath)
        .timeout(const Duration(seconds: 30));
    return processResponse<T>(response);
  }

  Future<MappedNetworkServiceResponse<T>> getAsyncToken<T>(
      String resourcePath) async {
    print(baseUrl + resourcePath);
    var token;
    await getToken().then((result) {
      token = result;
    });
    Map<String, String> mapToken = {"Authorization": "Bearer $token"};
    headers.addAll(mapToken);
    var response = await http
        .get(baseUrl + resourcePath, headers: headers)
        .timeout(const Duration(seconds: 30));
    return processResponse<T>(response);
  }

  Future<MappedNetworkServiceResponse<T>> postAsync<T>(
      String resourcePath, dynamic data) async {
    // var content = json.encoder.convert(data);
    var response =
        await http.post(baseUrl + resourcePath, body: data, headers: headers);
    return processResponse<T>(response);
  }

  Future<MappedNetworkServiceResponse<T>> postAsyncWithToken<T>(
      String resourcePath, dynamic data) async {
    // var content = json.encoder.convert(data);
    var token;
    await getToken().then((result) {
      token = result;
    });
    Map<String, String> mapToken = {"Authorization": "Bearer $token"};
    headers.addAll(mapToken);
    var response =
        await http.post(baseUrl + resourcePath, body: data, headers: headers);
    print(response.request.url);
    return processResponse<T>(response);
  }

  MappedNetworkServiceResponse<T> processResponse<T>(http.Response response) {
    if (!((response.statusCode < 200) ||
        (response.statusCode >= 300) ||
        (response.body == null))) {
      //have data
      var jsonResult = response.body;
      // print(jsonResult);
      var resultClass = jsonDecode(jsonResult);

      return new MappedNetworkServiceResponse<T>(
          mappedResult: resultClass,
          networkServiceResponse: new NetworkServiceResponse<T>(success: true));
    } else {
      print(response.body);
      return new MappedNetworkServiceResponse<T>(
          networkServiceResponse: new NetworkServiceResponse<T>(
              success: false, message: "Server Error"));
    }
  }

  getToken() async {
    Preference.load();
    String token = Preference.getString(SharePrefName.TOKEN);
    return token;
  }
}
