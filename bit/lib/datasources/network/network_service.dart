
import 'package:localbit/datasources/network/restclient.dart';

abstract class NetworkService {
  RestClient rest;
  NetworkService(this.rest);
}
