import 'package:localbit/%08models/base_response.dart';
import 'package:localbit/%08models/confirm_transfer.dart';
import 'package:localbit/%08models/curencies_response.dart';
import 'package:localbit/%08models/fiats_response.dart';
import 'package:localbit/%08models/forgot_request.dart';
import 'package:localbit/%08models/forgot_response.dart';
import 'package:localbit/%08models/login_request.dart';
import 'package:localbit/%08models/login_response.dart';
import 'package:localbit/%08models/login_response_error.dart';
import 'package:localbit/%08models/profile_response.dart';
import 'package:localbit/%08models/qrcode_generation_request.dart';
import 'package:localbit/%08models/qrcode_generation_response.dart';
import 'package:localbit/%08models/recipient_response.dart';
import 'package:localbit/%08models/register_request.dart';
import 'package:localbit/%08models/register_response.dart';
import 'package:localbit/%08models/transfer_request.dart';
import 'package:localbit/%08models/transfer_response.dart';
import 'package:localbit/%08models/wallet_balance_response.dart';
import 'package:localbit/datasources/network/restclient.dart';

import 'network_service.dart';
import 'network_service_response.dart';

class ApiNetworkProvider extends NetworkService {
  ApiNetworkProvider(RestClient rest) : super(rest);

  Future<NetworkServiceResponse<LoginResponse>> performLogin(
      LoginRequest loginRequest) async {
    Map<String, String> body = {
      'email': loginRequest.email,
      'password': loginRequest.password
    };

    var result =
        await rest.postAsync<LoginResponse>("/api/v1/auth/login", body);
    // print(result.mappedResult);
    if (result.mappedResult != null) {
      var data;
      try {
        data = LoginResponse.fromJson(result.mappedResult);
        return new NetworkServiceResponse(
          content: data,
          success: result.networkServiceResponse.success,
        );
      } catch (e) {
        print("parse");
        ErrorLogin data = ErrorLogin.fromJson(result.mappedResult);
        return new NetworkServiceResponse(
          content: LoginResponse(
              status: data.status, message: data.message.email[0]),
          success: result.networkServiceResponse.success,
        );
      }
    }
    return new NetworkServiceResponse(
        success: result.networkServiceResponse.success,
        message: result.networkServiceResponse.message);
  }

  Future<NetworkServiceResponse<RegisterResponse>> performRegister(
      RegisterRequest registerRequest) async {
    var result = await rest.postAsync<LoginResponse>(
        "/api/v1/auth/register", registerRequest.toJson());
    if (result.mappedResult != null) {
      var data = RegisterResponse.fromJson(result.mappedResult);
      return new NetworkServiceResponse(
        content: data,
        success: result.networkServiceResponse.success,
      );
    }
    return new NetworkServiceResponse(
        success: result.networkServiceResponse.success,
        message: result.networkServiceResponse.message);
  }

  Future<NetworkServiceResponse<ForgotPassResponse>> performForgotPass(
      ForgotPassRequest forgotPassRequest) async {
    var result = await rest.postAsync<ForgotPassResponse>(
        "/api/v1/auth/password/forgot", forgotPassRequest.toJson());

    if (result.mappedResult != null) {
      var data = ForgotPassResponse.fromJson(result.mappedResult);
      return new NetworkServiceResponse(
        content: data,
        success: result.networkServiceResponse.success,
      );
    }
    return new NetworkServiceResponse(
        success: result.networkServiceResponse.success,
        message: result.networkServiceResponse.message);
  }

  Future<NetworkServiceResponse<FiatsResponse>> getFiats() async {
    var result = await rest.getAsyncToken<FiatsResponse>(
      "/api/v1/home/get_fiats",
    );
    if (result.mappedResult != null) {
      var data = FiatsResponse.fromJson(result.mappedResult);
      return new NetworkServiceResponse(
        content: data,
        success: result.networkServiceResponse.success,
      );
    }

    return new NetworkServiceResponse(
        success: result.networkServiceResponse.success,
        message: result.networkServiceResponse.message);
  }

  Future<NetworkServiceResponse<CurrenciesResponse>> getCurrencies(
      String nameSymbol) async {
    var result = await rest.getAsyncToken<CurrenciesResponse>(
      "/api/v1/home/get_currencies/$nameSymbol",
    );
    try {
      if (result.mappedResult != null) {
        var data = CurrenciesResponse.fromJson(result.mappedResult);
        return new NetworkServiceResponse(
          content: data,
          success: result.networkServiceResponse.success,
        );
      }
    } catch (e, stacktrace) {
      print(stacktrace);
    }

    return new NetworkServiceResponse(
        success: result.networkServiceResponse.success,
        message: result.networkServiceResponse.message);
  }

  Future<NetworkServiceResponse<ProfileResponse>> getProfile() async {
    var result = await rest.getAsyncToken<ProfileResponse>(
      "/api/v1/account/profile",
    );
    try {
      if (result.mappedResult != null) {
        var data = ProfileResponse.fromJson(result.mappedResult);
        return new NetworkServiceResponse(
          content: data,
          success: result.networkServiceResponse.success,
        );
      }
    } catch (e, stacktrace) {
      print("Eror parser");
      print(stacktrace);
    }

    return new NetworkServiceResponse(
        success: result.networkServiceResponse.success,
        message: result.networkServiceResponse.message);
  }

  Future<NetworkServiceResponse<WalletBalanceResponse>>
      getWalletBalance() async {
    var result = await rest.getAsyncToken<WalletBalanceResponse>(
      "/api/v1/account/get_user_balance",
    );
    try {
      if (result.mappedResult != null) {
        var data = WalletBalanceResponse.fromJson(result.mappedResult);
        return new NetworkServiceResponse(
          content: data,
          success: result.networkServiceResponse.success,
        );
      }
    } catch (e, stacktrace) {
      print("Eror parser");
      print(stacktrace);
    }

    return new NetworkServiceResponse(
        success: result.networkServiceResponse.success,
        message: result.networkServiceResponse.message);
  }

  Future<NetworkServiceResponse<TransferResponse>> createTransfer(
      TransferRequest transferRequest) async {
    var result = await rest.postAsyncWithToken<TransferResponse>(
        "/api/v1/transfer/create?", transferRequest.toJson());
    try {
      if (result.mappedResult != null) {
        var data = TransferResponse.fromJson(result.mappedResult);
        return new NetworkServiceResponse(
          content: data,
          success: result.networkServiceResponse.success,
        );
      }
    } catch (e, stacktrace) {
      print("Eror parser");
      print(stacktrace);
    }

    return new NetworkServiceResponse(
        success: result.networkServiceResponse.success,
        message: result.networkServiceResponse.message);
  }

  Future<NetworkServiceResponse<ConfirmTransferResponse>> confirmTransfer(
      String transferId, String otp) async {
    Map<String, String> body = {"transfer_id": transferId, "otp": otp};
    var result = await rest.postAsyncWithToken<ConfirmTransferResponse>(
        "/api/v1/transfer/create/confirm?", body);
    try {
      if (result.mappedResult != null) {
        var data = ConfirmTransferResponse.fromJson(result.mappedResult);
        return new NetworkServiceResponse(
          content: data,
          success: result.networkServiceResponse.success,
        );
      }
    } catch (e, stacktrace) {
      print("Eror parser");
      print(stacktrace);
    }

    return new NetworkServiceResponse(
        success: result.networkServiceResponse.success,
        message: result.networkServiceResponse.message);
  }

  Future<NetworkServiceResponse<ListRecipientResponse>>
      getListRecipient() async {
    var result = await rest.getAsyncToken<ListRecipientResponse>(
      "/api/v1/transfer/recipients",
    );
    try {
      if (result.mappedResult != null) {
        var data = ListRecipientResponse.fromJson(result.mappedResult);
        return new NetworkServiceResponse(
          content: data,
          success: result.networkServiceResponse.success,
        );
      }
    } catch (e, stacktrace) {
      print("Eror parser");
      print(stacktrace);
    }

    return new NetworkServiceResponse(
        success: result.networkServiceResponse.success,
        message: result.networkServiceResponse.message);
  }

  Future<NetworkServiceResponse<BaseResponse>> saveRecipient(
      int recipientId) async {
    Map<String, String> body = {"userid": recipientId.toString()};
    var result = await rest.postAsyncWithToken<BaseResponse>(
        "/api/v1/transfer/save_recipient?", body);
    try {
      if (result.mappedResult != null) {
        var data = BaseResponse.fromJson(result.mappedResult);
        return new NetworkServiceResponse(
          content: data,
          success: result.networkServiceResponse.success,
        );
      }
    } catch (e, stacktrace) {
      print("Eror parser");
      print(stacktrace);
    }

    return new NetworkServiceResponse(
        success: result.networkServiceResponse.success,
        message: result.networkServiceResponse.message);
  }

  Future<NetworkServiceResponse<QRCodeGenResponse>> genCode(
      QRCodeGenRequest request) async {
    var result = await rest.postAsyncWithToken<QRCodeGenResponse>(
        "/api/v1/qrcode/generate", request.toJson());
    try {
      if (result.mappedResult != null) {
        var data = QRCodeGenResponse.fromJson(result.mappedResult);
        return new NetworkServiceResponse(
          content: data,
          success: result.networkServiceResponse.success,
        );
      }
    } catch (e, stacktrace) {
      print("Eror parser");
      print(stacktrace);
    }

    return new NetworkServiceResponse(
        success: result.networkServiceResponse.success,
        message: result.networkServiceResponse.message);
  }
}
