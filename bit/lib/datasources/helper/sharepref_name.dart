class SharePrefName {
  static const String TOKEN = "token_pref";

  static const String CURRENT_FIATS = "current_fiats_pref";

  static const String LIST_FIATS = "fiat_list_pref";
  static const String LIST_SYMBOL_CURRENCIES = "symbol_currencies_list_pref";
  
  static const String MY_WALLET = "my_wallet_pref";
}
