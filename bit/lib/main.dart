import 'package:flutter/material.dart';

import 'app.dart';
import 'di/di.dart';

void main() {
   Injector.configure(Flavor.DEV);
  runApp(MyApp());
}