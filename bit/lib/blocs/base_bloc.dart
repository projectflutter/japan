import 'package:localbit/datasources/user_repository.dart';
import 'package:localbit/utils/fetch_process.dart';
import 'package:rxdart/rxdart.dart';

abstract class BaseBloc {
  UserRepository userRepository = UserRepository();
  final apiController = BehaviorSubject<FetchProcess>();
  Observable<FetchProcess> get apiResult => apiController.stream;
  BaseBloc() {
    
  }
  void dispose();
}
