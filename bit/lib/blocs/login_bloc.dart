import 'package:localbit/%08models/login_request.dart';
import 'package:localbit/%08models/login_response.dart';
import 'package:localbit/blocs/base_bloc.dart';
import 'package:localbit/datasources/network/network_service_response.dart';
import 'package:localbit/utils/constant.dart';
import 'package:localbit/utils/fetch_process.dart';
import 'package:localbit/utils/uidata.dart';

import 'package:rxdart/rxdart.dart';

class LoginBloc extends BaseBloc {
//stream login
  final loginController = PublishSubject<LoginResponse>();
  Sink<LoginResponse> get loginSink => loginController.sink;
  Observable<LoginResponse> get loginStream => loginController.stream;
  Observable<FetchProcess> get apiResult => apiController.stream;

  void callLogin(LoginRequest loginRequest) async {
    FetchProcess process = new FetchProcess(loading: true);
    apiController.add(process);
    process.type = ApiType.performLogin;
    try {
      var result = await userRepository.performLogin(loginRequest);
      if (result.content.status == Constant.SUCESS_CODE) {
        await userRepository.saveToken(result.content.data.token);
      }

      process.loading = false;
      process.response = result;
      loginSink.add(result.content);
    } catch (e) {
      print(e);
      process.loading = false;
      process.response = NetworkServiceResponse(
          message: UIData.errorConnection, success: false, content: null);
    }
    apiController.add(process);
  }

  @override
  void dispose() {
    // TODO: implement dispose
    loginController.close();
    apiController.close();
  }
}
