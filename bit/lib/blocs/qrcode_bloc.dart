import 'package:localbit/%08models/qrcode_generation_request.dart';
import 'package:localbit/%08models/qrcode_generation_response.dart';
import 'package:localbit/blocs/base_bloc.dart';
import 'package:localbit/utils/fetch_process.dart';
import 'package:localbit/utils/uidata.dart';
import 'package:rxdart/rxdart.dart';

class QRCodeBloc extends BaseBloc {
  //stream qrcode

  final _genCodeController = PublishSubject<QRCodeGenResponse>();
  Sink<QRCodeGenResponse> get genCodeSink => _genCodeController.sink;
  Observable<QRCodeGenResponse> get genCodeStream => _genCodeController.stream;

  generation(QRCodeGenRequest request) async {
    FetchProcess process = new FetchProcess(loading: true);
    try {
      apiController.add(process);
      var result = await userRepository.genCode(request);
      genCodeSink.add(result.content);
      process.loading = false;
      process.response = result;
    } catch (e) {
      process.loading = false;
      _genCodeController.addError(UIData.errorConnection);
    }
    apiController.add(process);
  }

  @override
  void dispose() {
    _genCodeController?.close();
  }
}
