import 'dart:convert';

import 'package:localbit/%08models/curencies_response.dart';
import 'package:localbit/%08models/fiats_response.dart';
import 'package:localbit/%08models/wallet_balance_response.dart';
import 'package:localbit/blocs/base_bloc.dart';
import 'package:localbit/utils/fetch_process.dart';
import 'package:localbit/utils/uidata.dart';
import 'package:rxdart/rxdart.dart';

class HomePageBloc extends BaseBloc {
  //stream fiats

  final getFiatsController = PublishSubject<FiatsResponse>();
  Sink<FiatsResponse> get getFiatsSink => getFiatsController.sink;
  Observable<FiatsResponse> get getFiatsStream => getFiatsController.stream;
  //stream currencies

  final getCurrenciesController = PublishSubject<CurrenciesResponse>();
  Sink<CurrenciesResponse> get getCurrenciesSink =>
      getCurrenciesController.sink;
  Observable<CurrenciesResponse> get getCurrenciesStream =>
      getCurrenciesController.stream;

  final getWalletBalanceController = PublishSubject<WalletBalanceResponse>();
  Sink<WalletBalanceResponse> get getWalletBalanceSink =>
      getWalletBalanceController.sink;
  Observable<WalletBalanceResponse> get getWalletBalanceStream =>
      getWalletBalanceController.stream;

  void getFiats() async {
    FetchProcess process = new FetchProcess(loading: true);
    try {
      apiController.add(process);
      var result = await userRepository.getFiats();
      await userRepository.saveListFiats(result.content.data.fiats);
      if (userRepository.getCurrentFiat() == null)
        saveCurrentFiat(result.content.data.fiats[0]);
      getFiatsSink.add(result.content);
      process.loading = false;
      process.response = result;
    } catch (e) {
      process.loading = false;
      getFiatsController.addError(UIData.errorConnection);
    }
    apiController.add(process);
  }

  getCurrencies(String nameSymbol) async {
    FetchProcess process = new FetchProcess(loading: true);
    try {
      apiController.add(process);
      var result = await userRepository.getCurrencies(nameSymbol);
      await userRepository.saveSymbolCurrencies(result.content.data.currencies);
      getCurrenciesSink.add(result.content);
      process.loading = false;
      process.response = result;
    } catch (e) {
      process.loading = false;
      getCurrenciesController.addError(UIData.errorConnection);
    }
    apiController.add(process);
  }

  getWalletBalance() async {
    print("call api");
    // FetchProcess process = new FetchProcess(loading: true);
    try {
      // apiController.add(process);
      var result = await userRepository.getWalletBalance();
      getWalletBalanceSink.add(result.content);
      userRepository.saveListBalance(result.content.data.balance);
      // process.loading = false;
      // process.response = result;
    } catch (e, stacktrace) {
      print(stacktrace);
      // process.loading = false;
      getWalletBalanceController.addError(UIData.errorConnection);
    }
    // apiController.add(process);
  }

  @override
  void dispose() {
    getFiatsController?.close();
    apiController?.close();
    getCurrenciesController?.close();
    getWalletBalanceController?.close();
  }

  void saveCurrentFiat(Fiats listFiat) async {
    try {
      var stringFiat = json.encode(listFiat);
      await userRepository.saveCurrentFiat(stringFiat);
    } catch (e) {
      print(e);
    }
  }

  Fiats getCurrentFiats() {
    try {
      Fiats result = userRepository.getCurrentFiat();
      return result;
    } catch (e) {
      print(e);
      return null;
    }
  }

  List<String> getListSymbolFiats() {
    try {
      List<Fiats> fiatsLists = userRepository.getListFiats();
      List<Currencies> currenciesList = userRepository.getSymbolCurrencies();
      List<String> listSymbol = [];
      for (var item in fiatsLists) {
        listSymbol.add(item.name);
      }
      for (var item in currenciesList) {
        listSymbol.add(item.symbol);
      }
      return listSymbol;
    } catch (e,stacktrace) {
      print(stacktrace);
      return [];
    }
  }
}
