import 'package:localbit/%08models/forgot_request.dart';
import 'package:localbit/%08models/forgot_response.dart';
import 'package:localbit/blocs/base_bloc.dart';
import 'package:localbit/datasources/network/network_service_response.dart';
import 'package:localbit/utils/fetch_process.dart';
import 'package:localbit/utils/uidata.dart';
import 'package:rxdart/rxdart.dart';

class ForgotPasswordBloc extends BaseBloc{
 //stream login
  final forgotPassController = PublishSubject<ForgotPassResponse>();
  Sink<ForgotPassResponse> get forgotPassSink => forgotPassController.sink;
  Observable<ForgotPassResponse> get forgotPassStream => forgotPassController.stream;
  Observable<FetchProcess> get apiResult => apiController.stream;

  void callLogin(ForgotPassRequest forgotPassRequest) async {
    FetchProcess process = new FetchProcess(loading: true);
    apiController.add(process);
    process.type = ApiType.performLogin;
    try {
      var result = await userRepository.performForgotPass(forgotPassRequest);
      process.loading = false;
      process.response = result;
      forgotPassSink.add(result.content);
    } catch (e) {
      print(e);
      process.loading = false;
      process.response = NetworkServiceResponse(
          message: UIData.errorConnection, success: false, content: null);
    }
    apiController.add(process);
  }

  @override
  void dispose() {
    // TODO: implement dispose
    forgotPassController.close();
    apiController.close();
  }

}