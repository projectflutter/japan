import 'package:localbit/%08models/fiats_response.dart';
import 'package:localbit/blocs/base_bloc.dart';

class ExchangeBloc extends BaseBloc {
  // stream exchange
  // final registerController = PublishSubject<bool>();
  // Sink<bool> get authenSink => registerController.sink;
  // Observable<bool> get authenStream => registerController.stream;

  List<Fiats> getListFiats() {
    try {
      var result = userRepository.getListFiats();
      return result;
    } catch (e) {
      print(e);
      return [];
    }
  }

  @override
  void dispose() {
    apiController.close();
  }
}
