import 'package:localbit/blocs/base_bloc.dart';
import 'package:rxdart/rxdart.dart';

class AuthenticationBloc extends BaseBloc {
  //stream login
  final registerController = PublishSubject<bool>();
  Sink<bool> get authenSink => registerController.sink;
  Observable<bool> get authenStream => registerController.stream;

  void checkSignIned() async {
    try {
      var result = await userRepository.isSignedIn();
      authenSink.add(result);
    } catch (e) {
      print(e);
    }
  }

  @override
  void dispose() {
    // TODO: implement dispose
    registerController.close();
    apiController.close();
  }
}
