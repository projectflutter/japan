import 'package:localbit/%08models/profile_response.dart';
import 'package:localbit/blocs/base_bloc.dart';
import 'package:localbit/datasources/network/network_service_response.dart';
import 'package:localbit/utils/fetch_process.dart';
import 'package:localbit/utils/uidata.dart';
import 'package:rxdart/rxdart.dart';

class HomeScreenBloc extends BaseBloc {
  //stream clear token
  final homeScreenController = PublishSubject<bool>();
  final profileController = PublishSubject<ProfileResponse>();
  Sink<void> get homescreenSink => homeScreenController.sink;
  Observable<void> get homeScreenStream => homeScreenController.stream;
//stream profile
  Sink<ProfileResponse> get profileSink => profileController.sink;
  Observable<ProfileResponse> get profileStream => profileController.stream;

  void clearToken() async {
    try {
      var result = await userRepository.clearToken();
      homescreenSink.add(result);
    } catch (e) {
      print(e);
    }
  }

  void getProfile() async {
    print("Profile");
    FetchProcess process = new FetchProcess(loading: true);
    apiController.add(process);
    try {
      var result = await userRepository.getProfile();
      
      process.loading = false;
      process.response = result;
      profileSink.add(result.content);
    } catch (e) {
      print("errorProfile");
      print(e);
      process.loading = false;
      process.response = NetworkServiceResponse(
          message: UIData.errorConnection, success: false, content: null);
    }
    apiController.add(process);
  }

  @override
  void dispose() {
    // TODO: implement dispose
    homeScreenController?.close();
    apiController?.close();
    profileController?.close();
  }
}
