import 'package:localbit/%08models/register_request.dart';
import 'package:localbit/%08models/register_response.dart';
import 'package:localbit/blocs/base_bloc.dart';
import 'package:localbit/datasources/network/network_service_response.dart';
import 'package:localbit/utils/fetch_process.dart';
import 'package:localbit/utils/uidata.dart';
import 'package:rxdart/rxdart.dart';

class RegisterBloc extends BaseBloc {
//stream login
  final registerController = PublishSubject<RegisterResponse>();
  Sink<RegisterResponse> get registerSink => registerController.sink;
  Observable<RegisterResponse> get registerStream => registerController.stream;

  void callLogin(RegisterRequest registerRequest) async {
    FetchProcess process = new FetchProcess(loading: true);
    apiController.add(process);
    process.type = ApiType.performLogin;
    try {
      var result = await userRepository.performRegister(registerRequest);
      await userRepository.saveToken(result.content.data.token);
      process.loading = false;
      process.response = result;
      registerSink.add(result.content);
    } catch (e) {
      print(e);
      process.loading = false;
      process.response = NetworkServiceResponse(
          message: UIData.errorConnection, success: false, content: null);
    }
    apiController.add(process);
  }

  @override
  void dispose() {
    // TODO: implement dispose
    registerController.close();
    apiController.close();
  }
}
