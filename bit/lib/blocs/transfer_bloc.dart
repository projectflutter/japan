import 'dart:async';

import 'package:localbit/%08models/base_response.dart';
import 'package:localbit/%08models/confirm_transfer.dart';
import 'package:localbit/%08models/recipient_response.dart';
import 'package:localbit/%08models/transfer_request.dart';
import 'package:localbit/%08models/transfer_response.dart';
import 'package:localbit/%08models/wallet_balance_response.dart';
import 'package:localbit/blocs/base_bloc.dart';
import 'package:localbit/datasources/network/network_service_response.dart';
import 'package:localbit/ui/transfer/%08navigation_transfer_provider.dart';
import 'package:localbit/ui/transfer/widgets/enum_page.dart';
import 'package:localbit/utils/fetch_process.dart';
import 'package:localbit/utils/uidata.dart';
import 'package:rxdart/rxdart.dart';

class TransferBloc extends BaseBloc {
  final navigationController = StreamController();
  NavigationTransferProvider navigationProvider =
      new NavigationTransferProvider();

  Stream get getNavigation => navigationController.stream;

  void updateNavigation(TransferPageEnum navigation) {
    navigationProvider.updateNavigation(navigation);
    navigationController.sink.add(navigationProvider.currentNavigation);
  }

  //stream create
  final createTransferController = PublishSubject<TransferResponse>();
  Observable<TransferResponse> get createStream =>
      createTransferController.stream;

  //stream confirm
  final confirmTransferController = PublishSubject<ConfirmTransferResponse>();
  Observable<ConfirmTransferResponse> get confirmStream =>
      confirmTransferController.stream;

//stream getListRecipient
  final getListRecipientController = PublishSubject<ListRecipientResponse>();
  Observable<ListRecipientResponse> get getListRecipientStream =>
      getListRecipientController.stream;

      //stream saveRecipient
  final saveRecipientController = PublishSubject<BaseResponse>();
  Observable<BaseResponse> get saveRecipientStream =>
      saveRecipientController.stream;

  List<Balance> getListBalance() {
    try {
      var result = userRepository.getListBalance();
      print(result.length);
      return result;
    } catch (e, stacktrace) {
      return [];
    }
  }

  void createTransfer(TransferRequest transferRequest) async {
    FetchProcess process = new FetchProcess(loading: true);
    apiController.add(process);
    try {
      var result = await userRepository.createTransfer(transferRequest);

      process.loading = false;
      process.response = result;
      createTransferController.sink.add(result.content);
    } catch (e, stacktrace) {
      print(stacktrace);
      process.loading = false;
      process.response = NetworkServiceResponse(
          message: UIData.errorConnection, success: false, content: null);
    }
    apiController.add(process);
  }

  void confirmTransfer(String transferId, String otp) async {
    FetchProcess process = new FetchProcess(loading: true);
    apiController.add(process);
    try {
      var result = await userRepository.confirmTransfer(transferId, otp);

      process.loading = false;
      process.response = result;
      confirmTransferController.sink.add(result.content);
    } catch (e, stacktrace) {
      print(stacktrace);
      process.loading = false;
      process.response = NetworkServiceResponse(
          message: UIData.errorConnection, success: false, content: null);
    }
    apiController.add(process);
  }

  getListRecipient() async {
    FetchProcess process = new FetchProcess(loading: true);
    apiController.add(process);
    try {
      var result = await userRepository.getListRecipient();

      process.loading = false;
      process.response = result;
      getListRecipientController.sink.add(result.content);
      print(result);
    } catch (e, stacktrace) {
      print(stacktrace);
      process.loading = false;
      process.response = NetworkServiceResponse(
          message: UIData.errorConnection, success: false, content: null);
    }
    apiController.add(process);
  }

  saveRecipient(int recipientId) async {
    FetchProcess process = new FetchProcess(loading: true);
    apiController.add(process);
    try {
      var result = await userRepository.saveRecipient(recipientId);

      process.loading = false;
      process.response = result;
      saveRecipientController.sink.add(result.content);
    } catch (e, stacktrace) {
      print(stacktrace);
      process.loading = false;
      process.response = NetworkServiceResponse(
          message: UIData.errorConnection, success: false, content: null);
    }
    apiController.add(process);
  }

  @override
  void dispose() {
    createTransferController.close();
    confirmTransferController.close();
    apiController.close();
    navigationController.close();
    getListRecipientController?.close();
    saveRecipientController?.close();
  }

}
