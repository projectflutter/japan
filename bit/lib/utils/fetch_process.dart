import 'package:localbit/datasources/network/network_service_response.dart';

enum ApiType { performLogin }

class FetchProcess<T> {
  ApiType type;
  bool loading;
  NetworkServiceResponse<T> response;

  FetchProcess({this.loading, this.response, this.type});
}
