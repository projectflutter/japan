import 'package:intl/intl.dart';
import 'package:localbit/%08models/fiats_response.dart';

int getCurentFiat(List<Fiats> listFiats, fiat) {
  if (fiat != null) {
    for (var i = 0; i < listFiats.length; i++) {
      if (fiat.id == listFiats[i].id) {
        return i;
      }
    }
  }
  return 0;
}

String formatNumber(double temp) {
  final formatter = new NumberFormat("#,###.##");
  return formatter.format(temp);
}
