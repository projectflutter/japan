import 'dart:ui';

import 'package:flutter/material.dart';

class UIData {
  //routes
  static const String loginRoute = "/login";
  static const String registerRoute = "/register";
  static const String homeRoute = "/home";
  //images
  static const String imageDir = "assets/images";
  static const String imageBottomLoading = "$imageDir/bottom_loading.png";
  static const String logoLoading = "$imageDir/logo_trippay.png";

  //title appbar

  static const String enter_valid_usename = 'Enter Username than 6 digit';
  static const String enter_valid_password = 'Enter Passwrod than 6 digit';

  static const String enter_valid_email = 'Invalid Emmail';
  static const String enter_valid_verifypassword =
      'Enter Passwrod than 6 digit';
  static const String math = 'Password and VerifyPassword dont math!';

  static const String copyright = "\u00a9 Copyright Localbit.eu";

  static const String qrcodetitle="QR Code";

  

  //color

  static Color colorPrimary() => Color(0xff000000);
  static Color colorAccent() => Color(0xff7F055F);
  static Color colorPrimaryLight() => Color(0xff68456D);
  static Color colorCard() => Color(0xff1882AF);
  static Color colorYellow() => Color(0xffFDBA2C);
  static Color colorItem() => Color(0xff1882AF);
  static Color colorItemBottom1() => Color(0xff00B04C);
  static Color colorItemBottom2() => Color(0xff1262AF);
  static Color colorItemBottom3() => Color(0xffFDBA12);
  static Color colorItemBottom4() => Color(0xffA74687);
  static Color colorItemBottom5() => Color(0xffAF6B28);

  //padding
  static const double paddingNormal = 8.0;

   //gneric
  static const String error = "Error";
  static const String success = "Success";
  static const String ok = "OK";
  static const String forgot_password = "Forgot Password?";
  static const String something_went_wrong = "Something went wrong";
  static const String coming_soon = "Coming Soon";
  static const String errorConnection="No connection to Internet!";
}
