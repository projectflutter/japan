import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:localbit/ui/forgot/forgot_screen.dart';
import 'package:localbit/ui/home/home_screen.dart';
import 'package:localbit/ui/login/login_screen.dart';
import 'package:localbit/ui/qrcode/scan_qrcode_screen.dart';
import 'package:localbit/ui/register/register_screen.dart';
import 'package:localbit/ui/splash/splash_screen.dart';
import 'package:localbit/utils/uidata.dart';

import 'ui/loading/loading_screen.dart';

class MyApp extends StatefulWidget {
  MyApp({Key key}) : super(key: key);

  _MyAppState createState() => _MyAppState();
}

class _MyAppState extends State<MyApp> {
  @override
  void initState() {
    super.initState();
    SystemChrome.setPreferredOrientations([
      DeviceOrientation.portraitUp,
    ]);
  }

  @override
  Widget build(BuildContext context) {
    return MaterialApp(
        theme: ThemeData(
          primarySwatch: Colors.blue,
        ),
        home: Scaffold(body: SplashScreen()),
        routes: <String, WidgetBuilder>{
          "/loading": (BuildContext context) => LoadingScreen(),
          UIData.loginRoute: (BuildContext context) => LoginScreen(),
          UIData.registerRoute: (BuildContext context) => RegisterScreen(),
          "/forgotpassword": (BuildContext context) => ForgotPassScreen(),
          UIData.homeRoute: (BuildContext context) => HomeScreen(),
           "/scanresult": (BuildContext context) => ScanQRCodeScreen(barcode: ModalRoute.of(context).settings.arguments,),
        });
  }
}
