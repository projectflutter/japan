import 'package:flutter/material.dart';

typedef void OnPress();

class Button extends StatelessWidget {
  final String text;
  final OnPress onPress;
  final Color color;

  Button(
      {Key key,
      @required this.text,
      @required this.onPress,
      @required this.color})
      : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Container(
      padding: EdgeInsets.symmetric(vertical: 4.0, horizontal: 30.0),
      width: double.infinity,
      child: RaisedButton(
        padding: EdgeInsets.all(12.0),
        shape: new RoundedRectangleBorder(
            borderRadius: new BorderRadius.circular(10.0)),
        child: Text(
          text,
          style: TextStyle(color: Colors.white),
        ),
        color: color,
        onPressed: () {
          onPress();
        },
      ),
    );
  }
}
