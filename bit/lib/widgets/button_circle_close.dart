import 'package:flutter/material.dart';

class ButtonClose extends StatelessWidget {
  const ButtonClose({Key key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Align(
      alignment: Alignment.centerRight,
      child: GestureDetector(
        onTap: () {
          Navigator.pop(context);
        },
        child: Container(
          margin: const EdgeInsets.only(top:8.0,right: 8.0),
          decoration: BoxDecoration(
            border: Border.all(color: Colors.white, width: 1.0),
            shape: BoxShape.circle,
          ),
          child: Padding(
            padding: const EdgeInsets.all(1.0),
            child: Icon(
              Icons.close,
              color: Colors.white,
            ),
          ),
        ),
      ),
    );
  }
}