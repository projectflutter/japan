import 'package:flutter/material.dart';
import 'package:localbit/utils/uidata.dart';

class LoadingBackground extends StatelessWidget {
  const LoadingBackground({Key key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    final topHalf = new Flexible(
      flex: 3,
      child: new Container(),
    );
    final bottomHalf = new Flexible(
      flex: 2,
      child: Container(
          width: double.infinity,
          child: Image.asset(
            UIData.imageBottomLoading,
            fit: BoxFit.fill,
          )),
    );
    final bottomText = new Container(
      padding: EdgeInsets.all(UIData.paddingNormal),
      child: Text(
        UIData.copyright,
        style: TextStyle(color: Colors.white),
      ),
    );
    return Stack(
      children: <Widget>[
        Column(
          mainAxisSize: MainAxisSize.max,
          mainAxisAlignment: MainAxisAlignment.end,
          children: <Widget>[topHalf, bottomHalf],
        ),
        Align(alignment: Alignment.bottomCenter, child: bottomText)
      ],
    );
  }
}
