import 'package:flutter/material.dart';

class WalletProvider extends InheritedWidget {
  final Function reloadWallet;
  WalletProvider({
    this.reloadWallet,
    Key key,
    this.child,
  }) : super(key: key, child: child);

  final Widget child;

  static WalletProvider of(BuildContext context) {
    return (context.inheritFromWidgetOfExactType(WalletProvider)
        as WalletProvider);
  }

  @override
  bool updateShouldNotify(WalletProvider oldWidget) {
    return true;
  }

}
