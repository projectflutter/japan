import 'package:flutter/material.dart';

class GradientBackground extends StatelessWidget {
  const GradientBackground({Key key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Container(
      // Add box decoration
      decoration: new BoxDecoration(
        gradient: new LinearGradient(
            colors: [
              Color(0xff2CFEF1),
              Color(0xff2887E9),
            ],
            begin: const FractionalOffset(1.0, 1.0),
            end: const FractionalOffset(0.0, 0.0),
            stops: [0.0, 1.0],
            tileMode: TileMode.mirror),
      ),
    );
  }
}
