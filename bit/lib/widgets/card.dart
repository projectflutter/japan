import 'package:flutter/material.dart';
import 'package:localbit/utils/uidata.dart';

class CardCS extends StatelessWidget {
  final Widget form;
  CardCS({Key key, this.form}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Container(
        decoration: BoxDecoration(
          border: Border.all(color: UIData.colorCard(), width: 2.0),
          color: UIData.colorCard(),
          borderRadius: BorderRadius.all(
            Radius.circular(5.0),
          ),
          boxShadow: <BoxShadow>[
            new BoxShadow(
              color: Colors.black45,
              blurRadius: 3.0,
              offset: new Offset(0.0, 6.0),
            ),
          ],
        ),
        margin: EdgeInsets.all(15.0),
        child: form);
  }
}
