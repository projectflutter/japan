import 'package:flutter/material.dart';
import 'package:localbit/utils/uidata.dart';

class LogoApp extends StatelessWidget {
  const LogoApp({Key key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Container(
      child: Container(height: 48, child: Image.asset(UIData.logoLoading)),
    );
  }
}
