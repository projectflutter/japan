import 'package:flutter/material.dart';
import 'package:localbit/%08models/fiats_response.dart';

typedef OnGetValue(Fiats value);

class Combobox extends StatefulWidget {
  final List<Fiats> listDropdownField;
  final bool isBackgroundWhite;
  int currentIndex;
  OnGetValue onGetValue;
  Combobox(
      {Key key,
      this.listDropdownField,
      this.isBackgroundWhite = false,
      this.currentIndex = 0,
      this.onGetValue})
      : super(key: key);

  _ComboboxState createState() => _ComboboxState();
}

class _ComboboxState extends State<Combobox> {
  Color selectedColor;

  @override
  void initState() {
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    Widget _itemListDropDown(Fiats fiats, Color colorText) {
      return Container(
        margin: EdgeInsets.only(
          top: 8.0,
          bottom: 8.0,
        ),
        padding: const EdgeInsets.only(bottom: 5.0),
        child: new Row(
          crossAxisAlignment: CrossAxisAlignment.center,
          mainAxisAlignment: MainAxisAlignment.center,
          children: <Widget>[
            fiats != null && fiats.logo != null
                ? FadeInImage(
                    image: NetworkImage('${fiats.logo}'),
                    placeholder: AssetImage("assets/images/ic_placeholder.png"))
                : Image.asset("assets/images/ic_placeholder.png"),
            Padding(
              padding: const EdgeInsets.only(left: 8.0),
              child: Text(
                fiats != null && fiats.name != null ? fiats.name : "",
                style: TextStyle(color: colorText, fontSize: 18),
              ),
            )
          ],
        ),
      );
    }

    return Container(
      height: 50,
      margin: EdgeInsets.all(4.0),
      padding: EdgeInsets.only(left: 6.0),
      decoration: widget.isBackgroundWhite
          ? BoxDecoration(
              color: Colors.white,
              borderRadius: BorderRadius.circular(2.0),
              border: Border.all(color: Colors.white, width: 0.5),
            )
          : new BoxDecoration(
              borderRadius: BorderRadius.circular(2.0),
              border: Border.all(color: Colors.white, width: 0.5),
            ),
      child: DropdownButtonHideUnderline(
        child: ButtonTheme(
            child: DropdownButton(
          style: TextStyle(color: Colors.white),
          hint: _itemListDropDown(
              widget.listDropdownField.length > 0
                  ? widget.listDropdownField[widget.currentIndex]
                  : null,
              Colors.white),
          iconEnabledColor:
              widget.isBackgroundWhite ? Colors.black : Colors.white,
          items: widget.listDropdownField.map((Fiats fiats) {
            return DropdownMenuItem(
                value: fiats, child: _itemListDropDown(fiats, Colors.black));
          }).toList(),
          onChanged: (Fiats value) {
            for (var i = 0; i < widget.listDropdownField.length; i++) {
              if (value.id == widget.listDropdownField[i].id) {
                setState(() {
                  widget.currentIndex = i;
                  widget.onGetValue(value);
                });
                return;
              }
            }
          },
        )),
      ),
    );
  }
}
