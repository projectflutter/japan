import 'package:flutter/material.dart';

class CustomizeAppbar extends StatelessWidget {
  const CustomizeAppbar({Key key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Container(
      padding: EdgeInsets.only(top: 24.0),
      child: InkWell(
        onTap: () {
          Navigator.pop(context);
        },
        child: Align(
            alignment: Alignment.centerLeft,
            child: Padding(
              padding: const EdgeInsets.all(8.0),
              child: Icon(
                Icons.arrow_back_ios,
                color: Colors.blue,
              ),
            )),
      ),
    );
  }
}
