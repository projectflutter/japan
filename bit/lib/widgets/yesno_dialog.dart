import 'package:flutter/material.dart';

typedef OnCLickYes();
typedef OnCLickNo();

class YesNoDialog extends StatelessWidget {
  final OnCLickYes onCLickYes;
  final OnCLickNo onCLickNo;
  final String message;
  YesNoDialog({Key key, this.onCLickYes, this.onCLickNo, this.message})
      : super(key: key);

  @override
  Widget build(BuildContext context) {
    return AlertDialog(
      title: Text("Notification"),
      content: Text(message),
      actions: <Widget>[
        FlatButton(
          child: Text("No"),
          onPressed: ()  {onCLickNo();},
        ),
        FlatButton(
          child: Text("Yes"),
          onPressed: () { onCLickYes();},
        ),
      ],
    );
  }
}
