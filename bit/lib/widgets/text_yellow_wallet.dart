import 'package:flutter/material.dart';

class TextYellowWallet extends StatelessWidget {
  final String text;
  TextYellowWallet({Key key, this.text}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Text(
      text,
      style: TextStyle(
          color: Colors.yellow, fontSize: 20, fontWeight: FontWeight.bold),
    );
  }
}
