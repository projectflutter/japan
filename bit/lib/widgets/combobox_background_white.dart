import 'package:flutter/material.dart';

class ComboboxBgWhite extends StatelessWidget {
  const ComboboxBgWhite({Key key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Container(
      height: 40,
      padding: EdgeInsets.symmetric(horizontal: 5.0, vertical: 2.0),
      decoration: new BoxDecoration(
        color: Colors.white,
        borderRadius: BorderRadius.circular(3.0),
        border: Border.all(color: Colors.white, width: 0.5),
      ),
      child: Row(
        crossAxisAlignment: CrossAxisAlignment.center,
        mainAxisAlignment: MainAxisAlignment.center,
        children: <Widget>[
          Padding(
            padding:
                const EdgeInsets.symmetric(horizontal: 15.0, vertical: 4.0),
            child: Image.asset(
              'assets/images/ic_method.png',
              scale: 5,
            ),
          ),
          RotatedBox(
            child: Icon(
              Icons.play_arrow,
              color: Colors.black,
            ),
            quarterTurns: 1,
          )
        ],
      ),
    );
  }
}
