import 'dart:ui';

class UIData {
  //images
  static const String imageDir = "assets";
  static const String level1 = "$imageDir/ic_n1.png";
  static const String level2 = "$imageDir/ic_n2.png";
  static const String level3 = "$imageDir/ic_n3.png";
  static const String level4 = "$imageDir/ic_n4.png";
  static const String level5 = "$imageDir/ic_n5.png";

  //title appbar
  static const String titleAppBarBookmark = "Đã ghi nhớ";
  static const String title = "JLPT Training";

  static const String imageCircleBookmark = "$imageDir/ic_circle_bookmark.png";
  static const String imageResult = "$imageDir/img_result.png";

  static const String enter_valid_usename='Enter Username than 6 digit';
  static const String enter_valid_password='Enter Passwrod than 6 digit';

   static const String enter_valid_email='Invalid Emmail';
  static const String enter_valid_verifypassword='Enter Passwrod than 6 digit';
  static const String math='Password and VerifyPassword dont math!';

  //color
  static Color colorPrimary() => Color(0xff310E31);
  static Color colorAccent() => Color(0xff7F055F);
  static Color colorPrimaryLight() => Color(0xff68456D);
  static Color colorSelected() => Color(0xff9988B5);
  static Color colorCircleSelected() => Color(0xff291238);
  static Color colorBg()=>Color(0xffe6e8ee);
  static Color colorButtonResults()=>Color(0xff9285B0);
  static Color colorCircleFocus()=>Color(0xff291238);
  static Color colorItemFocus()=>Color(0xffDCD8E1);
  static Color colorCorrect()=>Color(0xffDCEDC8);
  static Color colorWrong()=>Color(0xffFFCDD2);
  static Color colorGreen()=>Color(0xff8BC34A);
  static Color colorRed()=>Color(0xffF44336);
}
