import 'dart:convert';

import 'package:learn_japanese/data/model/bookmar_item.dart';
import 'package:learn_japanese/data/model/bookmark_response.dart';
import 'package:learn_japanese/data/model/change_pass_response.dart';
import 'package:learn_japanese/data/model/detail_response.dart';
import 'package:learn_japanese/data/model/login_response.dart';
import 'package:learn_japanese/data/model/question_response.dart';
import 'package:learn_japanese/data/model/result_response.dart';
import 'package:learn_japanese/data/model/school_menu.dart';
import 'package:learn_japanese/service/restclient.dart';
import 'package:learn_japanese/ui/home/bookmarkpage/detailbookmark/detail_bookmark_model.dart';
import 'package:learn_japanese/ui/survey/answer_result.dart';
import 'package:learn_japanese/ui/survey/answer_result_response.dart';

import 'network_service.dart';
import 'network_service_response.dart';

class ApiNetworkProvider extends NetworkService {
  ApiNetworkProvider(RestClient rest) : super(rest);

  Future<NetworkServiceResponse<LoginResponse>> login(
      String email, String password) async {
    Map<String, String> body = {'email': email, 'password': password};
    var result = await rest.postAsync<LoginResponse>("/api/login?", body);
    if (result.mappedResult != null) {
      var data = LoginResponse.fromJson(result.mappedResult);
      return new NetworkServiceResponse(
        content: data,
        success: result.networkServiceResponse.success,
      );
    }
    return new NetworkServiceResponse(
        success: result.networkServiceResponse.success,
        message: result.networkServiceResponse.message);
  }

  Future<NetworkServiceResponse<LoginResponse>> register(String email,
      String username, String password, String verifyPassword) async {
    Map<String, String> body = {
      'email': email,
      'name': username,
      'password': password,
      'password_confirmation': verifyPassword
    };
    var result = await rest.postAsync<LoginResponse>("/api/register?", body);
    if (result.mappedResult != null) {
      var data = LoginResponse.fromJson(result.mappedResult);
      return new NetworkServiceResponse(
        content: data,
        success: result.networkServiceResponse.success,
      );
    }
    return new NetworkServiceResponse(
        success: result.networkServiceResponse.success,
        message: result.networkServiceResponse.message);
  }

  Future<NetworkServiceResponse<SchoolMenu>> getMenuItems() async {
    var result = await rest.getAsync<SchoolMenu>("/api/get-schools");
    if (result.mappedResult != null) {
      var data = SchoolMenu.fromJson(result.mappedResult);
      return new NetworkServiceResponse(
        content: data,
        success: result.networkServiceResponse.success,
      );
    }
    return new NetworkServiceResponse(
        success: result.networkServiceResponse.success,
        message: result.networkServiceResponse.message);
  }

  Future<NetworkServiceResponse<DetailResponse>> getdetail(int id) async {
    Map<String, String> body = {'school_id': "$id"};
    var result = await rest.getAsyncData<DetailResponse>(
        "/api/get-categories?school_id=$id", body);
    if (result.mappedResult != null) {
      var data = DetailResponse.fromJson(result.mappedResult);
      return new NetworkServiceResponse(
        content: data,
        success: result.networkServiceResponse.success,
      );
    }
    return new NetworkServiceResponse(
        success: result.networkServiceResponse.success,
        message: result.networkServiceResponse.message);
  }

  Future<NetworkServiceResponse<BookmarkItem>> getBookmarkItem(int id) async {
    var result =
        await rest.getAsync<DetailResponse>("/api/get-bookmarks?school_id=$id");
    if (result.mappedResult != null) {
      var data = BookmarkItem.fromJson(result.mappedResult);
      return new NetworkServiceResponse(
        content: data,
        success: result.networkServiceResponse.success,
      );
    }
    return new NetworkServiceResponse(
        success: result.networkServiceResponse.success,
        message: result.networkServiceResponse.message);
  }

  Future<NetworkServiceResponse<ChangePassResponse>> changePass(
      String oldPass, String password, String verifyPassword) async {
    Map<String, String> body = {
      'old_password': oldPass,
      'password': password,
      'password_confirmation': verifyPassword
    };
    var result = await rest.postAsyncToken<ChangePassResponse>(
        "/api/post-change-password?", body);
    if (result.mappedResult != null) {
      var data = ChangePassResponse.fromJson(result.mappedResult);
      return new NetworkServiceResponse(
        content: data,
        success: result.networkServiceResponse.success,
      );
    }
    return new NetworkServiceResponse(
        success: result.networkServiceResponse.success,
        message: result.networkServiceResponse.message);
  }

  Future<NetworkServiceResponse<QuestionResponse>> getListQuestion(
      int categoryId) async {
    var result = await rest.getAsync<QuestionResponse>(
        "/api/get-questions?category_id=$categoryId");
    if (result.mappedResult != null) {
      var data = QuestionResponse.fromJson(result.mappedResult);
      return new NetworkServiceResponse(
        content: data,
        success: result.networkServiceResponse.success,
      );
    }
    return new NetworkServiceResponse(
        success: result.networkServiceResponse.success,
        message: result.networkServiceResponse.message);
  }

  Future<NetworkServiceResponse<AnswerResultResponse>> postResult(
      List<AnswerResult> listResult) async {
    Map<String, String> body = {'answers': json.encode(listResult)};
    var result = await rest.postAsyncToken<AnswerResultResponse>(
        "/api/post-answers?", body);
    if (result.mappedResult != null) {
      var data = AnswerResultResponse.fromJson(result.mappedResult);
      return new NetworkServiceResponse(
        content: data,
        success: result.networkServiceResponse.success,
      );
    }
    return new NetworkServiceResponse(
        success: result.networkServiceResponse.success,
        message: result.networkServiceResponse.message);
  }

  Future<NetworkServiceResponse<ResultResponse>> getResult(int id) async {
    var result = await rest
        .getAsync<ResultResponse>("/api/get-category-answer?category_id=$id");
    if (result.mappedResult != null) {
      var data = ResultResponse.fromJson(result.mappedResult);
      return new NetworkServiceResponse(
        content: data,
        success: result.networkServiceResponse.success,
      );
    }

    return new NetworkServiceResponse(
        success: result.networkServiceResponse.success,
        message: result.networkServiceResponse.message);
  }

  Future<NetworkServiceResponse<BookmarkResponse>> postBookmark(int id) async {
    Map<String, String> body = {'question_id': '$id'};
    var result = await rest.postAsyncToken<BookmarkResponse>(
        "/api/post-bookmark?", body);
    if (result.mappedResult != null) {
      var data = BookmarkResponse.fromJson(result.mappedResult);
      return new NetworkServiceResponse(
        content: data,
        success: result.networkServiceResponse.success,
      );
    }
    return new NetworkServiceResponse(
        success: result.networkServiceResponse.success,
        message: result.networkServiceResponse.message);
  }

  Future<NetworkServiceResponse<DetailBookmarkResponse>> getDetailBookmark(
      int id) async {
    var result = await rest
        .getAsync<DetailBookmarkResponse>("/api/get-question?question_id=$id");
    if (result.mappedResult != null) {
      var data = DetailBookmarkResponse.fromJson(result.mappedResult);
      return new NetworkServiceResponse(
        content: data,
        success: result.networkServiceResponse.success,
      );
    }
    return new NetworkServiceResponse(
        success: result.networkServiceResponse.success,
        message: result.networkServiceResponse.message);
  }

  Future<NetworkServiceResponse<ChangePassResponse>> resetPass(
      String email) async {
          Map<String, String> body = {'email': email};
    var result = await rest
        .postAsync<ChangePassResponse>("/api/reset-password?",body);
    if (result.mappedResult != null) {
      var data = ChangePassResponse.fromJson(result.mappedResult);
      return new NetworkServiceResponse(
        content: data,
        success: result.networkServiceResponse.success,
      );
    }
    return new NetworkServiceResponse(
        success: result.networkServiceResponse.success,
        message: result.networkServiceResponse.message);
  }
}
