import 'dart:async';
import 'dart:convert';
import 'package:http/http.dart' as http;
import 'package:learn_japanese/data/preferences.dart';
import 'package:learn_japanese/data/sharepref_name.dart';
import 'network_service_response.dart';

class RestClient {
  String baseUrl;
  RestClient(String baseurl) {
    this.baseUrl = baseurl;
  }

  Map<String, String> header = {
    "Accept": "application/json",
    "Content-Type": "application/x-www-form-urlencoded"
  };

  Future<MappedNetworkServiceResponse<T>> getAsync<T>(
      String resourcePath) async {
    print(baseUrl + resourcePath);
    var token;
    await getToken().then((result) {
      token = result;
    });
    Map<String, String> mapToken = {"Authorization": "Bearer $token"};
    header.addAll(mapToken);
    var response = await http
        .get(baseUrl + resourcePath, headers: header)
        .timeout(const Duration(seconds: 30));
    return processResponse<T>(response);
  }

  Future<MappedNetworkServiceResponse<T>> getAsyncData<T>(
      String resourcePath, dynamic data) async {
    print(baseUrl + resourcePath);
    var token;
    await getToken().then((result) {
      token = result;
    });
    Map<String, String> mapToken = {"Authorization": "Bearer $token"};
    header.addAll(mapToken);
    var response = await http
        .get(baseUrl + resourcePath, headers: header)
        .timeout(const Duration(seconds: 30));
    return processResponse<T>(response);
  }

  Future<MappedNetworkServiceResponse<T>> postAsync<T>(
      String resourcePath, dynamic data) async {
    var response =
        await http.post(baseUrl + resourcePath, body: data, headers: header);
    print(response.request.url);
    return processResponse<T>(response);
  }

  Future<MappedNetworkServiceResponse<T>> postAsyncToken<T>(
      String resourcePath, dynamic data) async {
    // var content = json.encoder.convert(data);//json body
    // print(data);
    var token;
    await getToken().then((result) {
      token = result;
    });
    Map<String, String> mapToken = {"Authorization": "Bearer $token"};
    header.addAll(mapToken);
    var response =
        await http.post(baseUrl + resourcePath, body: data, headers: header);
    print(response.request.url);
    return processResponse<T>(response);
  }

  MappedNetworkServiceResponse<T> processResponse<T>(http.Response response) {
    try {
      if (!((response.statusCode < 200) ||
          (response.statusCode >= 300) ||
          (response.body == null))) {
        //have data
        var jsonResult = response.body;
        print(jsonResult);
        var resultClass = jsonDecode(jsonResult);

        return new MappedNetworkServiceResponse<T>(
            mappedResult: resultClass,
            networkServiceResponse:
                new NetworkServiceResponse<T>(success: true));
      } else {
        print(response.body);

        var errorResponse = response.body;
        return new MappedNetworkServiceResponse<T>(
            networkServiceResponse: new NetworkServiceResponse<T>(
                success: false,
                message:
                    "(${response.statusCode}) ${errorResponse.toString()}"));
      }
    } catch (e) {
      print("networkfail");
    }
  }

  getToken() async {
    Preference.load();
    String token = Preference.getString(SharePrefName.TOKEN);
    return token;
  }
}
