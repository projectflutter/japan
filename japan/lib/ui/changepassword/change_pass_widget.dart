import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:learn_japanese/data/user_repository.dart';
import 'package:learn_japanese/ui/changepassword/change_pass%1C_provider.dart';
import 'package:learn_japanese/ui/changepassword/change_pass_bloc.dart';
import 'package:learn_japanese/ui/changepassword/change_pass_state.dart';
import 'package:learn_japanese/utils/uidata.dart';

import 'change_pass_event.dart';
import 'change_pass_screen.dart';

class ChangePassWidget extends StatefulWidget {
  final UserRepository _userRepository;

  ChangePassWidget({Key key, @required UserRepository userRepository})
      : assert(userRepository != null),
        _userRepository = userRepository,
        super(key: key);
  _ChangePassWidgetState createState() => _ChangePassWidgetState();
}

class _ChangePassWidgetState extends State<ChangePassWidget> {
  ChangePassBloc _changePassBloc;

  UserRepository get _userRepository => widget._userRepository;

  final _oldPasswordController = TextEditingController();
  final _passwordController = TextEditingController();
  final _verifyPasswordController = TextEditingController();

  @override
  void initState() {
    super.initState();
    _changePassBloc = BlocProvider.of<ChangePassBloc>(context);
  }

  @override
  Widget build(BuildContext context) {
    Widget toolbar() {
      return InkWell(
        splashColor: Colors.transparent,
        highlightColor: Colors.transparent,
        onTap: () {
          Navigator.of(context).pop();
        },
        child: Container(
          child: Align(
            alignment: Alignment.centerLeft,
            child: Icon(Icons.arrow_back_ios),
          ),
        ),
      );
    }

    final logo = Center(
      child: Text(
        "Đổi mật khẩu",
        style: TextStyle(fontSize: 22.0, color: UIData.colorPrimary()),
      ),
    );

    final oldPassword = TextFormField(
      controller: _oldPasswordController,
      autofocus: false,
      obscureText: true,
      decoration: InputDecoration(
          filled: true,
          fillColor: Colors.white,
          prefixIcon: Icon(
            Icons.lock,
            color: UIData.colorPrimary(),
          ),
          hintText: 'Mật khẩu cũ',
          hintStyle: TextStyle(color: UIData.colorPrimary()),
          contentPadding: EdgeInsets.fromLTRB(20.0, 10.0, 20.0, 10.0),
          border: OutlineInputBorder(borderRadius: BorderRadius.circular(32.0)),
          focusedBorder: OutlineInputBorder(
              borderRadius: BorderRadius.circular(32.0),
              borderSide: BorderSide(color: UIData.colorPrimary()))),
    );

    final password = TextFormField(
      controller: _passwordController,
      autofocus: false,
      obscureText: true,
      decoration: InputDecoration(
          filled: true,
          fillColor: Colors.white,
          prefixIcon: Icon(
            Icons.lock,
            color: UIData.colorPrimary(),
          ),
          hintText: 'Mật khẩu mới',
          hintStyle: TextStyle(color: UIData.colorPrimary()),
          contentPadding: EdgeInsets.fromLTRB(20.0, 10.0, 20.0, 10.0),
          border: OutlineInputBorder(borderRadius: BorderRadius.circular(32.0)),
          focusedBorder: OutlineInputBorder(
              borderRadius: BorderRadius.circular(32.0),
              borderSide: BorderSide(color: UIData.colorPrimary()))),
    );

    final confirmpassword = TextFormField(
      controller: _verifyPasswordController,
      autofocus: false,
      obscureText: true,
      decoration: InputDecoration(
          filled: true,
          fillColor: Colors.white,
          prefixIcon: Icon(
            Icons.lock,
            color: UIData.colorPrimary(),
          ),
          hintText: 'Xác nhận mật khẩu',
          hintStyle: TextStyle(color: UIData.colorPrimary()),
          contentPadding: EdgeInsets.fromLTRB(20.0, 10.0, 20.0, 10.0),
          border: OutlineInputBorder(borderRadius: BorderRadius.circular(32.0)),
          focusedBorder: OutlineInputBorder(
              borderRadius: BorderRadius.circular(32.0),
              borderSide: BorderSide(color: UIData.colorPrimary()))),
    );

    Widget registerButton(BuildContext context) {
      return Container(
        padding: EdgeInsets.symmetric(vertical: 16.0),
        width: 180,
        child: RaisedButton(
          shape: RoundedRectangleBorder(
            borderRadius: BorderRadius.circular(24),
          ),
          onPressed: () {
            valdateForm(context);
          },
          padding: EdgeInsets.all(12),
          color: UIData.colorAccent(),
          child: Text('Đổi mật khẩu', style: TextStyle(color: Colors.white)),
        ),
      );
    }

    return BlocListener(
      bloc: _changePassBloc,
      child: Container(
        child: Padding(
          padding: EdgeInsets.only(top: 24, left: 10),
          child: Column(
            // shrinkWrap: false,
            // padding: EdgeInsets.only(left: 24.0, right: 24.0),
            children: <Widget>[
              toolbar(),
              Expanded(
                child: Padding(
                  padding: EdgeInsets.only(top: 0, left: 22, right: 22),
                  child: ListView(
                    children: <Widget>[
                      logo,
                      SizedBox(height: 96.0),
                      oldPassword,
                      SizedBox(height: 8.0),
                      password,
                      SizedBox(height: 8.0),
                      confirmpassword,
                      registerButton(context),
                      SizedBox(height: 72.0)
                    ],
                  ),
                ),
              )
            ],
          ),
        ),
      ),
      listener: (BuildContext context, ChangePassState state) {
        if (state is ShowLoading) {
          showDialog(
              context: context,
              barrierDismissible: false,
              builder: (context) => WillPopScope(
                  onWillPop: () {},
                  child: Center(
                    child: CircularProgressIndicator(),
                  )));
        }
        if (state is InChangePassState) {
          if (Navigator.canPop(context)) {
            Navigator.pop(context);
          }
          showDialog(
              barrierDismissible: false,
              context: context,
              builder: (context) => WillPopScope(
                    onWillPop: () {},
                    child: AlertDialog(
                      title: Text("Thông báo"),
                      content: Text("Đổi mật khẩu thành công!"),
                      actions: <Widget>[
                        FlatButton(
                          child: Text("ok"),
                          onPressed: () {
                            //pop dialog and pop page
                            Navigator.of(context).pop();
                            Navigator.of(context).pop("/changepass");
                          },
                        )
                      ],
                    ),
                  ));
        }
        if (state is ErrorChangePassState) {
          if (Navigator.canPop(context)) {
            Navigator.pop(context);
          }

          showDialog(
              barrierDismissible: false,
              context: context,
              builder: (context) => WillPopScope(
                    onWillPop: () {
                      return Future.value(false);
                    },
                    child: AlertDialog(
                      title: Text("Thông báo"),
                      content: Text(state.errorMessage),
                      actions: <Widget>[
                        FlatButton(
                          child: Text("ok"),
                          onPressed: () {
                            if (Navigator.canPop(context)) {
                              Navigator.pop(context);
                            }
                          },
                        )
                      ],
                    ),
                  ));
        }
      },
    );
  }

  void valdateForm(BuildContext context) {
    String oldPassword = _oldPasswordController.text;
    String password = _passwordController.text;
    String verifyPassword = _verifyPasswordController.text;
    if (!(oldPassword?.length >= 1)) {
      showErrorValidate(context, ChangePassValidationType.old);
    } else if (!(password?.length >= 6)) {
      showErrorValidate(context, ChangePassValidationType.password);
    } else if (!(verifyPassword?.length >= 6)) {
      showErrorValidate(context, ChangePassValidationType.validatepassword);
    } else if (!(password == verifyPassword)) {
      showErrorValidate(context, ChangePassValidationType.math);
    } else {
      _changePassBloc.dispatch(SubmitChangePassEvent(
          oldPass: oldPassword,
          password: password,
          verifyPassword: verifyPassword));
    }
  }

  @override
  void dispose() {
    // TODO: implement dispose
    super.dispose();
    _changePassBloc.dispose();
    _oldPasswordController.dispose();
    _passwordController.dispose();
    _verifyPasswordController.dispose();
  }

  void showErrorValidate(BuildContext context, ChangePassValidationType type) {
    print(type);
    ChangePassProvider.of(context).validationErrorCallback(type);
  }
}
