import 'package:equatable/equatable.dart';
import 'package:meta/meta.dart';

@immutable
abstract class ChangePassEvent extends Equatable {
  ChangePassEvent([List props = const []]) : super(props);
}

class SubmitChangePassEvent extends ChangePassEvent {
  String oldPass;
  String password;
  String verifyPassword;
  SubmitChangePassEvent(
      {@required this.oldPass,
      @required this.password,
      @required this.verifyPassword})
      : super([oldPass, password, verifyPassword]);

  @override
  String toString() => 'SubmitChangePassEvent';
}
