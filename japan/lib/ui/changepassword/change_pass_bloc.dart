import 'dart:async';
import 'package:bloc/bloc.dart';
import 'package:learn_japanese/data/user_repository.dart';

import 'change_pass_event.dart';
import 'change_pass_state.dart';

class ChangePassBloc extends Bloc<ChangePassEvent, ChangePassState> {
  UserRepository _userRepository = UserRepository();
  ChangePassState get initialState => new UnChangePassState();

  @override
  Stream<ChangePassState> mapEventToState(
    ChangePassEvent event,
  ) async* {
    if (event is SubmitChangePassEvent) {
      try {
        yield ShowLoading();
        var result = await _userRepository.changePass(
            event.oldPass, event.password, event.verifyPassword);
        if (result.content.status == 200) {
          yield InChangePassState();
        } else {
          yield ErrorChangePassState(result.content.message);
        }
      } catch (e) {
        print(e);
        yield ErrorChangePassState("Error change password!");
      }
    }
  }
}
