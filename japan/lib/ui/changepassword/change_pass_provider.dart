import 'package:flutter/material.dart';

class ChangePassProvider extends InheritedWidget {
  final Function validationErrorCallback;
  final Widget child;

  ChangePassProvider({this.validationErrorCallback, this.child})
      : super(child: child);

  static ChangePassProvider of(BuildContext context) =>
      context.inheritFromWidgetOfExactType(ChangePassProvider);

  @override
  bool updateShouldNotify(ChangePassProvider oldWidget) =>
      validationErrorCallback != oldWidget.validationErrorCallback;
}
