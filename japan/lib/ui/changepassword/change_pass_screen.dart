import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:learn_japanese/data/user_repository.dart';
import 'package:learn_japanese/ui/changepassword/change_pass_widget.dart';
import 'package:learn_japanese/utils/uidata.dart';

import 'change_pass_provider.dart';
import 'change_pass_bloc.dart';

enum ChangePassValidationType {
  old,
  password,
  validatepassword,
  math
}

class ChangePassScreen extends StatefulWidget {
  const ChangePassScreen({
    Key key,
    @required ChangePassBloc changePassBloc,
  })  : _changePassBloc = changePassBloc,
        super(key: key);

  final ChangePassBloc _changePassBloc;

  @override
  ChangePassScreenState createState() {
    return new ChangePassScreenState(_changePassBloc);
  }
}

class ChangePassScreenState extends State<ChangePassScreen> {
  final ChangePassBloc _changePassBloc;
  ChangePassScreenState(this._changePassBloc);
 final scaffoldState = GlobalKey<ScaffoldState>();
  @override
  void initState() {
    super.initState();
    // this._changePassBloc.dispatch(LoadChangePassEvent());
  }

  @override
  void dispose() {
    super.dispose();
    _changePassBloc.dispose();
  }

  @override
  Widget build(BuildContext context) {
     String getType(ChangePassValidationType type) {
      switch (type) {
        case ChangePassValidationType.old:
          return "Enter old password";
        case ChangePassValidationType.password:
          return UIData.enter_valid_password;
        case ChangePassValidationType.validatepassword:
          return UIData.enter_valid_verifypassword;
        case ChangePassValidationType.math:
          return UIData.math;
        default:
      }
    }

    showValidationError(ChangePassValidationType type) {
      print(getType(type));
      scaffoldState.currentState.showSnackBar(SnackBar(
        content: Text(getType(type)),
        duration: Duration(seconds: 2),
      ));
    }

    return ChangePassProvider(
      validationErrorCallback: showValidationError,
      child: Scaffold(
          key: scaffoldState,
          backgroundColor: Color(0xffF4F6F9),
          body: BlocProvider<ChangePassBloc>(
              bloc: _changePassBloc,
              child: ChangePassWidget(userRepository: UserRepository(),))),
    );
  }
  
}
