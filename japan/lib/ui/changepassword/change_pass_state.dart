import 'package:equatable/equatable.dart';
import 'package:meta/meta.dart';

@immutable
abstract class ChangePassState extends Equatable {
  ChangePassState([Iterable props]) : super(props);

  /// Copy object for use in action
  ChangePassState getStateCopy();
}

/// UnInitialized
class ShowLoading extends ChangePassState {
  @override
  String toString() => 'ShowLoading';

  @override
  ChangePassState getStateCopy() {
    return ShowLoading();
  }
}

/// UnInitialized
class UnChangePassState extends ChangePassState {
  @override
  String toString() => 'UnChangePassState';

  @override
  ChangePassState getStateCopy() {
    return UnChangePassState();
  }
}

/// Initialized
class InChangePassState extends ChangePassState {
  @override
  String toString() => 'InChangePassState';

  @override
  ChangePassState getStateCopy() {
    return InChangePassState();
  }
}

class ErrorChangePassState extends ChangePassState {
  final String errorMessage;

  ErrorChangePassState(this.errorMessage);
  
  @override
  String toString() => 'ErrorChangePassState';

  @override
  ChangePassState getStateCopy() {
    return ErrorChangePassState(this.errorMessage);
  }
}
