import 'package:flutter/material.dart';
import 'package:learn_japanese/utils/uidata.dart';

class SplashScreen extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: Color(0xff310E31),
      body: Center(child: Text(UIData.title,style: TextStyle(color: Colors.white),)),
    );
  }
}
