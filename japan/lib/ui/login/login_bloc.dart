import 'dart:async';
import 'package:bloc/bloc.dart';
import 'package:learn_japanese/data/model/photo.dart';
import 'package:learn_japanese/data/preferences.dart';
import 'package:learn_japanese/data/sharepref_name.dart';
import 'package:learn_japanese/data/user_repository.dart';
import 'package:learn_japanese/service/network_service_response.dart';
import 'package:meta/meta.dart';

import 'login_event.dart';
import 'login_state.dart';

class LoginBloc extends Bloc<LoginEvent, LoginState> {
  UserRepository _userRepository;

  LoginBloc({
    @required UserRepository userRepository,
  })  : assert(userRepository != null),
        _userRepository = userRepository;

  LoginState get initialState => new LoginInitial();

  @override
  Stream<LoginState> mapEventToState(LoginEvent event) async* {
    if (event is LoginWithButtonPressed) {
      yield LoginLoading();
      try {
        //  await Future.delayed(Duration(seconds: 3));
        var result = await _userRepository.login(event.email, event.password);
        print(result.content.token);
        if (result.content.status == 200) {
          await Preference.load();
          Preference.setString(SharePrefName.TOKEN, result.content.token);
          yield LoginSuccess();
        } else {
          yield LoginFailure(error: result.content.message);
        }
      } on Exception catch (error) {
        print("error: $error");
        yield LoginFailure(error: "$error.");
      }
    }
  }
}
