import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:learn_japanese/data/user_repository.dart';
import 'package:learn_japanese/ui/login/login_bloc.dart';
import 'package:learn_japanese/utils/uidata.dart';

import 'login_provider.dart';
import 'login_widget.dart';

enum LoginValidationType { username, password }

class LoginScreen extends StatefulWidget {
  final UserRepository _userRepository;
  LoginScreen({Key key, @required UserRepository userRepository})
      : assert(userRepository != null),
        _userRepository = userRepository,
        super(key: key);
  @override
  LoginScreenState createState() {
    return new LoginScreenState();
  }
}

class LoginScreenState extends State<LoginScreen> {
  final scaffoldState = GlobalKey<ScaffoldState>();
  LoginBloc _loginBloc;
  UserRepository get _userRepository => widget._userRepository;

  @override
  void initState() {
    // TODO: implement initState
    super.initState();
    _loginBloc = LoginBloc(
      userRepository: _userRepository,
    );
  }

  @override
  Widget build(BuildContext context) {
    return loginScaffold();
  }

  Widget loginScaffold() => LoginProvider(
        validationErrorCallback: showValidationError,
        child: Scaffold(
          key: scaffoldState,
          backgroundColor: Color(0xffF4F6F9),
          body: BlocProvider<LoginBloc>(
            bloc: _loginBloc,
            child: Padding(
              padding: EdgeInsets.only(top: 32, left: 32, right: 32),
              child: LoginWidget(userRepository: _userRepository,),
            ),
          ),
        ),
      );
  showValidationError(LoginValidationType type) {
    scaffoldState.currentState.showSnackBar(SnackBar(
      content: Text(type == LoginValidationType.username
          ? UIData.enter_valid_usename
          : UIData.enter_valid_password),
      duration: Duration(seconds: 2),
    ));
  }
}
