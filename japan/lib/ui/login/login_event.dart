import 'dart:async';
import 'package:equatable/equatable.dart';
import 'package:meta/meta.dart';

import 'login_bloc.dart';
import 'login_state.dart';

@immutable
abstract class LoginEvent extends Equatable {
  LoginEvent([List props = const []]) : super(props);
}

class LoginWithButtonPressed extends LoginEvent {
  final String email;
  final String password;

  LoginWithButtonPressed({@required this.email, @required this.password})
      : super([email, password]);

  @override
  String toString() {
    return 'LoginWithButtonPressed { email: $email, password: $password }';
  }
}
