import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:learn_japanese/data/user_repository.dart';
import 'package:learn_japanese/ui/login/login_event.dart';
import 'package:learn_japanese/ui/register/register_screen.dart';
import 'package:learn_japanese/ui/resetpassword/reset_pass_bloc.dart';
import 'package:learn_japanese/ui/resetpassword/reset_pass_screen.dart';
import 'package:learn_japanese/utils/uidata.dart';

import 'login_bloc.dart';
import 'login_provider.dart';
import 'login_screen.dart';
import 'login_state.dart';

class LoginWidget extends StatefulWidget {
  final UserRepository _userRepository;

  LoginWidget({Key key, @required UserRepository userRepository})
      : assert(userRepository != null),
        _userRepository = userRepository,
        super(key: key);
  _LoginWidgetState createState() => _LoginWidgetState();
}

class _LoginWidgetState extends State<LoginWidget> {
  final _usernameController = TextEditingController();
  final _passwordController = TextEditingController();
  LoginBloc _loginBloc;

  UserRepository get _userRepository => widget._userRepository;

  @override
  void initState() {
    // TODO: implement initState
    super.initState();
    _loginBloc = BlocProvider.of<LoginBloc>(context);
  }

  @override
  Widget build(BuildContext context) {
    final email = TextFormField(
      controller: _usernameController,
      keyboardType: TextInputType.emailAddress,
      autofocus: false,
      decoration: InputDecoration(
          filled: true,
          fillColor: Colors.white,
          prefixIcon: Icon(
            Icons.person,
            color: UIData.colorPrimary(),
          ),
          hintText: 'Tài khoản',
          hintStyle: TextStyle(color: UIData.colorPrimary()),
          contentPadding: EdgeInsets.fromLTRB(20.0, 10.0, 20.0, 10.0),
          border: OutlineInputBorder(
            borderRadius: BorderRadius.circular(32.0),
          ),
          focusedBorder: OutlineInputBorder(
              borderRadius: BorderRadius.circular(32.0),
              borderSide: BorderSide(color: UIData.colorPrimary()))),
    );

    final password = TextFormField(
      controller: _passwordController,
      autofocus: false,
      obscureText: true,
      decoration: InputDecoration(
          filled: true,
          fillColor: Colors.white,
          prefixIcon: Icon(
            Icons.lock,
            color: UIData.colorPrimary(),
          ),
          hintText: 'Mật khẩu',
          hintStyle: TextStyle(color: UIData.colorPrimary()),
          contentPadding: EdgeInsets.fromLTRB(20.0, 10.0, 20.0, 10.0),
          border: OutlineInputBorder(borderRadius: BorderRadius.circular(32.0)),
          focusedBorder: OutlineInputBorder(
              borderRadius: BorderRadius.circular(32.0),
              borderSide: BorderSide(color: UIData.colorPrimary()))),
    );
    void showErrorValidate(BuildContext context, LoginValidationType type) {
      LoginProvider.of(context).validationErrorCallback(type);
    }

    void valdateUsernamePassword() {
      String username = _usernameController.text;
      String password = _passwordController.text;

      if (!(username?.length >= 6)) {
        showErrorValidate(context, LoginValidationType.username);
      } else {
        if (!(password?.length >= 6)) {
          showErrorValidate(context, LoginValidationType.password);
        } else {
          //logined
          print(username + '\n' + password);
          BlocProvider.of<LoginBloc>(context).dispatch(
            LoginWithButtonPressed(email: username, password: password),
          );
        }
      }
    }

    Widget loginButton(BuildContext context) {
      return Container(
        padding: EdgeInsets.symmetric(vertical: 16.0),
        width: 180,
        child: RaisedButton(
          shape: RoundedRectangleBorder(
            borderRadius: BorderRadius.circular(24),
          ),
          onPressed: () {
            valdateUsernamePassword();
          },
          padding: EdgeInsets.all(12),
          color: UIData.colorAccent(),
          child: Text('ĐĂNG NHẬP', style: TextStyle(color: Colors.white)),
        ),
      );
    }

    final forgotLabel = Container(
      alignment: Alignment.centerRight,
      child: InkWell(
        onTap: () {
          Navigator.of(context).push(
          MaterialPageRoute(builder: (context) {
            return ResetPassScreen(resetPassBloc: ResetPassBloc(),);
          }),
        );
        },
        child: Container(
          padding: EdgeInsets.symmetric(vertical: 12.0),
          child: Text(
            'Quên mật khẩu?',
            style: TextStyle(
              color: UIData.colorPrimary(),
            ),
          ),
        ),
      ),
    );

    return BlocListener(
      bloc: _loginBloc,
      listener: (BuildContext context, LoginState state) {
        if (state is LoginLoading) {
          showDialog(
              context: context,
              barrierDismissible: false,
              builder: (context) => WillPopScope(
                  onWillPop: () {},
                  child: Center(
                    child: CircularProgressIndicator(
                    ),
                  )));
        }
        if (state is LoginSuccess) {
          if (Navigator.canPop(context)) {
            Navigator.pop(context);
          }
          Navigator.of(context).pushNamedAndRemoveUntil(
              '/home', (Route<dynamic> route) => false);
        }
        if (state is LoginFailure) {
          Navigator.pop(context);
          Scaffold.of(context)
            ..hideCurrentSnackBar()
            ..showSnackBar(
              SnackBar(
                content: Row(
                  mainAxisAlignment: MainAxisAlignment.spaceBetween,
                  children: [
                    Container(
                        width: 250.0, child: Text(state.error, maxLines: 1)),
                    Icon(Icons.error)
                  ],
                ),
                backgroundColor: Colors.red,
              ),
            );
        }
      },
      child: BlocBuilder(
        bloc: _loginBloc,
        builder: (BuildContext context, LoginState state) {
          return ListView(
            // shrinkWrap: false,
            // padding: EdgeInsets.only(left: 24.0, right: 24.0),
            children: <Widget>[
              logo,
              SizedBox(height: 196.0),
              email,
              SizedBox(height: 8.0),
              password,
              SizedBox(height: 8.0),
              forgotLabel,
              loginButton(context),
              SizedBox(height: 72.0),
              notyetAccount(context)
            ],
          );
        },
      ),
    );
  }
}

final logo = Center(
  child: Text(
    "Đăng Nhập",
    style: TextStyle(fontSize: 22.0, color: UIData.colorPrimary()),
  ),
);

Widget notyetAccount(BuildContext context) {
  return Container(
    alignment: Alignment.bottomCenter,
    child: InkWell(
      onTap: () {
        Navigator.of(context).push(
          MaterialPageRoute(builder: (context) {
            return RegisterScreen();
          }),
        );
      },
      child: Container(
        padding: EdgeInsets.symmetric(vertical: 12.0),
        child: Row(
          crossAxisAlignment: CrossAxisAlignment.center,
          mainAxisAlignment: MainAxisAlignment.center,
          children: <Widget>[
            Text(
              'Chưa có tài khoản',
              style: TextStyle(
                color: UIData.colorPrimary(),
              ),
            ),
            Text(
              ' Đăng kí',
              style: TextStyle(
                fontWeight: FontWeight.bold,
                color: Colors.red,
              ),
            ),
          ],
        ),
      ),
    ),
  );
}
