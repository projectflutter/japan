import 'package:meta/meta.dart';

@immutable
abstract class ResetPassEvent {}

class LoadResetPassEvent extends ResetPassEvent {
  final String email;

  LoadResetPassEvent(this.email);
  @override
  String toString() => 'LoadResetPassEvent';
}
