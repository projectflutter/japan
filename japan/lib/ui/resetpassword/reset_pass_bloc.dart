import 'dart:async';
import 'package:bloc/bloc.dart';
import 'package:learn_japanese/data/user_repository.dart';

import 'reset_pass_event.dart';
import 'reset_pass_state.dart';

class ResetPassBloc extends Bloc<ResetPassEvent, ResetPassState> {
  UserRepository _repository = UserRepository();

  ResetPassState get initialState => new UnResetPassState();

  @override
  Stream<ResetPassState> mapEventToState(
    ResetPassEvent event,
  ) async* {
    yield ShowLoadingState();
    if (event is LoadResetPassEvent) {
      try {
        var result = await _repository.resetPass(event.email);
        if (result.content.status == 200) {
          yield InResetPassState(result.content);
        } else {
          yield ErrorResetPassState(result.content.message);
        }
      } catch (_, stackTrace) {
        print('$_ $stackTrace');
        yield ErrorResetPassState("Reset mật khẩu thất bại!");
      }
    }
  }
}
