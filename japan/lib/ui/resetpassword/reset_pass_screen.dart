import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:learn_japanese/utils/uidata.dart';
import 'package:learn_japanese/utils/validators.dart';

import 'reset_pass_bloc.dart';
import 'reset_pass_event.dart';
import 'reset_pass_state.dart';

class ResetPassScreen extends StatefulWidget {
  const ResetPassScreen({
    Key key,
    @required ResetPassBloc resetPassBloc,
  })  : _resetPassBloc = resetPassBloc,
        super(key: key);

  final ResetPassBloc _resetPassBloc;

  @override
  ResetPassScreenState createState() {
    return new ResetPassScreenState(_resetPassBloc);
  }
}

class ResetPassScreenState extends State<ResetPassScreen> {
  final ResetPassBloc _resetPassBloc;
  ResetPassScreenState(this._resetPassBloc);
  final _emailController = TextEditingController();
  bool isShowLoading = false;

  @override
  void initState() {
    super.initState();
    // this._resetPassBloc.dispatch(LoadResetPassEvent());
  }

  @override
  void dispose() {
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    Widget toolbar() {
      return InkWell(
        splashColor: Colors.transparent,
        highlightColor: Colors.transparent,
        onTap: () {
          Navigator.of(context).pop();
        },
        child: Container(
          child: Align(
            alignment: Alignment.centerLeft,
            child: Icon(Icons.arrow_back_ios),
          ),
        ),
      );
    }

    final logo = Center(
      child: Text(
        "Reset mật khẩu",
        style: TextStyle(fontSize: 22.0, color: UIData.colorPrimary()),
      ),
    );
    final email = Padding(
        padding: EdgeInsets.only(left: 32, right: 32, top: 22),
        child: TextFormField(
          controller: _emailController,
          autofocus: false,
          obscureText: false,
          decoration: InputDecoration(
              filled: true,
              fillColor: Colors.white,
              prefixIcon: Icon(
                Icons.email,
                color: UIData.colorPrimary(),
              ),
              hintText: 'Nhập email',
              hintStyle: TextStyle(color: UIData.colorPrimary()),
              contentPadding: EdgeInsets.fromLTRB(20.0, 10.0, 20.0, 10.0),
              border:
                  OutlineInputBorder(borderRadius: BorderRadius.circular(32.0)),
              focusedBorder: OutlineInputBorder(
                  borderRadius: BorderRadius.circular(32.0),
                  borderSide: BorderSide(color: UIData.colorPrimary()))),
        ));

    void valdateForm(BuildContext context) {
      String email = _emailController.text;
      if (!(Validators.isValidEmail(email))) {
        WidgetsBinding.instance.addPostFrameCallback((_) => Scaffold.of(context)
            .showSnackBar(SnackBar(content: Text("Invalid email!"))));
      } else {
        _resetPassBloc.dispatch(LoadResetPassEvent(email));
      }
    }

    Widget resetPassButton(BuildContext context) {
      return Container(
        padding: EdgeInsets.symmetric(vertical: 16.0),
        width: 180,
        child: RaisedButton(
          shape: RoundedRectangleBorder(
            borderRadius: BorderRadius.circular(24),
          ),
          onPressed: () {
            valdateForm(context);
          },
          padding: EdgeInsets.all(12),
          color: UIData.colorAccent(),
          child: Text('Reset mật khẩu', style: TextStyle(color: Colors.white)),
        ),
      );
    }

    return Scaffold(
        body: SafeArea(
      child: BlocListener<ResetPassEvent, ResetPassState>(
        bloc: _resetPassBloc,
        listener: (context, currentState) {
          if (currentState is ShowLoadingState) {
            isShowLoading = true;
            showDialog(
                context: context,
                barrierDismissible: false,
                builder: (context) => WillPopScope(
                    onWillPop: () {
                      return Future.value(false);
                    },
                    child: Center(
                      child: CircularProgressIndicator(),
                    )));
          }
          if (currentState is ErrorResetPassState) {
            Navigator.pop(context);
            _showdialog(currentState.errorMessage, context);
          }
          if (currentState is InResetPassState) {
            Navigator.pop(context);
            _showdialog(currentState.changePassResponse.message, context);
          }
        },
        child: BlocBuilder(
            bloc: _resetPassBloc,
            builder: (context, currentState) {
              return Column(
                children: <Widget>[
                  Padding(
                      padding: EdgeInsets.only(top: 0, left: 10),
                    child: toolbar(),
                  ),
                  Expanded(
                    child: Column(
                      mainAxisAlignment: MainAxisAlignment.center,
                      crossAxisAlignment: CrossAxisAlignment.center,
                      children: <Widget>[logo, email, resetPassButton(context)],
                    ),
                  )
                ],
              );
            }),
      ),
    ));
  }

  _showdialog(String content, BuildContext context) {
    print("Show");
    showDialog(
        barrierDismissible: true,
        context: context,
        builder: (context) => AlertDialog(
              title: Text("Thông báo"),
              content: Text(content),
              actions: <Widget>[
                FlatButton(
                  child: Text("ok"),
                  onPressed: () {
                    // setState(() {
                    // });
                    Navigator.of(context).pop();
                  },
                )
              ],
            ));
  }
}
