import 'package:equatable/equatable.dart';
import 'package:learn_japanese/data/model/change_pass_response.dart';
import 'package:meta/meta.dart';

@immutable
abstract class ResetPassState extends Equatable {
  ResetPassState([Iterable props]) : super(props);

  /// Copy object for use in action
  ResetPassState getStateCopy();
}
class ShowLoadingState extends ResetPassState {
  @override
  String toString() => 'ShowLoadingState';

  @override
  ResetPassState getStateCopy() {
    return ShowLoadingState();
  }
}
/// UnInitialized
class UnResetPassState extends ResetPassState {
  @override
  String toString() => 'UnResetPassState';

  @override
  ResetPassState getStateCopy() {
    return UnResetPassState();
  }
}

/// Initialized
class InResetPassState extends ResetPassState {
 final ChangePassResponse changePassResponse;

  InResetPassState(this.changePassResponse);
  @override
  String toString() => 'InResetPassState';

  @override
  ResetPassState getStateCopy() {
    return InResetPassState(changePassResponse);
  }
}

class ErrorResetPassState extends ResetPassState {
  final String errorMessage;

  ErrorResetPassState(this.errorMessage);
  
  @override
  String toString() => 'ErrorResetPassState';

  @override
  ResetPassState getStateCopy() {
    return ErrorResetPassState(this.errorMessage);
  }
}
