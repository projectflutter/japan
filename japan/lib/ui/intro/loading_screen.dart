import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:learn_japanese/ui/intro/loading_form_page.dart';

class LoadingScreen extends StatefulWidget {
  @override
  LoadingScreenState createState() {
    return new LoadingScreenState();
  }
}

class LoadingScreenState extends State<LoadingScreen> {
  @override
  void initState() {
    super.initState();
  }

  @override
  void dispose() {
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: Color(0xffF6F8FC),
      body: LoadingFormPage(),
    );
  }
}
