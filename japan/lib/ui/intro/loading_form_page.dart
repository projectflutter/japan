import 'package:flutter/material.dart';
import 'package:learn_japanese/data/user_repository.dart';
import 'package:learn_japanese/ui/login/login_screen.dart';
import 'package:learn_japanese/ui/register/register_screen.dart';
import 'package:learn_japanese/utils/uidata.dart';
import 'package:path/path.dart';

class LoadingFormPage extends StatelessWidget {
  static const String routeName = "/introForm";
  Color colortext = Color(0xff310E31);

  @override
  Widget build(BuildContext context) {
    return Container(
      padding: new EdgeInsets.symmetric(vertical: 32.0),
      child: Column(
        children: <Widget>[
          titlePage,
          Expanded(
            flex: 0,
            child: Center(
                child: Text("Tiện lợi",
                    style: TextStyle(fontSize: 18.0, color: colortext))),
          ),
          Expanded(
            flex: 0,
            child: Padding(
              padding: const EdgeInsets.all(15.0),
              child: Center(
                  child: Text(
                "Cải thiện ngôn ngữ của bạn bất cứ lúc nào thuận tiện cho bạn, bất cứ nơi nào!",
                style: TextStyle(fontSize: 14.0, color: colortext),
                textAlign: TextAlign.center,
              )),
            ),
          ),
          Expanded(
            flex: 6,
            child: Container(
              width: 200,
              height: 200,
              child: Image.asset(
                "assets/logo_loading.png",
              ),
            ),
          ),
          Expanded(
            flex: 1,
            child: Padding(
              padding: const EdgeInsets.only(left: 10.0, right: 10.0),
              child: Row(
                mainAxisAlignment: MainAxisAlignment.spaceBetween,
                crossAxisAlignment: CrossAxisAlignment.end,
                children: <Widget>[
                  Expanded(
                    flex: 1,
                    child: buttonSignUp(context),
                  ),
                  Expanded(
                    flex: 1,
                    child: buttonSignIn(context),
                  ),
                ],
              ),
            ),
          )
        ],
      ),
    );
  }

  Widget get titlePage {
    return Padding(
      padding: const EdgeInsets.only(top: 50, bottom: 20),
      child: Center(
          child: Text("HỌC TIẾNG NHẬT",
              style: TextStyle(fontSize: 22.0, color: colortext))),
    );
  }

  Widget buttonSignIn(BuildContext context) {
    return Container(
      padding: EdgeInsets.symmetric(vertical: 0.0, horizontal: 0.0),
      width: double.infinity,
      child: RaisedButton(
        padding: EdgeInsets.all(12.0),
        shape: StadiumBorder(),
        child: Text(
          "ĐĂNG NHẬP",
          style: TextStyle(color: Colors.white),
        ),
        color: UIData.colorAccent(),
        onPressed: () {
          _onButtonSignInPress(context);
        },
      ),
    );
  }

  Widget buttonSignUp(BuildContext context) {
    return InkWell(
      onTap: (){_onButtonSignUpPress(context);},
          child: Container(
        padding: const EdgeInsets.all(12.0),
        child: Text(
          "ĐĂNG KÍ",
          style: TextStyle(fontSize: 18.0),
          textAlign: TextAlign.center,
        ),
      ),
    );
  }

  void _onButtonSignInPress(BuildContext context) {
     Navigator.of(context).push(
          MaterialPageRoute(builder: (context) {
            return LoginScreen(userRepository: UserRepository(),);
          }),
        );
  }
  void _onButtonSignUpPress(BuildContext context) {
     Navigator.of(context).push(
          MaterialPageRoute(builder: (context) {
            return RegisterScreen();
          }),
        );
  }
}
