import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:learn_japanese/data/model/detail_response.dart';
import 'package:learn_japanese/data/model/school_menu.dart';
import 'package:learn_japanese/ui/detail/detail_school_bloc.dart';
import 'package:learn_japanese/ui/detail/detail_school_event.dart';
import 'package:learn_japanese/ui/detail/detail_school_state.dart';
import 'package:learn_japanese/ui/survey/survey_page.dart';
import 'package:learn_japanese/utils/uidata.dart';

class DetailSchoolPage extends StatefulWidget {
  final Data menuData;
  DetailSchoolPage({Key key, this.menuData}) : super(key: key);

  _DetailSchoolPageState createState() =>
      _DetailSchoolPageState(menudata: menuData);
}

class _DetailSchoolPageState extends State<DetailSchoolPage> {
  final Data menudata;
  _DetailSchoolPageState({this.menudata});
  DetailSchoolBloc _detailSchoolBloc;
  @override
  void initState() {
    super.initState();
    _detailSchoolBloc = DetailSchoolBloc();
    _detailSchoolBloc.dispatch(LoadDetailSchoolEvent(id: menudata.id));
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: Colors.white,
      body: BlocBuilder(
          bloc: _detailSchoolBloc,
          builder: (BuildContext context, DetailSchoolState state) {
            return Column(
              children: <Widget>[
                appbarCustomize(menudata),
                Expanded(
                  flex: 1,
                  child: Padding(
                    padding: const EdgeInsets.only(top: 8.0),
                    child: MediaQuery.removePadding(
                      child: bodyList(state),
                      removeTop: true,
                      context: context,
                    ),
                  ),
                )
              ],
            );
          }),
    );
  }

  Widget appbarCustomize(Data menudata) {
    return Container(
      width: double.infinity,
      height: 128.0,
      color: UIData.colorPrimaryLight(),
      child: Stack(
        children: <Widget>[
          Align(
              alignment: Alignment.centerLeft,
              child: InkWell(
                onTap: () {
                  Navigator.of(context).pop();
                },
                child: Container(
                  child: Icon(
                    Icons.arrow_back_ios,
                    color: Colors.white,
                  ),
                ),
              )),
          Align(
              alignment: Alignment.center,
              child: Container(
                child: Text(
                  UIData.title,
                  textAlign: TextAlign.center,
                  style: TextStyle(color: Colors.white, fontSize: 18.0),
                ),
              )),
          Positioned(
            child: Align(
              alignment: Alignment.bottomLeft,
              child: Container(
                padding: EdgeInsets.all(10),
                child: Text(
                  UIData.title,
                  style: TextStyle(color: Colors.white, fontSize: 16.0),
                ),
              ),
            ),
          ),
          Positioned(
            child: Align(
              alignment: Alignment.bottomRight,
              child: Container(
                padding: EdgeInsets.all(10),
                child: Text(
                  menudata.schoolTitleDetail,
                  style: TextStyle(color: Color(0xffFFBA08), fontSize: 18.0),
                ),
              ),
            ),
          )
        ],
      ),
    );
  }

  Widget bodyList(DetailSchoolState state) {
    if (state is UnDetailSchoolState) {
      return Center(
        child: CircularProgressIndicator(),
      );
    }
    if (state is InDetailSchoolState) {
      return ListView.builder(
        itemCount: state.detailResponse.data.length,
        itemBuilder: (context, index) {
          return listItem(state.detailResponse.data[index]);
        },
      );
    }
    if (state is ErrorDetailSchoolState) {
      return Center(
        child: GestureDetector(
          onTap: () {
            _detailSchoolBloc.dispatch(LoadDetailSchoolEvent(id: menudata.id));
          },
          child: Column(
            crossAxisAlignment: CrossAxisAlignment.center,
            mainAxisAlignment: MainAxisAlignment.center,
            children: <Widget>[
              Text(state.errorMessage),
              Text("Click to Reload"),
            ],
          ),
        ),
      );
    }
    if (state is EmptySchoolState) {
      return Center(
        child: Column(
          crossAxisAlignment: CrossAxisAlignment.center,
          mainAxisAlignment: MainAxisAlignment.center,
          children: <Widget>[
            Text("Empty Data!"),
          ],
        ),
      );
    }
  }

  Widget listItem(DataDetail menu) {
    return InkWell(
      onTap: () => itemListClick(menu),
      child: Card(
        color: Colors.white,
        shape: RoundedRectangleBorder(
          borderRadius: BorderRadius.circular(20.0),
        ),
        elevation: 2.0,
        child: Padding(
          padding: const EdgeInsets.all(12.0),
          child: Row(
            mainAxisAlignment: MainAxisAlignment.spaceBetween,
            children: <Widget>[
              Icon(
                Icons.insert_drive_file,
                color: UIData.colorPrimary(),
              ),
              Flexible(
                fit: FlexFit.tight,
                child: Padding(
                  padding: const EdgeInsets.only(left: 10.0),
                  child: Text(
                    menu.categoryName,
                    maxLines: 1,
                    overflow: TextOverflow.ellipsis,
                    style: TextStyle(fontSize: 16.0),
                  ),
                ),
              ),
              Align(
                alignment: Alignment.centerRight,
                child: Icon(
                  Icons.keyboard_arrow_right,
                  color: UIData.colorPrimary(),
                ),
              )
            ],
          ),
        ),
      ),
    );
  }

  itemListClick(DataDetail menu) {
    Navigator.of(context).push(
      MaterialPageRoute(builder: (context) {
        return SurveyPage(
          catelogyId: menu.id,
          titleName: menu.categoryName,
        );
      }),
    );
  }
}
