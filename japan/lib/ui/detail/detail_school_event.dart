import 'package:meta/meta.dart';

@immutable
abstract class DetailSchoolEvent {}

class LoadDetailSchoolEvent extends DetailSchoolEvent {
  int id;
  LoadDetailSchoolEvent({this.id});
  @override
  String toString() => 'LoadDetailSchoolEvent';
}
