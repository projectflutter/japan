import 'package:flutter/material.dart';

import 'detail_school_bloc.dart';


class DetailSchoolBlocProvider extends InheritedWidget {
  final DetailSchoolBloc bloc;

  DetailSchoolBlocProvider({Key key, Widget child})
      : bloc = DetailSchoolBloc(),
        super(key: key, child: child);

  @override
  bool updateShouldNotify(_) {
    return true;
  }

  static DetailSchoolBloc of(BuildContext context) {
    return (context.inheritFromWidgetOfExactType(DetailSchoolBlocProvider)
            as DetailSchoolBlocProvider)
        .bloc;
  }
}
