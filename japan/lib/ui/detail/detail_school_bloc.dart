import 'package:bloc/bloc.dart';
import 'package:learn_japanese/data/model/detail_response.dart';
import 'package:learn_japanese/data/user_repository.dart';
import 'package:learn_japanese/ui/detail/detail_school_event.dart';
import 'package:learn_japanese/ui/detail/detail_school_state.dart';

class DetailSchoolBloc extends Bloc<DetailSchoolEvent, DetailSchoolState> {
  UserRepository _userRepository = UserRepository();
  @override
  // TODO: implement initialState
  DetailSchoolState get initialState => UnDetailSchoolState();

  @override
  Stream<DetailSchoolState> mapEventToState(DetailSchoolEvent event) async* {
    try {
      yield initialState;
      if (event is LoadDetailSchoolEvent) {
        var result = await _userRepository.getdetail(event.id);
        if (result.content.status == 200) {
          if (result.content.data != null && result.content.data.length > 0) {
            yield InDetailSchoolState(result.content);
          } else {
            yield EmptySchoolState();
          }
        } else {
          yield ErrorDetailSchoolState("Error get detail!");
        }
      }
    } catch (e) {
      yield ErrorDetailSchoolState("Error get detail!");
    }
  }
}
