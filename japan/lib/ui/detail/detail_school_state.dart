import 'package:equatable/equatable.dart';
import 'package:learn_japanese/data/model/detail_response.dart';
import 'package:meta/meta.dart';

@immutable
abstract class DetailSchoolState extends Equatable {
  DetailSchoolState([Iterable props]) : super(props);
  @override
  DetailSchoolState getStateCopy();
}

/// UnInitialized
class UnDetailSchoolState extends DetailSchoolState {
  @override
  String toString() => 'UnDetailSchoolState';

  @override
  DetailSchoolState getStateCopy() {
    return UnDetailSchoolState();
  }
}

/// Initialized
class InDetailSchoolState extends DetailSchoolState {
  final DetailResponse detailResponse;

  InDetailSchoolState(this.detailResponse);
  @override
  String toString() => 'InDetailSchoolState';

  @override
  DetailSchoolState getStateCopy() {
    // TODO: implement getStateCopy
    return InDetailSchoolState(detailResponse);
  }
}

class EmptySchoolState extends DetailSchoolState {
  EmptySchoolState();

  @override
  String toString() => 'ErrorDetailSchoolState';

  @override
  DetailSchoolState getStateCopy() {
    return EmptySchoolState();
  }
}

class ErrorDetailSchoolState extends DetailSchoolState {
  final String errorMessage;

  ErrorDetailSchoolState(this.errorMessage);

  @override
  String toString() => 'ErrorDetailSchoolState';

  @override
  DetailSchoolState getStateCopy() {
    // TODO: implement getStateCopy
    return ErrorDetailSchoolState("Error");
  }
}
