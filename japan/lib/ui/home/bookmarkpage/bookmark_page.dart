import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:learn_japanese/data/model/school_menu.dart';
import 'package:learn_japanese/ui/home/bookmarkpage/bookmark_bloc.dart';
import 'package:learn_japanese/ui/home/bookmarkpage/bookmark_event.dart';
import 'package:learn_japanese/ui/home/bookmarkpage/bookmark_state.dart';
import 'package:learn_japanese/utils/uidata.dart';

import 'itempage/item_page.dart';

class BookmarkPage extends StatefulWidget {
  BookmarkPage({Key key, this.color}) : super(key: key);
  final Color color;

  _BookmarkState createState() => _BookmarkState();
}

class _BookmarkState extends State<BookmarkPage>
    with AutomaticKeepAliveClientMixin<BookmarkPage> {
  BookmarkBloc _bookmarkBloc;
  @override
  void initState() {
    super.initState();
    _bookmarkBloc = BookmarkBloc();
    _bookmarkBloc.dispatch(LoadListTabBookmarkEvent());
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: BlocBuilder(
          bloc: _bookmarkBloc,
          builder: (context, BookmarkState state) {
            return bodyListBookmark(state);
          }),
    );
  }

  @override
  bool get wantKeepAlive => true;

  Widget bodyListBookmark(BookmarkState state) {
     if (state is UnBookmarkState) {
      return Center(
        child: CircularProgressIndicator(),
      );
    }
 
    if (state is ListTabBookmarkState) {
         print(state.listTabBookmark);
      return Column(
        children: <Widget>[
          appBar(),
          Expanded(flex: 1, child: contentList(state.listTabBookmark))
        ],
      );
    }
    if (state is ErrorListTabBookmarkState)
      return Column(
        children: <Widget>[
          appBar(),
          Expanded(flex: 1, child: Center(child: Text("Error get menu Item!")))
        ],
      );
  }

  Widget appBar() {
    return Container(
      padding: EdgeInsets.only(top: 24.0),
      width: double.infinity,
      color: UIData.colorPrimaryLight(),
      height: 96,
      child: Center(
          child: Text(
        UIData.titleAppBarBookmark,
        style: TextStyle(color: Colors.white, fontSize: 18.0),
      )),
    );
  }

  Widget contentList(List<Data> menu) {
    List<Tab> listChildrenTab = [];
    for (var item in menu) {
      listChildrenTab.add(new Tab(
        text: item.schoolTitle,
      ));
    }
    List<ItemPage> listChildrenPage = [];
    for (var item in menu) {
      listChildrenPage.add(new ItemPage(
        id: item.id,
      ));
    }
    return DefaultTabController(
      length: menu.length,
      child: Scaffold(
        appBar: new PreferredSize(
          preferredSize: Size.fromHeight(35.0),
          child: new Column(
            mainAxisAlignment: MainAxisAlignment.start,
            children: [
              new TabBar(
                  isScrollable: true,
                  labelColor: UIData.colorPrimaryLight(),
                  labelStyle: TextStyle(color: UIData.colorPrimaryLight()),
                  unselectedLabelColor: Colors.grey,
                  indicatorColor: UIData.colorPrimaryLight(),
                  tabs: listChildrenTab),
            ],
          ),
        ),
        body: TabBarView(
          children: listChildrenPage,
        ),
      ),
    );
  }
}
