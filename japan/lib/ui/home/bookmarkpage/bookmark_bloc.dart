import 'dart:async';
import 'dart:convert';
import 'package:bloc/bloc.dart';
import 'package:learn_japanese/data/model/school_menu.dart';
import 'package:learn_japanese/data/preferences.dart';
import 'package:learn_japanese/data/sharepref_name.dart';
import 'package:learn_japanese/data/user_repository.dart';

import 'bookmark_event.dart';
import 'bookmark_state.dart';

class BookmarkBloc extends Bloc<BookmarkEvent, BookmarkState> {
  UserRepository _userRepository = UserRepository();
  static final BookmarkBloc _bookmarkBlocSingleton =
      new BookmarkBloc._internal();
  factory BookmarkBloc() {
    return _bookmarkBlocSingleton;
  }
  BookmarkBloc._internal();

  BookmarkState get initialState => new UnBookmarkState();

  @override
  Stream<BookmarkState> mapEventToState(
    BookmarkEvent event,
  ) async* {
    if (event is LoadListTabBookmarkEvent) {
      try {
        yield UnBookmarkState();
        await Preference.load();
        Iterable result =
            json.decode(Preference.getString(SharePrefName.LIST_MENU));
        List<Data> listData =
            result.map((model) => Data.fromJson(model)).toList();
        yield ListTabBookmarkState(listData);
      } catch (_, stackTrace) {
        print("$_");
        yield ErrorListTabBookmarkState();
      }
    }

    
  }
}
