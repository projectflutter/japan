import 'package:equatable/equatable.dart';
import 'package:learn_japanese/data/model/school_menu.dart';
import 'package:meta/meta.dart';

@immutable
abstract class BookmarkState extends Equatable {
  BookmarkState([Iterable props]) : super(props);

  /// Copy object for use in action
  BookmarkState getStateCopy();
}

/// UnInitialized
class UnBookmarkState extends BookmarkState {
  @override
  String toString() => 'UnBookmarkState';

  @override
  BookmarkState getStateCopy() {
    return UnBookmarkState();
  }
}

class ListTabBookmarkState extends BookmarkState {
  final List<Data> listTabBookmark;

  ListTabBookmarkState(this.listTabBookmark);
  @override
  String toString() => 'ListTabBookmarkState';

  @override
  BookmarkState getStateCopy() {
    return ListTabBookmarkState(listTabBookmark);
  }
}

class ErrorListTabBookmarkState extends BookmarkState {

  ErrorListTabBookmarkState();
  
  @override
  String toString() => 'ErrorBookmarkState';

  @override
  BookmarkState getStateCopy() {
    return ErrorListTabBookmarkState();
  }
}
