import 'package:equatable/equatable.dart';
import 'package:learn_japanese/data/model/bookmar_item.dart';
import 'package:meta/meta.dart';

@immutable
abstract class ItemPageBookmarkState extends Equatable {
  ItemPageBookmarkState([Iterable props]) : super(props);

  /// Copy object for use in action
  ItemPageBookmarkState getStateCopy();
}

/// unitialized
class ShowLoading extends ItemPageBookmarkState {
  ShowLoading();
  @override
  String toString() => 'ShowLoading';

  @override
  ItemPageBookmarkState getStateCopy() {
    return ShowLoading();
  }
}

/// Initialized
class LoadListItemBookmarkState extends ItemPageBookmarkState {
  final BookmarkItem item;

  LoadListItemBookmarkState(this.item);
  @override
  String toString() => 'InBookmarkState';

  @override
  ItemPageBookmarkState getStateCopy() {
    return LoadListItemBookmarkState(item);
  }
}

class ListItemEmptyBookmarkState extends ItemPageBookmarkState {
  @override
  String toString() => 'ListItemEmptyBookmarkState';

  @override
  ItemPageBookmarkState getStateCopy() {
    return ListItemEmptyBookmarkState();
  }
}

class ErrorBookmarkState extends ItemPageBookmarkState {
  final String errorMessage;

  ErrorBookmarkState(this.errorMessage);

  @override
  String toString() => 'ErrorBookmarkState';

  @override
  ItemPageBookmarkState getStateCopy() {
    return ErrorBookmarkState(this.errorMessage);
  }
}
