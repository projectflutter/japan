import 'dart:async';
import 'package:meta/meta.dart';

@immutable
abstract class ItemPageBookmarkEvent {
  
}

class LoadItemBookmarkEvent extends ItemPageBookmarkEvent {
  final int idPage;

  LoadItemBookmarkEvent({this.idPage});
  @override
  String toString() => 'LoadItemBookmarkEvent';
}