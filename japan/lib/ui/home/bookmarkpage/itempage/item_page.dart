import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:learn_japanese/data/model/bookmar_item.dart';
import 'package:learn_japanese/ui/home/bookmarkpage/detailbookmark/detail_bookmar_screen.dart';
import 'package:learn_japanese/ui/home/bookmarkpage/itempage/item_page_bookmark_bloc.dart';
import 'package:learn_japanese/ui/home/bookmarkpage/itempage/item_page_bookmark_state.dart';
import 'package:learn_japanese/utils/uidata.dart';

import 'item_page_bookmark_event.dart';

class ItemPage extends StatefulWidget {
  final int id;
  ItemPage({Key key, this.id}) : super(key: key);

  _ItemPageState createState() => _ItemPageState(id);
}

class _ItemPageState extends State<ItemPage>
    with AutomaticKeepAliveClientMixin<ItemPage> {
  int id;
  ItemPageBookmarkBloc _itemPageBookmarkBloc;
  _ItemPageState(int id) {
    this.id = id;
  }

  @override
  void initState() {
    super.initState();
    _itemPageBookmarkBloc = ItemPageBookmarkBloc();
    _itemPageBookmarkBloc.dispatch(LoadItemBookmarkEvent(idPage: this.id));
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: BlocBuilder(
          bloc: _itemPageBookmarkBloc,
          builder: (context, ItemPageBookmarkState state) {
            if (state is ShowLoading) {
              return Center(
                child: CircularProgressIndicator(),
              );
            }
            if (state is ErrorBookmarkState) {
              return Center(
                child: GestureDetector(
                  onTap: () {
                    _itemPageBookmarkBloc
                        .dispatch(LoadItemBookmarkEvent(idPage: id));
                  },
                  child: Column(
                    crossAxisAlignment: CrossAxisAlignment.center,
                    mainAxisAlignment: MainAxisAlignment.center,
                    children: <Widget>[
                      Text(state.errorMessage),
                      Text("Click to Reload"),
                    ],
                  ),
                ),
              );
            }

            if (state is LoadListItemBookmarkState) {
              return ListView.builder(
                itemCount: state.item.data.length,
                itemBuilder: (context, index) {
                  return listItem(state.item.data[index]);
                },
              );
            }
            if (state is ListItemEmptyBookmarkState) {
              return Center(
                child: Column(
                  crossAxisAlignment: CrossAxisAlignment.center,
                  mainAxisAlignment: MainAxisAlignment.center,
                  children: <Widget>[
                    Text("Empty Data!"),
                  ],
                ),
              );
            }
          }),
    );
  }

  @override
  // TODO: implement wantKeepAlive
  bool get wantKeepAlive => true;

  Widget listItem(DataBookmark menu) {
    return InkWell(
      onTap: () {
        _onClickItem(menu);
      },
      child: Card(
        elevation: 2.0,
        child: Padding(
          padding: const EdgeInsets.all(8.0),
          child: Row(
            mainAxisAlignment: MainAxisAlignment.spaceBetween,
            children: <Widget>[
              Image.asset(
                UIData.imageCircleBookmark,
                scale: 2,
              ),
              Flexible(
                fit: FlexFit.tight,
                child: Padding(
                  padding: const EdgeInsets.only(left: 10.0),
                  child: Text(
                    menu.questionTitle,
                    maxLines: 1,
                    overflow: TextOverflow.ellipsis,
                    style: TextStyle(fontSize: 16.0),
                  ),
                ),
              ),
              Align(
                alignment: Alignment.centerRight,
                child: Icon(
                  Icons.keyboard_arrow_right,
                  color: UIData.colorPrimary(),
                ),
              )
            ],
          ),
        ),
      ),
    );
  }

  void _onClickItem(DataBookmark menu) {
    print(menu.id);
     
    Navigator.of(context).push(
      MaterialPageRoute(builder: (context) {
        return DetailBookmarkScreen(questionId:menu.id);
      }),
    );
  }
}
