import 'dart:async';
import 'package:bloc/bloc.dart';
import 'package:learn_japanese/data/user_repository.dart';

import 'item_page_bookmark_event.dart';
import 'item_page_bookmark_state.dart';

class ItemPageBookmarkBloc extends Bloc<ItemPageBookmarkEvent, ItemPageBookmarkState> {
 UserRepository _userRepository = UserRepository();
  ItemPageBookmarkState get initialState => new ShowLoading();

  @override
  Stream<ItemPageBookmarkState> mapEventToState(
    ItemPageBookmarkEvent event,
  ) async* {
   if (event is LoadItemBookmarkEvent) {
      try {
        yield initialState;
        var result = await _userRepository.getBookmarkItem(event.idPage);
        print(result);
        if (result.content.status == 200) {
          if (result.content.data != null && result.content.data.length > 0) {
            yield LoadListItemBookmarkState(result.content);
          } else {
            yield ListItemEmptyBookmarkState();
          }
        } else {
          yield ErrorBookmarkState("Error get bookmark List!");
        }
      } catch (_, stackTrace) {
        print("$_");
        yield ErrorBookmarkState("Error get bookmark List!");
      }
    }
  }
}
