import 'dart:async';
import 'package:meta/meta.dart';

@immutable
abstract class DetailBookmarkEvent {}

class LoadDetailBookmarkEvent extends DetailBookmarkEvent {
  final int questionid;

  LoadDetailBookmarkEvent(this.questionid);
  @override
  String toString() => 'LoadDetailBookmarkEvent';
}
