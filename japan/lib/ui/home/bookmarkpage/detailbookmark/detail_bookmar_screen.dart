import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:flutter_html_view/flutter_html_view.dart';
import 'package:learn_japanese/ui/home/bookmarkpage/detailbookmark/detail_bookmark_bloc.dart';
import 'package:learn_japanese/ui/view_result/radio_model_result.dart';
import 'package:learn_japanese/utils/uidata.dart';

import 'detail_bookmark_event.dart';
import 'detail_bookmark_model.dart';
import 'detail_bookmark_state.dart';

class DetailBookmarkScreen extends StatefulWidget {
  final int questionId;
  DetailBookmarkScreen({Key key, this.questionId}) : super(key: key);

  _DetailBookmarkScreenState createState() =>
      _DetailBookmarkScreenState(questionId: questionId);
}

class _DetailBookmarkScreenState extends State<DetailBookmarkScreen> {
  final int questionId;
  bool isExpand = false;
  DataDetailBookmark data;
  List<Widget> listWidget = new List<Widget>();
  DetailBookmarkBloc _detailBookmarkBloc;
  _DetailBookmarkScreenState({this.questionId});
  @override
  void initState() {
    super.initState();
    _detailBookmarkBloc = DetailBookmarkBloc();
    _detailBookmarkBloc.dispatch(LoadDetailBookmarkEvent(questionId));
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: BlocBuilder<DetailBookmarkEvent, DetailBookmarkState>(
          bloc: _detailBookmarkBloc,
          builder: (context, state) {
            return Column(
              children: <Widget>[
                _buildHeader(),
                Expanded(child: _buildQuestionBookmark(state))
              ],
            );
          }),
    );
  }

  Widget _buildHeader() {
    return Padding(
      padding: const EdgeInsets.only(top: 24.0),
      child: Container(
        padding: EdgeInsets.all(10.0),
        child: Column(
          children: <Widget>[
            Row(
              children: <Widget>[
                Align(
                  alignment: Alignment.topLeft,
                  child: Container(
                    width: 42.0,
                    height: 42.0,
                    child: FloatingActionButton(
                      backgroundColor: Colors.white,
                      onPressed: () {
                        Navigator.of(context).pop();
                      },
                      child: Icon(
                        Icons.close,
                        color: UIData.colorPrimary(),
                      ),
                    ),
                  ),
                ),
                Padding(
                  padding: const EdgeInsets.only(left: 15.0),
                  child: Text(
                    "Bookmark",
                    style: TextStyle(color: UIData.colorPrimaryLight()),
                  ),
                )
              ],
            ),
          ],
        ),
      ),
    );
  }

  Widget _buildQuestionBookmark(DetailBookmarkState state) {
    if (state is UnDetailBookmarkState) {
      return Center(
        child: CircularProgressIndicator(),
      );
    }

    if (state is InDetailBookmarkState) {
      data = state.data;
      for (var i = 0; i < state.data.answers.length; i++) {
        var item = state.data.answers[i];
        listWidget.add(AnswerItem(RadioModelResult(i, item.id, -1,
            item.isCorrected, "${item.answerSort}", item.answerTitle)));
      }

      return Padding(
        padding: const EdgeInsets.all(8.0),
        child: Column(
          mainAxisSize: MainAxisSize.min,
          children: <Widget>[
            Align(
                alignment: Alignment.topLeft,
                child: Text('${data.questionSort + 1}. ${data.questionTitle}')),
            ListView(
              physics: NeverScrollableScrollPhysics(),
              shrinkWrap: true,
              children: listWidget,
            ),
            InkWell(
              onTap: () {
                setState(() {
                  isExpand = !isExpand;
                });
              },
              child: Align(
                alignment: Alignment.topLeft,
                child: Row(
                  children: <Widget>[
                    Text(
                      "Xem giải thích",
                      style: TextStyle(color: UIData.colorGreen()),
                    ),
                    handlerMoreLess(),
                  ],
                ),
              ),
            ),
            handlerShowhideText()
          ],
        ),
      );
    }

    if (state is ErrorDetailBookmarkState) {
      return Center(
          child: GestureDetector(
        onTap: () {
          _detailBookmarkBloc.dispatch(LoadDetailBookmarkEvent(questionId));
        },
        child: Column(
          crossAxisAlignment: CrossAxisAlignment.center,
          mainAxisAlignment: MainAxisAlignment.center,
          children: <Widget>[
            Text(state.errorMessage),
            Text("Click to Reload"),
          ],
        ),
      ));
    }
  }

  Widget handlerMoreLess() {
    print(isExpand);
    return isExpand
        ? Icon(
            Icons.expand_less,
            color: UIData.colorGreen(),
          )
        : Icon(
            Icons.expand_more,
            color: UIData.colorGreen(),
          );
  }

  Widget handlerShowhideText() {
    if (isExpand) {
      return Flexible(
        child: Container(
            decoration: BoxDecoration(
                borderRadius: BorderRadius.circular(10.0),
                border: Border.all(color: Colors.green, width: 1.0)),
            width: double.infinity,
            height: 100,
            child: HtmlView(
              data: data.questionExplain,
            )),
      );
    } else {
      return Container();
    }
  }
}

class AnswerItem extends StatelessWidget {
  final RadioModelResult _item;
  AnswerItem(this._item);
  @override
  Widget build(BuildContext context) {
    return new Container(
      decoration: new BoxDecoration(
          color: checkColor(_item),
          borderRadius: new BorderRadius.all(const Radius.circular(15.0))),
      margin: new EdgeInsets.only(bottom: 10.0, top: 10.0),
      child: new Row(
        mainAxisSize: MainAxisSize.max,
        children: <Widget>[
          new Container(
            margin: EdgeInsets.all(3.0),
            height: 32.0,
            width: 32.0,
            child: new Center(
              child: new Text('${_item.index+1}',
                  style: new TextStyle(
                      color: Colors.black,
                      //fontWeight: FontWeight.bold,
                      fontSize: 18.0)),
            ),
            decoration:
                new BoxDecoration(shape: BoxShape.circle, color: Colors.white),
          ),
          Expanded(
            child: new Container(
              margin: new EdgeInsets.only(left: 10.0),
              child: new Text(
                _item.text,
                textAlign: TextAlign.start,
              ),
            ),
          ),
          Padding(
            padding: const EdgeInsets.all(8.0),
            child:
                Align(alignment: Alignment.topRight, child: handlerIcon(_item)),
          )
        ],
      ),
    );
  }

  Color checkColor(RadioModelResult item) {
    if (item.isCorrect == 1) {
      return UIData.colorCorrect();
    }
    return Colors.white;
  }

  Widget handlerIcon(RadioModelResult item) {
    if (item.isCorrect == 1) {
      return Icon(
        Icons.done,
        color: UIData.colorGreen(),
      );
    }

    return Container();
  }
}
