import 'package:equatable/equatable.dart';
import 'package:meta/meta.dart';

import 'detail_bookmark_model.dart';

@immutable
abstract class DetailBookmarkState extends Equatable {
  DetailBookmarkState([Iterable props]) : super(props);

  /// Copy object for use in action
  DetailBookmarkState getStateCopy();
}

/// UnInitialized
class UnDetailBookmarkState extends DetailBookmarkState {
  @override
  String toString() => 'UnDetailBookmarkState';

  @override
  DetailBookmarkState getStateCopy() {
    return UnDetailBookmarkState();
  }
}

/// Initialized
class InDetailBookmarkState extends DetailBookmarkState {
  final DataDetailBookmark data;

  InDetailBookmarkState(this.data);
  @override
  String toString() => 'InDetailBookmarkState';

  @override
  DetailBookmarkState getStateCopy() {
    return InDetailBookmarkState(data);
  }
}

class ErrorDetailBookmarkState extends DetailBookmarkState {
  final String errorMessage;

  ErrorDetailBookmarkState(this.errorMessage);
  
  @override
  String toString() => 'ErrorDetailBookmarkState';

  @override
  DetailBookmarkState getStateCopy() {
    return ErrorDetailBookmarkState(this.errorMessage);
  }
}
