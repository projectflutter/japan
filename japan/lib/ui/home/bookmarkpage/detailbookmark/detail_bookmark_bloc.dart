import 'dart:async';
import 'package:bloc/bloc.dart';
import 'package:learn_japanese/data/user_repository.dart';

import 'detail_bookmark_event.dart';
import 'detail_bookmark_state.dart';

class DetailBookmarkBloc
    extends Bloc<DetailBookmarkEvent, DetailBookmarkState> {
  UserRepository _userRepository = UserRepository();
  DetailBookmarkState get initialState => new UnDetailBookmarkState();

  @override
  Stream<DetailBookmarkState> mapEventToState(
    DetailBookmarkEvent event,
  ) async* {
    if (event is LoadDetailBookmarkEvent) {
      yield initialState;
      try {
        var result = await _userRepository.getDetailBookmark(event.questionid);
        if (result.content.status == 200) {
           yield InDetailBookmarkState(result.content.data);
        } else {
          yield ErrorDetailBookmarkState(result.content.message);
        }
      } catch (_, stackTrace) {
        print('$_ $stackTrace');
        yield ErrorDetailBookmarkState("Error get detail bookmark!");
      }
    }
  }
}
