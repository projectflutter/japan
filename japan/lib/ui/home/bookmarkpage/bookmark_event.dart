import 'package:meta/meta.dart';


@immutable
abstract class BookmarkEvent {
}
class LoadListTabBookmarkEvent extends BookmarkEvent {
  @override
  String toString() => 'LoadListTabBookmarkEvent';
}


