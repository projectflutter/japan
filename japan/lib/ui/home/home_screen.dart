import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';

import 'bookmarkpage/bookmark_page.dart';
import 'profilepage/profile_widget.dart';
import 'schoolpage/school_page.dart';

class HomeScreen extends StatefulWidget {
  @override
  HomeScreenState createState() {
    return new HomeScreenState();
  }
}

class HomeScreenState extends State<HomeScreen>
    with SingleTickerProviderStateMixin {
  int _currentIndex = 0;
  final List<Widget> _children = [
    SchoolPage(),
    BookmarkPage(color: Colors.deepOrange),
    ProfilePage(color: Colors.green)
  ];
  PageController _tabController;
  @override
  void initState() {
    _tabController = new PageController(initialPage: _currentIndex);
    super.initState();
  }

  @override
  void dispose() {
    _tabController.dispose();
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    return DefaultTabController(
      length: 3,
      child: Scaffold(
        body: PageView(
          physics: new NeverScrollableScrollPhysics(),
          controller: _tabController,
          onPageChanged: (newPage) {
            setState(() {
              print(newPage);
              this._currentIndex = newPage;
            });
          },
          children: _children,
        ), // new
        bottomNavigationBar: BottomNavigationBar(
          onTap: onTabTapped, // new
          currentIndex: _currentIndex, // new
          items: [
            new BottomNavigationBarItem(
              icon: Icon(Icons.school),
              title: Text('Home'),
            ),
            new BottomNavigationBarItem(
              icon: Icon(Icons.bookmark),
              title: Text('Bookmark'),
            ),
            new BottomNavigationBarItem(
                icon: Icon(Icons.person), title: Text('Profile'))
          ],
        ),
      ),
    );
  }

  void onTabTapped(int index) {
    this._tabController.animateToPage(index,
        duration: const Duration(milliseconds: 200), curve: Curves.easeInOut);
    setState(() {
      this._currentIndex = index;
    });
  }
}
