import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:learn_japanese/data/model/school_menu.dart';
import 'package:learn_japanese/ui/detail/detail_school_page.dart';
import 'package:learn_japanese/ui/detail/movie_detail_bloc_provider.dart';
import 'package:learn_japanese/ui/home/schoolpage/school_bloc.dart';
import 'package:learn_japanese/ui/home/schoolpage/school_state.dart';
import 'package:learn_japanese/utils/uidata.dart';

import 'school_event.dart';

class SchoolPage extends StatefulWidget {
  const SchoolPage({Key key}) : super(key: key);

  _SchoolState createState() => _SchoolState();
}

class _SchoolState extends State<SchoolPage>
    with AutomaticKeepAliveClientMixin<SchoolPage> {
  SchoolBloc _schoolBloc; 
  @override
  void initState() {
    // TODO: implement initState
    print("reloadSchoolPage");
    super.initState();
    _schoolBloc = SchoolBloc();
    _schoolBloc.dispatch(LoadSchoolEvent());
  }

  @override
  Widget build(BuildContext context) {
    var size = MediaQuery.of(context).size;
    /*24 is for notification bar on Android*/
    final double itemHeight = (size.height - kToolbarHeight - 128) / 3;
    final double itemWidth = (size.width / 2);
    return Scaffold(
        backgroundColor: UIData.colorBg(),
        body: BlocBuilder(
            bloc: _schoolBloc,
            builder: (BuildContext context, SchoolState state) {
              return Column(
                children: <Widget>[
                  appbarCustomize,
                  Expanded(
                      flex: 1,
                      child: Padding(
                        padding: const EdgeInsets.only(
                            left: 10.0, right: 10.0, top: 10.0),
                        child: bodySliverList(itemWidth, itemHeight, state),
                      ))
                ],
              );
            }));
  }

  @override
  bool get wantKeepAlive => true;

  var appbarCustomize = Container(
    width: double.infinity,
    height: 128.0,
    color: UIData.colorPrimaryLight(),
    child: Stack(
      children: <Widget>[
        Align(
            alignment: Alignment.center,
            child: Container(
              child: Text(
                UIData.title,
                textAlign: TextAlign.center,
                style: TextStyle(color: Colors.white, fontSize: 18.0),
              ),
            )),
        Positioned(
          child: Align(
            alignment: Alignment.bottomLeft,
            child: Container(
              padding: EdgeInsets.all(10),
              child: Text(
                UIData.title,
                style: TextStyle(color: Colors.white, fontSize: 16.0),
              ),
            ),
          ),
        )
      ],
    ),
  );

  Widget bodySliverList(
      double itemWidth, double itemHeight, SchoolState state) {
    if (state is UnSchoolState) {
      return Center(
        child: CircularProgressIndicator(),
      );
    }
    if (state is InSchoolState) {
      return bodyGrid(state.schoolMenu.data, itemWidth, itemHeight);
    }
    if (state is ErrorSchoolState) {
      return Center(
        child: GestureDetector(
          onTap: () {
            _schoolBloc.dispatch(LoadSchoolEvent());
          },
          child: Column( crossAxisAlignment:  CrossAxisAlignment.center,
          mainAxisAlignment:  MainAxisAlignment.center,
            children: <Widget>[
              Text(state.errorMessage),
              Text("Click to Reload"),
            ],
          ),
        ),
      );
    }
  }

  Widget bodyGrid(List<Data> menu, double itemWidth, double itemHeight) {
    return CustomScrollView(slivers: <Widget>[
      SliverGrid(
        gridDelegate: SliverGridDelegateWithFixedCrossAxisCount(
          crossAxisCount: 2,
          crossAxisSpacing: 10.0,
          mainAxisSpacing: 10.0,
          childAspectRatio: itemWidth / itemHeight,
        ),
        delegate: SliverChildBuilderDelegate(
          (BuildContext context, int index) {
            return menuStack(context, menu[index]);
          },
          childCount: menu.length,
          semanticIndexOffset: 2,
        ),
      ),
    ]);
  }

  //menuStack
  Widget menuStack(BuildContext context, Data menu) => InkWell(
        borderRadius: BorderRadius.circular(10.0),
        onTap: () => onMenuItemClick(menu),
        splashColor: UIData.colorAccent(),
        child: Card(
            shape: RoundedRectangleBorder(
              borderRadius: BorderRadius.circular(10.0),
            ),
            clipBehavior: Clip.antiAlias,
            elevation: 2.0,
            child: itemMenu(menu)),
      );

  Widget itemMenu(Data menu) {
    return Container(
      child: Center(
        child: GridTile(
          child: Column(
            mainAxisAlignment: MainAxisAlignment.center,
            crossAxisAlignment: CrossAxisAlignment.center,
            children: <Widget>[
              Image.network(
                menu.schoolImage,
                scale: 2,
              ),
              Padding(
                padding: const EdgeInsets.all(14.0),
                child: Text(
                  menu.schoolTitle,
                  textAlign: TextAlign.center,
                ),
              ),
            ],
          ),
        ),
      ),
      color: Color(0xffdcd8e1),
    );
  }

  onMenuItemClick(Data menu) {
    final page = DetailSchoolBlocProvider(
      child: DetailSchoolPage(
        menuData: menu,
      ),
    );
    Navigator.of(context).push(
      MaterialPageRoute(builder: (context) {
        return page;
      }),
    );
  }
}
