import 'package:equatable/equatable.dart';
import 'package:learn_japanese/data/model/school_menu.dart';
import 'package:meta/meta.dart';

@immutable
abstract class SchoolState extends Equatable {
  SchoolState([Iterable props]) : super(props);

  /// Copy object for use in action
  SchoolState getStateCopy();
}

/// UnInitialized
class UnSchoolState extends SchoolState {
  @override
  String toString() => 'UnSchoolState';

  @override
  SchoolState getStateCopy() {
    return UnSchoolState();
  }
}

/// Initialized
class InSchoolState extends SchoolState {
 final SchoolMenu schoolMenu;

  InSchoolState(this.schoolMenu);
  @override
  String toString() => 'InSchoolState';

  @override
  SchoolState getStateCopy() {
    return InSchoolState(schoolMenu);
  }
}

class ErrorSchoolState extends SchoolState {
  final String errorMessage;

  ErrorSchoolState(this.errorMessage);
  
  @override
  String toString() => 'ErrorSchoolState';

  @override
  SchoolState getStateCopy() {
    return ErrorSchoolState(this.errorMessage);
  }
}
