import 'dart:async';
import 'dart:convert';

import 'package:bloc/bloc.dart';
import 'package:learn_japanese/data/preferences.dart';
import 'package:learn_japanese/data/sharepref_name.dart';
import 'package:learn_japanese/data/user_repository.dart';
import 'package:learn_japanese/ui/home/schoolpage/school_event.dart';
import 'package:learn_japanese/ui/home/schoolpage/school_state.dart';

class SchoolBloc extends Bloc<SchoolEvent, SchoolState> {
  UserRepository _userRepository = UserRepository();
  // final _menuVM = SchoolViewModel();
  // final menuController = StreamController<SchoolMenu>();
  // Stream<SchoolMenu> get menuItems => menuController.stream;

  SchoolBloc() {
    // SchoolMenu menulist = _userRepository.getMenuItems();
    // menuController.add(menulist);
  }

  @override
  SchoolState get initialState => UnSchoolState();

  @override
  Stream<SchoolState> mapEventToState(SchoolEvent event) async* {
    try {
      yield initialState;
      var results = await _userRepository.getMenuItems();
      if (results.content.status == 200) {
        await Preference.load();
        var jsonString = json.encode(results.content.data);
        Preference.setString(SharePrefName.LIST_MENU, jsonString);
        yield InSchoolState(results.content);
      } else {
        yield ErrorSchoolState(results.content.message);
      }
    } catch (e, stackTrace) {
      print('${e.toString()}');
      yield ErrorSchoolState("Error get From Server!");
    }
  }
}
