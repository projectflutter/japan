import 'package:meta/meta.dart';


@immutable
abstract class SchoolEvent {
  // Future<SchoolState> applyAsync(
  //     {SchoolState currentState, SchoolBloc bloc});
}

class LoadSchoolEvent extends SchoolEvent {
  @override
  String toString() => 'LoadSchoolEvent';

  // @override
  // Future<SchoolState> applyAsync(
  //     {SchoolState currentState, SchoolBloc bloc}) async {
  //   try {
  //     await Future.delayed(new Duration(seconds: 2));
  //     this._schoolProvider.test();
  //     return new InSchoolState();
  //   } catch (_, stackTrace) {
  //     print('$_ $stackTrace');
  //     return new ErrorSchoolState(_?.toString());
  //   }
  // }
}
