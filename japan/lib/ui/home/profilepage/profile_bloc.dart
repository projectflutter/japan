import 'dart:async';
import 'package:bloc/bloc.dart';
import 'package:learn_japanese/data/preferences.dart';

import 'profile_event.dart';
import 'profile_state.dart';

class ProfileBloc extends Bloc<ProfileEvent, ProfileState> {
  static final ProfileBloc _profileBlocSingleton = new ProfileBloc._internal();
  factory ProfileBloc() {
    return _profileBlocSingleton;
  }
  ProfileBloc._internal();
  
  ProfileState get initialState => new UnProfileState();

  @override
  Stream<ProfileState> mapEventToState(
    ProfileEvent event,
  ) async* {
    if(event is LogoutEvent){
      await Preference.load();
      Preference.removeAll();
      yield LogoutSuccessState();
    }
  }
}
