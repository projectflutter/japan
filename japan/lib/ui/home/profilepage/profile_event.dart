import 'package:meta/meta.dart';


@immutable
abstract class ProfileEvent {}

class LogoutEvent extends ProfileEvent {
  @override
  String toString() => 'LogoutEvent';
}
