import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:learn_japanese/ui/home/profilepage/profile_bloc.dart';
import 'package:learn_japanese/utils/uidata.dart';

import 'profile_event.dart';
import 'profile_state.dart';

class ProfilePage extends StatefulWidget {
  ProfilePage({Key key, this.color}) : super(key: key);
  final Color color;

  _ProfileState createState() => _ProfileState();
}

class _ProfileState extends State<ProfilePage>
    with AutomaticKeepAliveClientMixin<ProfilePage> {
  ProfileBloc _profileBloc;
  @override
  void initState() {
    // TODO: implement initState
    super.initState();
    _profileBloc = ProfileBloc();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
        backgroundColor: Color(0xffF6F8FC),
        body: BlocListener(
            bloc: _profileBloc,
            listener: (context, ProfileState state) {
              if (state is LogoutSuccessState) {
                Navigator.of(context).pushNamedAndRemoveUntil(
                    '/login', (Route<dynamic> route) => false);
              }
            },
            child: Column(
              children: <Widget>[
                appbarCustomize,
                Expanded(flex: 1, child: bodyContent()),
              ],
            )));
  }

  @override
  bool get wantKeepAlive => true;

  var appbarCustomize = Container(
    width: double.infinity,
    height: 128.0,
    color: UIData.colorPrimaryLight(),
    child: Stack(
      children: <Widget>[
        Align(
            alignment: Alignment.center,
            child: Container(
              child: Text(
                UIData.title,
                textAlign: TextAlign.center,
                style: TextStyle(color: Colors.white, fontSize: 18.0),
              ),
            )),
        Positioned(
          child: Align(
            alignment: Alignment.bottomLeft,
            child: Container(
              padding: EdgeInsets.all(10),
              child: Text(
                UIData.title,
                style: TextStyle(color: Colors.white, fontSize: 16.0),
              ),
            ),
          ),
        )
      ],
    ),
  );

  Widget bodyContent() {
    return Column(
      mainAxisAlignment: MainAxisAlignment.center,
      crossAxisAlignment: CrossAxisAlignment.center,
      children: <Widget>[changePassButton(), signOutButton()],
    );
  }

  Widget changePassButton() {
    return Container(
      width: 180,
      child: OutlineButton(
        borderSide: BorderSide(color: UIData.colorAccent()),
        shape: RoundedRectangleBorder(
          borderRadius: BorderRadius.circular(24),
        ),
        onPressed: () {
          Navigator.of(context).pushNamed('/changepassword');
        },
        padding: EdgeInsets.all(12),
        child:
            Text('ĐỔI MẬT KHẨU', style: TextStyle(color: UIData.colorAccent())),
      ),
    );
  }

  Widget signOutButton() {
    return Container(
      width: 180,
      child: RaisedButton(
        shape: RoundedRectangleBorder(
          borderRadius: BorderRadius.circular(24),
        ),
        onPressed: () {
          _profileBloc.dispatch(LogoutEvent());
        },
        padding: EdgeInsets.all(12),
        color: UIData.colorAccent(),
        child: Text('ĐĂNG XUẤT', style: TextStyle(color: Colors.white)),
      ),
    );
  }
}
