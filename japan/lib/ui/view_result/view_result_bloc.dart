import 'dart:async';
import 'package:bloc/bloc.dart';
import 'package:learn_japanese/data/user_repository.dart';

import 'view_result_event.dart';
import 'view_result_state.dart';

class ViewResultBloc extends Bloc<ViewResultEvent, ViewResultState> {
  UserRepository _userRepository = UserRepository();

  ViewResultState get initialState => new UnViewResultState();

  @override
  Stream<ViewResultState> mapEventToState(
    ViewResultEvent event,
  ) async* {
    if (event is LoadViewResultEvent) {
      yield initialState;
      try {
        var user = await _userRepository.getResult(event.id);
        if (user.content.status == 200) {
          yield InViewResultState(user.content.data);
        } else {
         
          yield ErrorViewResultState(user.content.message);
        }
      } catch (e) {
        
        yield ErrorViewResultState("Error get result!");
      }
    }
  }
}
