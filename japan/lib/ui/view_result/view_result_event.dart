import 'dart:async';
import 'package:meta/meta.dart';

import 'view_result_state.dart';

@immutable
abstract class ViewResultEvent {}

class LoadViewResultEvent extends ViewResultEvent {
  final int id;

  LoadViewResultEvent(this.id);
  @override
  String toString() => 'LoadViewResultEvent';
}
