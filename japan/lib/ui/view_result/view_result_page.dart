import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:learn_japanese/ui/survey/answer_result_response.dart';
import 'package:learn_japanese/ui/view_result/question_item_result.dart';
import 'package:learn_japanese/ui/view_result/view_result_bloc.dart';
import 'package:learn_japanese/ui/view_result/view_result_event.dart';
import 'package:learn_japanese/ui/view_result/view_result_state.dart';
import 'package:learn_japanese/utils/uidata.dart';

class ViewResultPage extends StatefulWidget {
  final int id;
  final String titleName;
  final DataResult dataResult;
  ViewResultPage({Key key, this.id, this.titleName, this.dataResult})
      : super(key: key);

  _ViewResultState createState() =>
      _ViewResultState(id: id, titleName: titleName);
}

class _ViewResultState extends State<ViewResultPage> {
  final int id;
  final String titleName;
  _ViewResultState({this.id, this.titleName});
  @override
  void initState() {
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
        backgroundColor: UIData.colorBg(),
        body: NotificationListener<OverscrollIndicatorNotification>(
            onNotification: (overscroll) {
              overscroll.disallowGlow();
            },
            child: Column(
              children: <Widget>[
                _buildHeader(titleName),
                Expanded(child: _buildListResult())
              ],
            )));
  }

  Widget _buildHeader(String titleName) {
    return Padding(
      padding: const EdgeInsets.only(top: 24.0),
      child: Container(
        padding: EdgeInsets.all(10.0),
        child: Column(
          children: <Widget>[
            Row(
              children: <Widget>[
                Align(
                  alignment: Alignment.topLeft,
                  child: Container(
                    width: 42.0,
                    height: 42.0,
                    child: FloatingActionButton(
                      backgroundColor: Colors.white,
                      onPressed: () {
                        Navigator.of(context).pop();
                      },
                      child: Icon(
                        Icons.close,
                        color: UIData.colorPrimary(),
                      ),
                    ),
                  ),
                ),
                Padding(
                  padding: const EdgeInsets.only(left: 15.0),
                  child: Text(
                    "Đáp án $titleName",
                    style: TextStyle(color: UIData.colorPrimaryLight()),
                  ),
                )
              ],
            ),
          ],
        ),
      ),
    );
  }

  Widget _buildListResult() {
    print(widget.dataResult.total);
    if (widget.dataResult.data != null) {
      if (widget.dataResult.data.length == 0) {
        return Container(
            child: Center(
          child: Text("Empty list data!"),
        ));
      } else {
        return ListView.builder(
            padding: const EdgeInsets.all(8.0),
            itemCount: widget.dataResult.data.length,
            itemBuilder: (BuildContext context, int index) {
              return QuestionResultItem(
                dataList: widget.dataResult.data[index],
                index: index,
              );
            });
      }
    } else {
      return Container(
          child: Center(
        child: Text("Error data!"),
      ));
    }
  }
}
