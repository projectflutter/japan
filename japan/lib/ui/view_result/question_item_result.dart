import 'package:flutter/material.dart';
import 'package:learn_japanese/data/model/result_response.dart';
import 'package:learn_japanese/ui/survey/answer_result_response.dart';
import 'package:learn_japanese/utils/uidata.dart';
import 'package:flutter_html_view/flutter_html_view.dart';
import 'radio_model_result.dart';

class QuestionResultItem extends StatefulWidget {
  final DataViewResult dataList;
  final int index;
  QuestionResultItem({
    Key key,
    this.dataList,
    this.index,
  }) : super(key: key);

  _QuestionResultItemState createState() =>
      _QuestionResultItemState(dataList: dataList, index: index);
}

class _QuestionResultItemState extends State<QuestionResultItem> {
  List<Widget> listWidget = new List<Widget>();
  final DataViewResult dataList;
  bool isExpand = false;
  final int index;
  _QuestionResultItemState({this.dataList, this.index});
  @override
  void initState() {
    super.initState();
    for (var i = 0; i < dataList.answers.length; i++) {
      var item = dataList.answers[i];
      listWidget.add(AnswerItem(RadioModelResult(i, item.id, dataList.answerId,
          item.isCorrected, "${item.answerSort}", item.answerTitle)));
    }
  }

  @override
  Widget build(BuildContext context) {
    return Padding(
      padding: const EdgeInsets.all(8.0),
      child: Column(
        mainAxisSize: MainAxisSize.min,
        children: <Widget>[
          Align(
              alignment: Alignment.topLeft,
              child: Text('${index + 1}. ${dataList.questionTitle}')),
          ListView(
            physics: NeverScrollableScrollPhysics(),
            shrinkWrap: true,
            children: listWidget,
          ),
          InkWell(
            onTap: () {
              setState(() {
                isExpand = !isExpand;
              });
            },
            child: Align(
              alignment: Alignment.topLeft,
              child: Row(
                children: <Widget>[
                  Text(
                    "Xem giải thích",
                    style: TextStyle(color: UIData.colorGreen()),
                  ),
                  handlerMoreLess(),
                ],
              ),
            ),
          ),
          handlerShowhideText()
        ],
      ),
    );
  }

  Widget handlerMoreLess() {
    print(isExpand);
    return isExpand
        ? Icon(
            Icons.expand_less,
            color: UIData.colorGreen(),
          )
        : Icon(
            Icons.expand_more,
            color: UIData.colorGreen(),
          );
  }

  Widget handlerShowhideText() {
    if (isExpand) {
      return Flexible(
        child: Container(
            decoration: BoxDecoration(
                borderRadius: BorderRadius.circular(10.0),
                border: Border.all(color: Colors.green, width: 1.0)),
            width: double.infinity,
            height: 100,
            child: HtmlView(
              data: dataList.questionExplain,
            )),
      );
    } else {
      return Container();
    }
  }
}

class AnswerItem extends StatelessWidget {
  final RadioModelResult _item;
  AnswerItem(this._item);
  @override
  Widget build(BuildContext context) {
    return new Container(
      decoration: new BoxDecoration(
          color: checkColor(_item),
          borderRadius: new BorderRadius.all(const Radius.circular(15.0))),
      margin: new EdgeInsets.only(bottom: 10.0, top: 10.0),
      child: new Row(
        mainAxisSize: MainAxisSize.max,
        children: <Widget>[
          new Container(
            margin: EdgeInsets.all(3.0),
            height: 32.0,
            width: 32.0,
            child: new Center(
              child: new Text('${_item.index+1}',
                  style: new TextStyle(
                      color: Colors.black,
                      //fontWeight: FontWeight.bold,
                      fontSize: 18.0)),
            ),
            decoration:
                new BoxDecoration(shape: BoxShape.circle, color: Colors.white),
          ),
          Expanded(
            child: new Container(
              margin: new EdgeInsets.only(left: 10.0),
              child: new Text(
                _item.text,
                textAlign: TextAlign.start,
              ),
            ),
          ),
          Padding(
            padding: const EdgeInsets.all(8.0),
            child:
                Align(alignment: Alignment.topRight, child: handlerIcon(_item)),
          )
        ],
      ),
    );
  }

  Color checkColor(RadioModelResult item) {
    if (item.isCorrect == 1) {
      return UIData.colorCorrect();
    }
    if (item.idUserChoose == item.idAnswer) {
      return UIData.colorWrong();
    }

    return Colors.white;
  }

  Widget handlerIcon(RadioModelResult item) {
    if (item.isCorrect == 1) {
      return Icon(
        Icons.done,
        color: UIData.colorGreen(),
      );
    }
    if (item.idUserChoose == item.idAnswer) {
      return Icon(
        Icons.close,
        color: Colors.red,
      );
    }
    return Container();
  }
}
