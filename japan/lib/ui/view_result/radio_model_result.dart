class RadioModelResult {
  final int index;
  int idAnswer;
  int idUserChoose;
  int isCorrect;
  final String buttonText;
  final String text;

  RadioModelResult(
    this.index,
    this.idAnswer,
    this.idUserChoose,
    this.isCorrect,
    this.buttonText,
    this.text, 
  );
}
