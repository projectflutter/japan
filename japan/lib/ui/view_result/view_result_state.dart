import 'package:equatable/equatable.dart';
import 'package:learn_japanese/data/model/result_response.dart';
import 'package:meta/meta.dart';

@immutable
abstract class ViewResultState extends Equatable {
  ViewResultState([Iterable props]) : super(props);

  /// Copy object for use in action
  ViewResultState getStateCopy();
}

/// UnInitialized
class UnViewResultState extends ViewResultState {
  @override
  String toString() => 'UnViewResultState';

  @override
  ViewResultState getStateCopy() {
    return UnViewResultState();
  }
}

/// Initialized
class InViewResultState extends ViewResultState {
 final List<DataQuestionResult> dataList;

  InViewResultState(this.dataList);
  @override
  String toString() => 'InViewResultState';

  @override
  ViewResultState getStateCopy() {
    return InViewResultState(dataList);
  }
}

class ErrorViewResultState extends ViewResultState {
  final String errorMessage;

  ErrorViewResultState(this.errorMessage);

  @override
  String toString() => 'ErrorViewResultState';

  @override
  ViewResultState getStateCopy() {
    return ErrorViewResultState(this.errorMessage);
  }
}
