class RadioModel {
  final int index;
  final int idAnswer;
  final int idQuestion;
  bool isSelected;
  final String buttonText;
  final String text;

  RadioModel(this.isSelected, this.buttonText, this.text, this.idAnswer,
      this.idQuestion,this.index);
}
