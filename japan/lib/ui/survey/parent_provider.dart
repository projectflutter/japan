import 'package:flutter/material.dart';

class ParentProvider extends InheritedWidget {
  final Function nextpage;
  final Function hideDialog;
  ParentProvider({
    this.nextpage,
    Key key,
    this.child,
    this.hideDialog,
  }) : super(key: key, child: child);

  final Widget child;

  static ParentProvider of(BuildContext context) {
    return (context.inheritFromWidgetOfExactType(ParentProvider)
        as ParentProvider);
  }

  @override
  bool updateShouldNotify(ParentProvider oldWidget) {
    return true;
  }
}
