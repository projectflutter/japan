import 'package:flutter/material.dart';
import 'package:learn_japanese/data/model/question_response.dart';
import 'package:learn_japanese/utils/uidata.dart';

import 'parent_provider.dart';
import 'radio_model.dart';

class ItemQuestionPage extends StatefulWidget {
  final DataQuestion dataQuestion;
  final int index;
  ItemQuestionPage({Key key, this.dataQuestion, this.index}) : super(key: key);

  _ItemQuestionState createState() =>
      _ItemQuestionState(dataQuestion: dataQuestion, index: index);
}

class _ItemQuestionState extends State<ItemQuestionPage>
    with AutomaticKeepAliveClientMixin<ItemQuestionPage> {
  int radioValue = -1;
  List<RadioModel> listAnswer = new List<RadioModel>();
  final DataQuestion dataQuestion;
  final int index;
  _ItemQuestionState({this.dataQuestion, this.index});
  @override
  void initState() {
    super.initState();
    for (var i = 0; i < dataQuestion.answers.length; i++) {
      var item = dataQuestion.answers[i];
      listAnswer.add(new RadioModel(false, '${item.answerSort}',
          '${item.answerTitle}', item.id, item.questionId, i));
    }
  
  }

  @override
  Widget build(BuildContext context) {
    void handleRadioValueChanged(int value) {
      print(value);
      setState(() {
        radioValue = value;
      });
    }

    return ConstrainedBox(
      constraints: const BoxConstraints.expand(),
      child: Container(
        padding: EdgeInsets.all(10.0),
        child: Column(
          crossAxisAlignment: CrossAxisAlignment.start,
          children: <Widget>[
            Text('${index + 1}' + '. ' + '${this.dataQuestion.questionTitle}'),
            Expanded(child: _buildContentAnswer())
          ],
        ),
      ),
    );
  }

  Widget _buildContentAnswer() {
    return ListView.builder(
        itemCount: listAnswer.length,
        itemBuilder: (BuildContext context, int index) {
          return new InkWell(
            enableFeedback: false,
            highlightColor: Colors.transparent,
            splashColor: Colors.transparent,
            onTap: () {
              setState(() {
                listAnswer.forEach((element) => element.isSelected = false);
                listAnswer[index].isSelected = true;
              });
              ParentProvider.of(context).nextpage(listAnswer[index]);
            },
            child: new RadioItem(listAnswer[index]),
          );
        });
  }

  @override
  // TODO: implement wantKeepAlive
  bool get wantKeepAlive => true;
}

class RadioItem extends StatelessWidget {
  final RadioModel _item;
  RadioItem(this._item);
  @override
  Widget build(BuildContext context) {
    return new Container(
      decoration: new BoxDecoration(
          color: _item.isSelected ? UIData.colorSelected() : Colors.white,
          borderRadius: new BorderRadius.all(const Radius.circular(15.0))),
      margin: new EdgeInsets.only(bottom: 10.0, top: 10.0),
      child: new Row(
        mainAxisSize: MainAxisSize.max,
        children: <Widget>[
          new Container(
            margin: EdgeInsets.all(3.0),
            height: 32.0,
            width: 32.0,
            child: new Center(
              child: new Text('${_item.index+1}',
                  style: new TextStyle(
                      color: _item.isSelected ? Colors.white : Colors.black,
                      //fontWeight: FontWeight.bold,
                      fontSize: 18.0)),
            ),
            decoration: new BoxDecoration(
              shape: BoxShape.circle,
              color:
                  _item.isSelected ? UIData.colorPrimary() : Colors.transparent,
            ),
          ),
          new Container(
            margin: new EdgeInsets.only(left: 10.0),
            child: new Text(_item.text),
          )
        ],
      ),
    );
  }
}
