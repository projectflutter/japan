class AnswerResult{
  int questionId;
  int answerId;
  AnswerResult(this.questionId,this.answerId);
   Map<String, dynamic> toJson() =>
    {
      'q': questionId,
      'a': answerId,
    };
}