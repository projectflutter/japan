import 'package:equatable/equatable.dart';
import 'package:learn_japanese/data/model/question_response.dart';
import 'package:learn_japanese/ui/survey/answer_result_response.dart';
import 'package:meta/meta.dart';

@immutable
abstract class SurveyState extends Equatable {
  SurveyState([Iterable props]) : super(props);
  SurveyState getStateCopy();
}

/// UnInitialized
class UnSurveyState extends SurveyState {
  @override
  String toString() => 'UnSurveyState';

  @override
  SurveyState getStateCopy() {
    return UnSurveyState();
  }
}

//event get list question
class ShowLoadingState extends SurveyState {
  @override
  String toString() => 'ShowLoadingState';

  @override
  SurveyState getStateCopy() {
    return ShowLoadingState();
  }
}

class GetListQuestionSuccessState extends SurveyState {
  final List<DataQuestion> listdata;

  GetListQuestionSuccessState(this.listdata);
  @override
  String toString() => 'GetListQuestionSuccessState';

  @override
  SurveyState getStateCopy() {
    return GetListQuestionSuccessState(listdata);
  }
}

class EmptyListQuestionState extends SurveyState {
  @override
  String toString() => 'EmptyListQuestionState';

  @override
  SurveyState getStateCopy() {
    return EmptyListQuestionState();
  }
}

class ErrorListQuestionState extends SurveyState {
  final String errorMessage;

  ErrorListQuestionState(this.errorMessage);

  @override
  String toString() => 'ErrorListQuestionSurveyState';

  @override
  SurveyState getStateCopy() {
    return ErrorListQuestionState(this.errorMessage);
  }
}

//event post question
class ShowLoadingDialogState extends SurveyState {
  @override
  String toString() => 'ShowLoadingDialogState';

  @override
  SurveyState getStateCopy() {
    return ShowLoadingDialogState();
  }
}
class PostResultSuccessState extends SurveyState {
  final DataResult dataResult;

  PostResultSuccessState(this.dataResult);
  @override
  String toString() => 'PostResultSuccessState';

  @override
  SurveyState getStateCopy() {
    return PostResultSuccessState(dataResult);
  }
}
class PostResultFailState extends SurveyState {
  final String error;

  PostResultFailState(this.error);
  @override
  String toString() => 'PostResultFailState';

  @override
  SurveyState getStateCopy() {
    return PostResultFailState(error);
  }
}
class BookmarkSuccessState extends SurveyState {
  final String message;

  BookmarkSuccessState(this.message);
  @override
  String toString() => 'BookmarkSuccessState';

  @override
  SurveyState getStateCopy() {
    return PostResultFailState(message);
  }
}
