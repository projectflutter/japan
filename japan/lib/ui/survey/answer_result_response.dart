class AnswerResultResponse {
  int status;
  String message;
  DataResult data;

  AnswerResultResponse({this.status, this.message, this.data});

  AnswerResultResponse.fromJson(Map<String, dynamic> json) {
    status = json['status'];
    message = json['message'];
    data = json['data'] != null ? new DataResult.fromJson(json['data']) : null;
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['status'] = this.status;
    data['message'] = this.message;
    if (this.data != null) {
      data['data'] = this.data.toJson();
    }
    return data;
  }
}

class DataResult {
  int correct;
  int total;
  List<DataViewResult> data;

  DataResult({this.correct, this.total, this.data});

  DataResult.fromJson(Map<String, dynamic> json) {
    correct = json['correct'];
    total = json['total'];
    if (json['data'] != null) {
      data = new List<DataViewResult>();
      json['data'].forEach((v) {
        data.add(new DataViewResult.fromJson(v));
      });
    }
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['correct'] = this.correct;
    data['total'] = this.total;
    if (this.data != null) {
      data['data'] = this.data.map((v) => v.toJson()).toList();
    }
    return data;
  }
}

class DataViewResult {
  int id;
  int schoolId;
  int questionTypeId;
  String questionTitle;
  String questionExplain;
  int questionSort;
  String createdAt;
  int answerId;
  List<Answers> answers;

  DataViewResult(
      {this.id,
      this.schoolId,
      this.questionTypeId,
      this.questionTitle,
      this.questionExplain,
      this.questionSort,
      this.createdAt,
      this.answerId,
      this.answers});

  DataViewResult.fromJson(Map<String, dynamic> json) {
    id = json['id'];
    schoolId = json['school_id'];
    questionTypeId = json['question_type_id'];
    questionTitle = json['question_title'];
    questionExplain = json['question_explain'];
    questionSort = json['question_sort'];
    createdAt = json['created_at'];
    answerId = json['answer_id'];
    if (json['answers'] != null) {
      answers = new List<Answers>();
      json['answers'].forEach((v) {
        answers.add(new Answers.fromJson(v));
      });
    }
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['id'] = this.id;
    data['school_id'] = this.schoolId;
    data['question_type_id'] = this.questionTypeId;
    data['question_title'] = this.questionTitle;
    data['question_explain'] = this.questionExplain;
    data['question_sort'] = this.questionSort;
    data['created_at'] = this.createdAt;
    data['answer_id'] = this.answerId;
    if (this.answers != null) {
      data['answers'] = this.answers.map((v) => v.toJson()).toList();
    }
    return data;
  }
}

class Answers {
  int id;
  int questionId;
  String answerTitle;
  int isCorrected;
  int answerSort;

  Answers(
      {this.id,
      this.questionId,
      this.answerTitle,
      this.isCorrected,
      this.answerSort});

  Answers.fromJson(Map<String, dynamic> json) {
    id = json['id'];
    questionId = json['question_id'];
    answerTitle = json['answer_title'];
    isCorrected = json['is_corrected'];
    answerSort = json['answer_sort'];
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['id'] = this.id;
    data['question_id'] = this.questionId;
    data['answer_title'] = this.answerTitle;
    data['is_corrected'] = this.isCorrected;
    data['answer_sort'] = this.answerSort;
    return data;
  }
}
