import 'package:flutter/material.dart';
import 'package:learn_japanese/ui/survey/answer_result_response.dart';
import 'package:learn_japanese/ui/view_result/view_result_page.dart';
import 'package:learn_japanese/utils/uidata.dart';

class ResultAnswerScreen extends StatefulWidget {
  final DataResult dataResult;
  final int id;
  final String titleName;
  ResultAnswerScreen({this.dataResult, this.id, this.titleName});

  // final ResultAnswerBloc _resultAnswerBloc;

  @override
  ResultAnswerScreenState createState() {
    return new ResultAnswerScreenState(
        dataResult: dataResult, id: id, titleName: titleName);
  }
}

class ResultAnswerScreenState extends State<ResultAnswerScreen> {
  final DataResult dataResult;
  final int id;
  final String titleName;
  ResultAnswerScreenState({this.dataResult, this.id, this.titleName});

  // final ResultAnswerBloc _resultAnswerBloc;
  // ResultAnswerScreenState(this._resultAnswerBloc);

  @override
  void initState() {
    super.initState();
    // this._resultAnswerBloc.dispatch(LoadResultAnswerEvent());
  }

  @override
  void dispose() {
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: UIData.colorBg(),
      body: Container(
        child: Column(
          mainAxisAlignment: MainAxisAlignment.center,
          crossAxisAlignment: CrossAxisAlignment.center,
          children: <Widget>[
            Expanded(
                          child: SizedBox(
                height: 64,
              ),
            ),
            Expanded(
                          child: new ClipRRect(
                borderRadius: new BorderRadius.circular(8.0),
                child: Image.asset(
                  UIData.imageResult,
                  height: 200.0,
                  width: 150.0,
                ),
              ),
            ),
            Icon(
              Icons.done,
              color: Color(0xffFFBA08),
              size: 64,
            ),
            Text(
                "Đã hoàn thành ${dataResult.correct}/${dataResult.total} câu hỏi!"),
            Expanded(
              child: SizedBox(
                height: 128,
              ),
            ),
            Expanded(
                          child: Align(
                alignment: Alignment.bottomCenter,
                child: Column(
                  children: <Widget>[viewResultButton(), continueButton()],
                ),
              ),
            )
          ],
        ),
      ),
    );
  }

  Widget viewResultButton() {
    return Container(
      width: 180,
      child: OutlineButton(
        borderSide: BorderSide(color: UIData.colorAccent()),
        shape: RoundedRectangleBorder(
          borderRadius: BorderRadius.circular(24),
        ),
        onPressed: () {
          Navigator.of(context).push(
            MaterialPageRoute(builder: (context) {
              return ViewResultPage(id: id, titleName: titleName,dataResult: dataResult,);
            }),
          );
          //  Navigator.of(context).pushNamed(
          //           '/changepassword');
        },
        padding: EdgeInsets.all(12),
        child:
            Text('XEM ĐÁP ÁN', style: TextStyle(color: UIData.colorAccent())),
      ),
    );
  }

  Widget continueButton() {
    return Container(
      width: 180,
      child: RaisedButton(
        shape: RoundedRectangleBorder(
          borderRadius: BorderRadius.circular(24),
        ),
        onPressed: () {
          Navigator.of(context).pushNamedAndRemoveUntil(
              '/home', (Route<dynamic> route) => false);
        },
        padding: EdgeInsets.all(12),
        color: UIData.colorAccent(),
        child: Text('TIẾP TỤC', style: TextStyle(color: Colors.white)),
      ),
    );
  }
}
