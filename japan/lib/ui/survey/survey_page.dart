import 'package:flutter/cupertino.dart';
import 'package:flutter/foundation.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:learn_japanese/data/model/question_response.dart';
import 'package:learn_japanese/ui/survey/item_question_page.dart';
import 'package:learn_japanese/ui/survey/radio_model.dart';
import 'package:learn_japanese/ui/survey/survey_bloc.dart';
import 'package:learn_japanese/ui/survey/survey_event.dart';
import 'package:learn_japanese/ui/widget/flutter_animation_progress_bar.dart';
import 'package:learn_japanese/utils/uidata.dart';

import 'answer_result.dart';
import 'parent_provider.dart';
import 'result_answer_screen.dart';
import 'survey_state.dart';

class SurveyPage extends StatefulWidget {
  final int catelogyId;
  final String titleName;
  SurveyPage({Key key, this.catelogyId, this.titleName}) : super(key: key);

  _SurveyPageState createState() => _SurveyPageState(catelogyId, titleName);
}

class _SurveyPageState extends State<SurveyPage> {
  int catelogyId;
  int _currentIndex = 1;
  int totalPage = 1;
  bool isShowButtonResult = false;
  bool initListResult = false;
  bool isShowLoading = false;
  bool isShowDialog = false;
  PageController _tabController;
  SurveyBloc _surveyBloc;
  bool _locked;
  List<AnswerResult> listAnswerResult = [];
  List<DataQuestion> listdataState;
  String titleName;

  _SurveyPageState(int catelogyId, String titleName)
      : this.catelogyId = catelogyId,
        this.titleName = titleName;
  @override
  void initState() {
    _tabController = new PageController(initialPage: _currentIndex - 1)
      ..addListener(scrollListener);
    _locked = false;

    super.initState();
    _surveyBloc = SurveyBloc();
    _surveyBloc.dispatch(GetListQuestionEvent(id: catelogyId));
  }

  @override
  Widget build(BuildContext context) {
    return ParentProvider(
      nextpage: functionHandleNextPage,
      child: WillPopScope(
        onWillPop: () {
          Future.value(false);
        },
        child: Scaffold(
            backgroundColor: UIData.colorBg(),
            body: BlocBuilder<SurveyEvent, SurveyState>(
              bloc: _surveyBloc,
              builder: (context, state) {
                return NotificationListener<OverscrollIndicatorNotification>(
                    onNotification: (overscroll) {
                      overscroll.disallowGlow();
                    },
                    child: contentPage(context, state));
              },
            )),
      ),
    );
  }

  Widget contentPage(BuildContext context, SurveyState state) {
    return Container(
      padding: EdgeInsets.only(
        top: 24.0,
      ),
      child: _buildBody(state),
    );
  }

  void scrollListener() {
    // print(_tabController.offset);
    double _currentOffset = _tabController.offset;
    double _width = MediaQuery.of(context).size.width;
    if (_currentOffset > _tabController.page * _width) {}
  }

  void onPageChange(int newPage) {
    // print('$newPage newpage');
    for (var i = 0; i < listAnswerResult.length; i++) {
      if (newPage > 0 && listAnswerResult[newPage - 1].answerId == -1) {
        print("back");
        this._tabController.animateToPage(newPage - 1,
            duration: const Duration(milliseconds: 300),
            curve: Curves.easeInOut);

        return;
      }
    }
    setState(() {
      this._currentIndex = newPage + 1;
      isShowDialog = true;
      print('$_currentIndex currentindex');
    });
  }

  functionHandleNextPage(RadioModel modelRadio) {
    int indexPage = _currentIndex;
    int countAnswer = 0;
    if (indexPage <= totalPage) {
      for (var item in listAnswerResult) {
        if (item.questionId == modelRadio.idQuestion) {
          item.answerId = modelRadio.idAnswer;
        }
      }
      for (var item in listAnswerResult) {
        // print('${item.questionId}' + '/' + '${item.answerId}');
        if (item.answerId != -1) {
          countAnswer = countAnswer + 1;
        }
      }
      // print(countAnswer);
      // print("end");
      if (countAnswer == listAnswerResult.length) {
        setState(() {
          isShowButtonResult = true;
        });
      } else {
        isShowButtonResult = false;
      }
      if (_currentIndex < totalPage) {
        setState(() {
          this._currentIndex = indexPage + 1;
          isShowDialog = true;
        });
        // print('$_currentIndex auto');
        this._tabController.animateToPage(_currentIndex - 1,
            duration: const Duration(milliseconds: 300),
            curve: Curves.easeInOut); //nextpage
      } else {
        print("nextpage");
        isShowDialog = true;
      }
    }
  }

  Widget _buildHeader(BuildContext context) {
    return Container(
      padding: EdgeInsets.all(10.0),
      child: Column(
        children: <Widget>[
          Row(
            children: <Widget>[
              Align(
                alignment: Alignment.topLeft,
                child: Container(
                  width: 42.0,
                  height: 42.0,
                  child: FloatingActionButton(
                    backgroundColor: Colors.white,
                    onPressed: () {
                      _onPressBack(context);
                    },
                    child: Icon(
                      Icons.close,
                      color: UIData.colorPrimary(),
                    ),
                  ),
                ),
              ),
            ],
          ),
        ],
      ),
    );
  }

  Widget _buildHeader1(BuildContext context) {
    int indexRealPage = _currentIndex;
    double currentProgress = ((indexRealPage * 100) / totalPage);
    return Container(
      padding: EdgeInsets.all(10.0),
      child: Column(
        children: <Widget>[
          Row(
            children: <Widget>[
              Align(
                alignment: Alignment.topLeft,
                child: Container(
                  width: 42.0,
                  height: 42.0,
                  child: FloatingActionButton(
                    backgroundColor: Colors.white,
                    onPressed: () {
                      _onPressBack(context);
                    },
                    child: Icon(
                      Icons.close,
                      color: UIData.colorPrimary(),
                    ),
                  ),
                ),
              ),
              Expanded(
                child: SizedBox(
                  height: 10.0,
                  child: Padding(
                    padding: const EdgeInsets.only(left: 10.0, right: 10.0),
                    child: FAProgressBar(
                      progressColor: Colors.yellow,
                      backgroundColor: Colors.yellow[100],
                      currentValue: currentProgress.toInt(),
                      borderRadius: 21,
                    ),
                  ),
                ),
              ),
              Align(
                alignment: Alignment.topRight,
                child: Text('$indexRealPage/$totalPage'),
              ),
            ],
          ),
          Stack(
            children: <Widget>[
              Align(
                  alignment: Alignment.center,
                  child: Container(
                    padding: const EdgeInsets.all(10.0),
                    child: Text(
                      "Vuốt sang phải để xem lại các câu hỏi",
                      style: TextStyle(
                        color: UIData.colorAccent(),
                      ),
                    ),
                  )),
              InkWell(
                onTap: () {
                  _onBookmarkQuestionClick();
                },
                child: Align(
                  alignment: Alignment.topRight,
                  child: Padding(
                    padding: const EdgeInsets.all(6.0),
                    child: Icon(
                      Icons.bookmark_border,
                      color: UIData.colorAccent(),
                    ),
                  ),
                ),
              )
            ],
          ),
        ],
      ),
    );
  }

  Widget _buildBody(SurveyState state) {
    if (state is ShowLoadingState) {
      return Column(
        children: <Widget>[
          _buildHeader1(context),
          Padding(
            padding: const EdgeInsets.only(top: 128.0),
            child: Center(
              child: CircularProgressIndicator(),
            ),
          ),
        ],
      );
    }
    if (state is GetListQuestionSuccessState) {
      listdataState = state.listdata;
      if (initListResult == false) {
        initListResult = true;
        intiAnswerResult(state.listdata);
      } else {}
      final List<Widget> _children = genarateList(state.listdata);
      return bodyAnswer(_children);
    }
    if (state is EmptyListQuestionState) {
      return Column(
        children: <Widget>[
          _buildHeader1(context),
          Center(
            child: Column(
              crossAxisAlignment: CrossAxisAlignment.center,
              mainAxisAlignment: MainAxisAlignment.center,
              children: <Widget>[
                Text("Empty list question!"),
              ],
            ),
          ),
        ],
      );
    }
    if (state is ErrorListQuestionState) {
      return Column(
        children: <Widget>[
          _buildHeader1(context),
          Center(
              child: GestureDetector(
            onTap: () {
              _surveyBloc.dispatch(GetListQuestionEvent(id: catelogyId));
            },
            child: Column(
              crossAxisAlignment: CrossAxisAlignment.center,
              mainAxisAlignment: MainAxisAlignment.center,
              children: <Widget>[
                Text(state.errorMessage),
                Text("Click to Reload"),
              ],
            ),
          )),
        ],
      );
    }
    if (state is ShowLoadingDialogState) {
      WidgetsBinding.instance
          .addPostFrameCallback((_) => yourFunction(context));
      final List<Widget> _children = genarateList(listdataState);

      return bodyAnswer(_children);
    }
    if (state is PostResultFailState) {
      WidgetsBinding.instance
          .addPostFrameCallback((_) => _dissmisdialog(context));
      print(isShowDialog);
      if (isShowDialog == false) {
        WidgetsBinding.instance
            .addPostFrameCallback((_) => _showdialog(state.error, context));
      }
      final List<Widget> _children = genarateList(listdataState);
      return bodyAnswer(_children);
    }
    if (state is BookmarkSuccessState) {
      WidgetsBinding.instance
          .addPostFrameCallback((_) => _dissmisdialog(context));
      if (isShowDialog == false) {
        WidgetsBinding.instance
            .addPostFrameCallback((_) => _showdialog(state.message, context));
      }
      final List<Widget> _children = genarateList(listdataState);
      return bodyAnswer(_children);
    }
    if (state is PostResultSuccessState) {
      // if (Navigator.canPop(context)) {
      WidgetsBinding.instance
          .addPostFrameCallback((_) => _dissmisdialog(context));

      return ResultAnswerScreen(
          dataResult: state.dataResult, id: catelogyId, titleName: titleName,);
    }
  }

  Widget _buildFooter() {
    return isShowButtonResult
        ? Container(
            padding: EdgeInsets.symmetric(vertical: 16.0),
            width: 180,
            child: RaisedButton(
              shape: RoundedRectangleBorder(
                borderRadius: BorderRadius.circular(24),
              ),
              onPressed: () {
                _submitAnswerResult();
              },
              padding: EdgeInsets.all(12),
              color: UIData.colorButtonResults(),
              child: Text('Xem kết quả', style: TextStyle(color: Colors.white)),
            ),
          )
        : Container();
  }

  void _onPressBack(BuildContext context) {
    Navigator.of(context).pop();
  }

  List<Widget> genarateList(List<DataQuestion> listdata) {
    totalPage = listdata.length;
    List<Widget> _children = [];
    for (var i = 0; i < totalPage; i++) {
       _children.add(ItemQuestionPage(
        dataQuestion: listdata[i],index: i,
      ));
    }
    for (var item in listdata) {
      _children.add(ItemQuestionPage(
        dataQuestion: item,
      ));
    }
    return _children;
  }

  void intiAnswerResult(List<DataQuestion> listdata) {
    for (var item in listdata) {
      listAnswerResult.add(AnswerResult(item.id, -1));
    }
  }

  @override
  void dispose() {
    super.dispose();
    _tabController.dispose();
    _surveyBloc.dispose();
  }

  void _submitAnswerResult() {
    _surveyBloc.dispatch(PostListAnswerEvent(listAnswerResult));
  }

  yourFunction(BuildContext context) {
    isShowLoading = true;
    showDialog(
        context: context,
        barrierDismissible: false,
        builder: (context) => WillPopScope(
            onWillPop: () {
              return Future.value(false);
            },
            child: Center(
              child: CircularProgressIndicator(
              ),
            )));
  }

  Widget bodyAnswer(List<Widget> children) {
    return Column(
      children: <Widget>[
        _buildHeader1(context),
        Expanded(
          child: PageView(
            controller: _tabController,
            onPageChanged: (newPage) {
              onPageChange(newPage);
            },
            children: children,
          ),
        ),
        _buildFooter()
      ],
    );
  }

  _showdialog(String content, BuildContext context) {
    print("Show");
    isShowDialog = true;
    showDialog(
        barrierDismissible: true,
        context: context,
        builder: (context) => AlertDialog(
              title: Text("Thông báo"),
              content: Text(content),
              actions: <Widget>[
                FlatButton(
                  child: Text("ok"),
                  onPressed: () {
                    // setState(() {
                    isShowDialog = false;
                    // });
                    Navigator.of(context).pop();
                  },
                )
              ],
            ));
  }

  _dissmisdialog(BuildContext context) {
    if (isShowLoading == true) {
      isShowLoading = false;
      Navigator.pop(context);
    }
  }

  void _onBookmarkQuestionClick() {
    print("click bookmark");
    isShowDialog = false;
    int questionId = listdataState[_currentIndex - 1].id;
    _surveyBloc.dispatch(BookmarQuestionEvent(questionId));
  }
}
