import 'package:meta/meta.dart';

import 'answer_result.dart';

@immutable
abstract class SurveyEvent {}

class GetListQuestionEvent extends SurveyEvent {
  final int id;

  GetListQuestionEvent({this.id});
  @override
  String toString() => 'GetListQuestionEvent';
}

class PostListAnswerEvent extends SurveyEvent {
  final List<AnswerResult> listResult;

  PostListAnswerEvent(this.listResult);
  @override
  String toString() => 'PostListAnswerEvent';
}

class BookmarQuestionEvent extends SurveyEvent {
  final int questionId;

  BookmarQuestionEvent(this.questionId);
  @override
  String toString() => 'BookmarQuestionEvent';
}
