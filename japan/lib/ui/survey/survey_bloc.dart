import 'dart:async';
import 'package:bloc/bloc.dart';
import 'package:learn_japanese/data/user_repository.dart';
import 'package:learn_japanese/ui/survey/answer_result.dart';

import 'survey_event.dart';
import 'survey_state.dart';

class SurveyBloc extends Bloc<SurveyEvent, SurveyState> {
  UserRepository _userRepository = UserRepository();

  SurveyState get initialState => new ShowLoadingState();

  @override
  Stream<SurveyState> mapEventToState(
    SurveyEvent event,
  ) async* {
    if (event is GetListQuestionEvent) {
      yield* _mapListQuestion(event.id);
    }
    if (event is PostListAnswerEvent) {
      yield* _mapListAnswerResult(event.listResult);
    }
    if (event is BookmarQuestionEvent) {
      yield* _mapBookmarQuestion(event.questionId);
    }
  }

  Stream<SurveyState> _mapListQuestion(int categoryId) async* {
    yield initialState;
    try {
      var result = await _userRepository.getListQuestion(categoryId);
      if (result.content.status == 200) {
        if (result.content.data != null && result.content.data.length > 0) {
          yield GetListQuestionSuccessState(result.content.data);
        } else {
          yield EmptyListQuestionState();
        }
      } else {
        yield ErrorListQuestionState(result.content.message);
      }
    } catch (e) {
      yield ErrorListQuestionState("Error get list question!");
    }
  }

  Stream<SurveyState> _mapListAnswerResult(
      List<AnswerResult> listResult) async* {
    yield ShowLoadingDialogState();
    try {
      var result = await _userRepository.postResult(listResult);
      if (result.content.status == 200) {
        yield PostResultSuccessState(result.content.data);
      } else {
        yield PostResultFailState(result.content.message);
      }
    } catch (e) {
      yield PostResultFailState("Error post result!");
    }
  }

  Stream<SurveyState> _mapBookmarQuestion(int questionId) async*{
    yield ShowLoadingDialogState();
    try {
      var result = await _userRepository.postBookmark(questionId);
      if (result.content.status == 200) {
        yield BookmarkSuccessState(result.content.message);
      } else {
        yield PostResultFailState(result.content.message);
      }
    } catch (e) {
      yield PostResultFailState("Error post result!");
    }
  }
}
