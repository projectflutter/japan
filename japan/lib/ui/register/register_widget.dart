import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:learn_japanese/data/user_repository.dart';
import 'package:learn_japanese/ui/register/register_event.dart';
import 'package:learn_japanese/utils/uidata.dart';
import 'package:learn_japanese/utils/validators.dart';

import 'register_bloc.dart';
import 'register_provider.dart';
import 'register_screen.dart';
import 'register_state.dart';

class RegisterWidget extends StatefulWidget {
  final UserRepository _userRepository;

  RegisterWidget({Key key, @required UserRepository userRepository})
      : assert(userRepository != null),
        _userRepository = userRepository,
        super(key: key);
  _RegisterWidgetState createState() => _RegisterWidgetState();
}

class _RegisterWidgetState extends State<RegisterWidget> {
  RegisterBloc _registerBloc;

  UserRepository get _userRepository => widget._userRepository;

  final _emailController = TextEditingController();
  final _usernameController = TextEditingController();
  final _passwordController = TextEditingController();
  final _verifyPasswordController = TextEditingController();

  @override
  void initState() {
    // TODO: implement initState
    super.initState();
    _registerBloc = BlocProvider.of<RegisterBloc>(context);
  }

  @override
  Widget build(BuildContext context) {
    Widget toolbar() {
      return InkWell(
        splashColor: Colors.transparent,
        highlightColor: Colors.transparent,
        onTap: () {
          Navigator.of(context).pop();
        },
        child: Container(
          child: Align(
            alignment: Alignment.centerLeft,
            child: Icon(Icons.arrow_back_ios),
          ),
        ),
      );
    }

    final logo = Center(
      child: Text(
        "Đăng kí",
        style: TextStyle(fontSize: 22.0, color: UIData.colorPrimary()),
      ),
    );

    final email = TextFormField(
      controller: _emailController,
      keyboardType: TextInputType.emailAddress,
      autofocus: false,
      decoration: InputDecoration(
          filled: true,
          fillColor: Colors.white,
          prefixIcon: Icon(
            Icons.email,
            color: UIData.colorPrimary(),
          ),
          hintText: 'Email',
          hintStyle: TextStyle(color: UIData.colorPrimary()),
          contentPadding: EdgeInsets.fromLTRB(20.0, 10.0, 20.0, 10.0),
          border: OutlineInputBorder(
            borderRadius: BorderRadius.circular(32.0),
          ),
          focusedBorder: OutlineInputBorder(
              borderRadius: BorderRadius.circular(32.0),
              borderSide: BorderSide(color: UIData.colorPrimary()))),
    );
    final account = TextFormField(
      controller: _usernameController,
      keyboardType: TextInputType.emailAddress,
      autofocus: false,
      decoration: InputDecoration(
          filled: true,
          fillColor: Colors.white,
          prefixIcon: Icon(
            Icons.person,
            color: UIData.colorPrimary(),
          ),
          hintText: 'Tài khoản',
          hintStyle: TextStyle(color: UIData.colorPrimary()),
          contentPadding: EdgeInsets.fromLTRB(20.0, 10.0, 20.0, 10.0),
          border: OutlineInputBorder(
            borderRadius: BorderRadius.circular(32.0),
          ),
          focusedBorder: OutlineInputBorder(
              borderRadius: BorderRadius.circular(32.0),
              borderSide: BorderSide(color: UIData.colorPrimary()))),
    );

    final password = TextFormField(
      controller: _passwordController,
      autofocus: false,
      obscureText: true,
      decoration: InputDecoration(
          filled: true,
          fillColor: Colors.white,
          prefixIcon: Icon(
            Icons.lock,
            color: UIData.colorPrimary(),
          ),
          hintText: 'Mật khẩu',
          hintStyle: TextStyle(color: UIData.colorPrimary()),
          contentPadding: EdgeInsets.fromLTRB(20.0, 10.0, 20.0, 10.0),
          border: OutlineInputBorder(borderRadius: BorderRadius.circular(32.0)),
          focusedBorder: OutlineInputBorder(
              borderRadius: BorderRadius.circular(32.0),
              borderSide: BorderSide(color: UIData.colorPrimary()))),
    );

    final confirmpassword = TextFormField(
      controller: _verifyPasswordController,
      autofocus: false,
      obscureText: true,
      decoration: InputDecoration(
          filled: true,
          fillColor: Colors.white,
          prefixIcon: Icon(
            Icons.lock,
            color: UIData.colorPrimary(),
          ),
          hintText: 'Xác nhận mật khẩu',
          hintStyle: TextStyle(color: UIData.colorPrimary()),
          contentPadding: EdgeInsets.fromLTRB(20.0, 10.0, 20.0, 10.0),
          border: OutlineInputBorder(borderRadius: BorderRadius.circular(32.0)),
          focusedBorder: OutlineInputBorder(
              borderRadius: BorderRadius.circular(32.0),
              borderSide: BorderSide(color: UIData.colorPrimary()))),
    );

    Widget registerButton(BuildContext context) {
      return Container(
        padding: EdgeInsets.symmetric(vertical: 16.0),
        width: 180,
        child: RaisedButton(
          shape: RoundedRectangleBorder(
            borderRadius: BorderRadius.circular(24),
          ),
          onPressed: () {
            // Navigator.of(context).pushNamed(HomePage.tag);

            //remove all route navigator to home screen
            // Navigator.of(context).pushNamedAndRemoveUntil(
            //     '/home', (Route<dynamic> route) => false);
            valdateForm(context);
          },
          padding: EdgeInsets.all(12),
          color: UIData.colorAccent(),
          child: Text('ĐĂNG KÍ', style: TextStyle(color: Colors.white)),
        ),
      );
    }

    return BlocListener(
      bloc: _registerBloc,
      child: Container(
        child: Padding(
          padding: EdgeInsets.only(top: 24,left:10),
          child: Column(
            // shrinkWrap: false,
            // padding: EdgeInsets.only(left: 24.0, right: 24.0),
            children: <Widget>[
              toolbar(),
              Expanded(
                child: Padding(
                  padding: EdgeInsets.only(top: 0, left: 22, right: 22),
                  child: ListView(
                    children: <Widget>[
                      logo,
                      SizedBox(height: 96.0),
                      email,
                      SizedBox(height: 8.0),
                      account,
                      SizedBox(height: 8.0),
                      password,
                      SizedBox(height: 8.0),
                      confirmpassword,
                      registerButton(context),
                      SizedBox(height: 72.0)
                    ],
                  ),
                ),
              )
            ],
          ),
        ),
      ),
      listener: (BuildContext context, RegisterState state) {
        if (state is RegisterLoading) {
          showDialog(
              context: context,
              barrierDismissible: false,
              builder: (context) => WillPopScope(
                  onWillPop: () {},
                  child: Center(
                    child: CircularProgressIndicator(),
                  )));
        }
        if (state is RegisterSuccess) {
          showDialog(
              barrierDismissible: false,
              context: context,
              builder: (context) => WillPopScope(
                    onWillPop: () {
                      return Future.value(false);
                    },
                    child: AlertDialog(
                      title: Text("Thông báo"),
                      content: Text("Đăng kí thành công!"),
                      actions: <Widget>[
                        FlatButton(
                          child: Text("ok"),
                          onPressed: () {
                            if (Navigator.canPop(context)) {
                              Navigator.pop(context);
                            }
                            Navigator.of(context).pushNamedAndRemoveUntil(
                                '/home', (Route<dynamic> route) => false);
                          },
                        )
                      ],
                    ),
                  ));
        }
        if (state is RegisterFailure) {
          Navigator.pop(context);
          Scaffold.of(context)
            ..hideCurrentSnackBar()
            ..showSnackBar(
              SnackBar(
                content: Row(
                  mainAxisAlignment: MainAxisAlignment.spaceBetween,
                  children: [
                    Container(
                        width: 250.0, child: Text(state.error, maxLines: 1)),
                    Icon(Icons.error)
                  ],
                ),
                backgroundColor: Colors.red,
              ),
            );
        }
      },
    );
  }

  void valdateForm(BuildContext context) {
    String email = _emailController.text;
    String username = _usernameController.text;
    String password = _passwordController.text;
    String verifyPassword = _verifyPasswordController.text;
    if (!(Validators.isValidEmail(email))) {
      showErrorValidate(context, RegisterValidationType.email);
    } else if (!(username?.length >= 6)) {
      showErrorValidate(context, RegisterValidationType.username);
    } else if (!(password?.length >= 6)) {
      showErrorValidate(context, RegisterValidationType.password);
    } else if (!(verifyPassword?.length >= 6)) {
      showErrorValidate(context, RegisterValidationType.validatepassword);
    } else if (!(password == verifyPassword)) {
      showErrorValidate(context, RegisterValidationType.math);
    } else {
      print("submit");
      _registerBloc.dispatch(SubmitRegisterEvent(
          email: email,
          username: username,
          password: password,
          verifyPassword: verifyPassword));
    }
  }

  void showErrorValidate(BuildContext context, RegisterValidationType type) {
    print(type);
    RegisterProvider.of(context).validationErrorCallback(type);
  }
}
