import 'package:flutter/material.dart';

class RegisterProvider extends InheritedWidget {
  final Function validationErrorCallback;
  final Widget child;

  RegisterProvider({this.validationErrorCallback, this.child})
      : super(child: child);

  static RegisterProvider of(BuildContext context) =>
      context.inheritFromWidgetOfExactType(RegisterProvider);

  @override
  bool updateShouldNotify(RegisterProvider oldWidget) =>
      validationErrorCallback != oldWidget.validationErrorCallback;
}
