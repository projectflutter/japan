import 'package:equatable/equatable.dart';
import 'package:meta/meta.dart';

@immutable
abstract class RegisterEvent extends Equatable {
  RegisterEvent([List props = const []]) : super(props);
}

class SubmitRegisterEvent extends RegisterEvent {
  String email;
  String username;
  String password;
  String verifyPassword;
  SubmitRegisterEvent(
      {@required this.email,
      @required this.username,
      @required this.password,
      @required this.verifyPassword})
      : super([email, password]);

  @override
  String toString() => 'LoadRegisterEvent';
}
