import 'dart:async';
import 'package:bloc/bloc.dart';
import 'package:learn_japanese/data/preferences.dart';
import 'package:learn_japanese/data/sharepref_name.dart';
import 'package:learn_japanese/data/user_repository.dart';

import 'register_event.dart';
import 'register_state.dart';

class RegisterBloc extends Bloc<RegisterEvent, RegisterState> {
  UserRepository _userRepository;
  RegisterBloc({UserRepository userRepository})
      : _userRepository = userRepository;

  RegisterState get initialState => new RegisterInitial();

  @override
  Stream<RegisterState> mapEventToState(
    RegisterEvent event,
  ) async* {
    if (event is SubmitRegisterEvent) {
      try {
        yield RegisterLoading();
        var result = await _userRepository.register(
            event.email, event.username, event.password, event.verifyPassword);
        print(result.content.token);
        if (result.content.status == 200) {
          await Preference.load();
          Preference.setString(SharePrefName.TOKEN, result.content.token);
          yield RegisterSuccess();
        } else {
          yield RegisterFailure(error: result.content.message);
        }
      } catch (_, stackTrace) {
        print('$_ $stackTrace');
        yield RegisterFailure(error: "$stackTrace");
      }
    }
  }
}
