import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:learn_japanese/data/user_repository.dart';
import 'package:learn_japanese/ui/register/register_widget.dart';
import 'package:learn_japanese/utils/uidata.dart';
import 'package:learn_japanese/utils/validators.dart';

import 'register_bloc.dart';
import 'register_provider.dart';

enum RegisterValidationType {
  email,
  username,
  password,
  validatepassword,
  math
}

class RegisterScreen extends StatefulWidget {
  @override
  RegisterScreenState createState() {
    return new RegisterScreenState();
  }
}

class RegisterScreenState extends State<RegisterScreen> {
  final scaffoldState = GlobalKey<ScaffoldState>();
  RegisterBloc _registerBloc;
  UserRepository _userRepository = UserRepository();
  @override
  void initState() {
    super.initState();
    _registerBloc = RegisterBloc(
      userRepository: _userRepository,
    );
  }

  @override
  void dispose() {
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    String getType(RegisterValidationType type) {
      switch (type) {
        case RegisterValidationType.email:
          return UIData.enter_valid_email;
        case RegisterValidationType.username:
          return UIData.enter_valid_usename;
        case RegisterValidationType.password:
          return UIData.enter_valid_password;
        case RegisterValidationType.validatepassword:
          return UIData.enter_valid_verifypassword;
        case RegisterValidationType.math:
          return UIData.math;
        default:
      }
    }

    showValidationError(RegisterValidationType type) {
      print(getType(type));
      scaffoldState.currentState.showSnackBar(SnackBar(
        content: Text(getType(type)),
        duration: Duration(seconds: 2),
      ));
    }

    return RegisterProvider(
      validationErrorCallback: showValidationError,
      child: Scaffold(
          key: scaffoldState,
          backgroundColor: Color(0xffF4F6F9),
          body: BlocProvider<RegisterBloc>(
              bloc: _registerBloc,
              child: RegisterWidget(
                userRepository: _userRepository,
              ))),
    );
  }
}
