import 'dart:async';
import 'package:bloc/bloc.dart';
import 'package:learn_japanese/authentication_bloc/authentication_event.dart';
import 'package:learn_japanese/authentication_bloc/authentication_state.dart';
import 'package:learn_japanese/data/user_repository.dart';
import 'package:meta/meta.dart';

class AuthenticationBloc
    extends Bloc<AuthenticationEvent, AuthenticationState> {
  final UserRepository _userRepository;

  AuthenticationBloc({@required UserRepository userRepository})
      : assert(userRepository != null),
        _userRepository = userRepository;

  AuthenticationState get initialState => new Uninitialized();

  @override
  Stream<AuthenticationState> mapEventToState(
    AuthenticationEvent event,
  ) async* {
    try {
      if (event is AppStarted) {
        final isSignedIn = await _userRepository.isSignedIn();
        if (isSignedIn) {
          // final name = await _userRepository.isSignedIn();
          yield Authenticated("username");
        } else {
          await Future.delayed(Duration(seconds: 2));
          yield Unauthenticated();
        }
      } else if (event is LoggedIn) {
        yield Authenticated("username");
      }
    } catch (_, stackTrace) {
      print('$_ $stackTrace');
      yield Unauthenticated();
    }
  }
}
