import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:learn_japanese/authentication_bloc/authentication_state.dart';
import 'package:learn_japanese/ui/changepassword/change_pass_bloc.dart';
import 'package:learn_japanese/ui/changepassword/change_pass_screen.dart';
import 'package:learn_japanese/ui/home/home_screen.dart';
import 'package:learn_japanese/ui/intro/%08loading_screen.dart';
import 'package:learn_japanese/ui/login/login_screen.dart';
import 'package:learn_japanese/ui/view_result/view_result_page.dart';
import 'package:learn_japanese/utils/uidata.dart';

import 'authentication_bloc/authentication_bloc.dart';
import 'authentication_bloc/authentication_event.dart';
import 'data/user_repository.dart';
import 'di/dependency_injection.dart';
import 'ui/splash_screen.dart';

void main() {
  Injector.configure(Flavor.PRO);
  runApp(App());
}

class App extends StatefulWidget {
  _AppState createState() => _AppState();
}

class _AppState extends State<App> {
  final UserRepository _userRepository = UserRepository();
  AuthenticationBloc _authenticationBloc;

  @override
  void initState() {
    SystemChrome.setSystemUIOverlayStyle(SystemUiOverlayStyle.dark.copyWith(
      statusBarColor: Colors.transparent, //or set color with: Color(0xFF0000FF)
    ));
    super.initState();
    _authenticationBloc = AuthenticationBloc(userRepository: _userRepository);
    _authenticationBloc.dispatch(AppStarted());
    SystemChrome.setPreferredOrientations([
    DeviceOrientation.portraitUp,
    DeviceOrientation.portraitDown,
  ]);
  }

  @override
  Widget build(BuildContext context) {
    return BlocProvider(
      bloc: _authenticationBloc,
      child: MaterialApp( 
        theme: ThemeData(
          primaryColor: UIData.colorAccent(),
          accentColor: UIData.colorAccent(),
          brightness: Brightness.light,
        ),
        routes: <String, WidgetBuilder>{
          "/home": (BuildContext context) => new HomeScreen(),
          "/login": (BuildContext context) => new LoginScreen(userRepository: _userRepository,),
          "/changepassword": (BuildContext context) => new ChangePassScreen(changePassBloc:ChangePassBloc(),),
          "/viewresult": (BuildContext context) => new ViewResultPage(),
        },
        home: BlocBuilder(
          bloc: _authenticationBloc,
          builder: (BuildContext context, AuthenticationState state) {
            if (state is Uninitialized) {
              return SplashScreen();
            }
            if (state is Unauthenticated) {
              return LoadingScreen();
            }
             if (state is Authenticated) {
              return HomeScreen();
            }
          },
        ),
      ),
    );
  }

  @override
  void dispose() {
    _authenticationBloc.dispose();
    super.dispose();
  }
}
