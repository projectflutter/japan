import 'dart:ui';

import 'package:learn_japanese/data/model/school_menu.dart';
import 'package:learn_japanese/utils/uidata.dart';

class SchoolViewModel {
  List<SchoolMenu> menuItems;

  SchoolViewModel({this.menuItems});

  // getMenuItems() {
  //   return menuItems = <SchoolMenu>[
  //     SchoolMenu(
  //         id: 1,
  //         title: "Cấp bậc N1",
  //         titleDetail: "N1",
  //         menuColor: Color(0xffDCD8E1),
  //         image: UIData.level1),
  //     SchoolMenu(
  //         id: 2,
  //         title: "Cấp bậc N2",
  //         titleDetail: "N2",
  //         menuColor: Color(0xffDCD8E1),
  //         image: UIData.level2),
  //     SchoolMenu(
  //         id: 3,
  //         title: "Cấp bậc N3",
  //         titleDetail: "N3",
  //         menuColor: Color(0xffDCD8E1),
  //         image: UIData.level3),
  //     SchoolMenu(
  //         id: 4,
  //         title: "Cấp bậc N4",
  //         titleDetail: "N4",
  //         menuColor: Color(0xffDCD8E1),
  //         image: UIData.level4),
  //     SchoolMenu(
  //         id: 5,
  //         title: "Cấp bậc N5",
  //         titleDetail: "N5",
  //         menuColor: Color(0xffDCD8E1),
  //         image: UIData.level5)
  //   ];
  // }
}
