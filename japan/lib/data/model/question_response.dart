class QuestionResponse {
  int status;
  String message;
  List<DataQuestion> data;

  QuestionResponse({this.status, this.message, this.data});

  QuestionResponse.fromJson(Map<String, dynamic> json) {
    status = json['status'];
    message = json['message'];
    if (json['data'] != null) {
      data = new List<DataQuestion>();
      json['data'].forEach((v) {
        data.add(new DataQuestion.fromJson(v));
      });
    }
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['status'] = this.status;
    data['message'] = this.message;
    if (this.data != null) {
      data['data'] = this.data.map((v) => v.toJson()).toList();
    }
    return data;
  }
}

class DataQuestion {
  int id;
  int schoolId;
  int categoryId;
  String questionTitle;
  String questionExplain;
  int questionSort;
  String createdAt;
  List<AnswersQuestion> answers;

  DataQuestion(
      {this.id,
      this.schoolId,
      this.categoryId,
      this.questionTitle,
      this.questionExplain,
      this.questionSort,
      this.createdAt,
      this.answers});

  DataQuestion.fromJson(Map<String, dynamic> json) {
    id = json['id'];
    schoolId = json['school_id'];
    categoryId = json['category_id'];
    questionTitle = json['question_title'];
    questionExplain = json['question_explain'];
    questionSort = json['question_sort'];
    createdAt = json['created_at'];
    if (json['answers'] != null) {
      answers = new List<AnswersQuestion>();
      json['answers'].forEach((v) {
        answers.add(new AnswersQuestion.fromJson(v));
      });
    }
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['id'] = this.id;
    data['school_id'] = this.schoolId;
    data['category_id'] = this.categoryId;
    data['question_title'] = this.questionTitle;
    data['question_explain'] = this.questionExplain;
    data['question_sort'] = this.questionSort;
    data['created_at'] = this.createdAt;
    if (this.answers != null) {
      data['answers'] = this.answers.map((v) => v.toJson()).toList();
    }
    return data;
  }
}

class AnswersQuestion {
  int id;
  int questionId;
  String answerTitle;
  int isCorrected;
  int answerSort;

  AnswersQuestion(
      {this.id,
      this.questionId,
      this.answerTitle,
      this.isCorrected,
      this.answerSort});

  AnswersQuestion.fromJson(Map<String, dynamic> json) {
    id = json['id'];
    questionId = json['question_id'];
    answerTitle = json['answer_title'];
    isCorrected = json['is_corrected'];
    answerSort = json['answer_sort'];
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['id'] = this.id;
    data['question_id'] = this.questionId;
    data['answer_title'] = this.answerTitle;
    data['is_corrected'] = this.isCorrected;
    data['answer_sort'] = this.answerSort;
    return data;
  }
}