class DetailResponse {
  int status;
  String message;
  List<DataDetail> data;

  DetailResponse({this.status, this.message, this.data});

  DetailResponse.fromJson(Map<String, dynamic> json) {
    status = json['status'];
    message = json['message'];
    if (json['data'] != null) {
      data = new List<DataDetail>();
      json['data'].forEach((v) {
        data.add(new DataDetail.fromJson(v));
      });
    }
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['status'] = this.status;
    data['message'] = this.message;
    if (this.data != null) {
      data['data'] = this.data.map((v) => v.toJson()).toList();
    }
    return data;
  }
}

class DataDetail {
  int id;
  int schoolId;
  String categoryName;
  String categoryDescription;
  int categorySort;

  DataDetail(
      {this.id,
      this.schoolId,
      this.categoryName,
      this.categoryDescription,
      this.categorySort});

  DataDetail.fromJson(Map<String, dynamic> json) {
    id = json['id'];
    schoolId = json['school_id'];
    categoryName = json['category_name'];
    categoryDescription = json['category_description'];
    categorySort = json['category_sort'];
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['id'] = this.id;
    data['school_id'] = this.schoolId;
    data['category_name'] = this.categoryName;
    data['category_description'] = this.categoryDescription;
    data['category_sort'] = this.categorySort;
    return data;
  }
}