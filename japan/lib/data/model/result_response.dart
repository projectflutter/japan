class ResultResponse {
  int status;
  String message;
  List<DataQuestionResult> data;

  ResultResponse({this.status, this.message, this.data});

  ResultResponse.fromJson(Map<String, dynamic> json) {
    status = json['status'];
    message = json['message'];
    if (json['data'] != null) {
      data = new List<DataQuestionResult>();
      json['data'].forEach((v) {
        data.add(new DataQuestionResult.fromJson(v));
      });
    }
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['status'] = this.status;
    data['message'] = this.message;
    if (this.data != null) {
      data['data'] = this.data.map((v) => v.toJson()).toList();
    }
    return data;
  }
}

class DataQuestionResult {
  int id;
  int schoolId;
  int categoryId;
  String questionTitle;
  String questionExplain;
  int questionSort;
  String createdAt;
  String categoryName;
  int answerId;
  List<Answers> answers;

  DataQuestionResult(
      {this.id,
      this.schoolId,
      this.categoryId,
      this.questionTitle,
      this.questionExplain,
      this.questionSort,
      this.createdAt,
      this.categoryName,
      this.answerId,
      this.answers});

  DataQuestionResult.fromJson(Map<String, dynamic> json) {
    id = json['id'];
    schoolId = json['school_id'];
    categoryId = json['category_id'];
    questionTitle = json['question_title'];
    questionExplain = json['question_explain'];
    questionSort = json['question_sort'];
    createdAt = json['created_at'];
    categoryName = json['category_name'];
    answerId = json['answer_id'];
    if (json['answers'] != null) {
      answers = new List<Answers>();
      json['answers'].forEach((v) {
        answers.add(new Answers.fromJson(v));
      });
    }
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['id'] = this.id;
    data['school_id'] = this.schoolId;
    data['category_id'] = this.categoryId;
    data['question_title'] = this.questionTitle;
    data['question_explain'] = this.questionExplain;
    data['question_sort'] = this.questionSort;
    data['created_at'] = this.createdAt;
    data['category_name'] = this.categoryName;
    data['answer_id'] = this.answerId;
    if (this.answers != null) {
      data['answers'] = this.answers.map((v) => v.toJson()).toList();
    }
    return data;
  }
}

class Answers {
  int id;
  int questionId;
  String answerTitle;
  int isCorrected;
  int answerSort;

  Answers(
      {this.id,
      this.questionId,
      this.answerTitle,
      this.isCorrected,
      this.answerSort});

  Answers.fromJson(Map<String, dynamic> json) {
    id = json['id'];
    questionId = json['question_id'];
    answerTitle = json['answer_title'];
    isCorrected = json['is_corrected'];
    answerSort = json['answer_sort'];
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['id'] = this.id;
    data['question_id'] = this.questionId;
    data['answer_title'] = this.answerTitle;
    data['is_corrected'] = this.isCorrected;
    data['answer_sort'] = this.answerSort;
    return data;
  }
}