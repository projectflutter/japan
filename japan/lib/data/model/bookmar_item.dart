class BookmarkItem {
  int status;
  String message;
  List<DataBookmark> data;

  BookmarkItem({this.status, this.message, this.data});

  BookmarkItem.fromJson(Map<String, dynamic> json) {
    status = json['status'];
    message = json['message'];
    if (json['data'] != null) {
      data = new List<DataBookmark>();
      json['data'].forEach((v) {
        data.add(new DataBookmark.fromJson(v));
      });
    }
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['status'] = this.status;
    data['message'] = this.message;
    if (this.data != null) {
      data['data'] = this.data.map((v) => v.toJson()).toList();
    }
    return data;
  }
}

class DataBookmark {
  int bookmarkId;
  int id;
  int schoolId;
  int categoryId;
  String questionTitle;
  String questionExplain;
  int questionSort;
  String createdAt;

  DataBookmark(
      {this.bookmarkId,
      this.id,
      this.schoolId,
      this.categoryId,
      this.questionTitle,
      this.questionExplain,
      this.questionSort,
      this.createdAt});

  DataBookmark.fromJson(Map<String, dynamic> json) {
    bookmarkId = json['bookmark_id'];
    id = json['id'];
    schoolId = json['school_id'];
    categoryId = json['category_id'];
    questionTitle = json['question_title'];
    questionExplain = json['question_explain'];
    questionSort = json['question_sort'];
    createdAt = json['created_at'];
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['bookmark_id'] = this.bookmarkId;
    data['id'] = this.id;
    data['school_id'] = this.schoolId;
    data['category_id'] = this.categoryId;
    data['question_title'] = this.questionTitle;
    data['question_explain'] = this.questionExplain;
    data['question_sort'] = this.questionSort;
    data['created_at'] = this.createdAt;
    return data;
  }
}
