class SchoolMenu {
  int status;
  String message;
  List<Data> data;

  SchoolMenu({this.status, this.message, this.data});

  SchoolMenu.fromJson(Map<String, dynamic> json) {
    status = json['status'];
    message = json['message'];
    if (json['data'] != null) {
      data = new List<Data>();
      json['data'].forEach((v) {
        data.add(new Data.fromJson(v));
      });
    }
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['status'] = this.status;
    data['message'] = this.message;
    if (this.data != null) {
      data['data'] = this.data.map((v) => v.toJson()).toList();
    }
    return data;
  }
}

class Data {
  int id;
  String schoolTitle;
  String schoolTitleDetail;
  String schoolMenuColor;
  String schoolImage;
  int schoolSort;

  Data(
      {this.id,
      this.schoolTitle,
      this.schoolTitleDetail,
      this.schoolMenuColor,
      this.schoolImage,
      this.schoolSort});

  Data.fromJson(Map<String, dynamic> json) {
    id = json['id'];
    schoolTitle = json['school_title'];
    schoolTitleDetail = json['school_title_detail'];
    schoolMenuColor = json['school_menu_color'];
    schoolImage = json['school_image'];
    schoolSort = json['school_sort'];
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['id'] = this.id;
    data['school_title'] = this.schoolTitle;
    data['school_title_detail'] = this.schoolTitleDetail;
    data['school_menu_color'] = this.schoolMenuColor;
    data['school_image'] = this.schoolImage;
    data['school_sort'] = this.schoolSort;
    return data;
  }
}
