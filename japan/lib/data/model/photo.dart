class Photo {

  int albumId;
  int id;
  String title;
  String url;
  String thumbnailUrl;

  Photo(
      {this.albumId, this.id, this.title, this.url, this.thumbnailUrl});

  factory Photo.fromJson(Map<String, dynamic> json) =>
      new Photo(
          albumId: json['albumId'],
          id: json['id'],
          title: json['title'],
          url: json['url'],
          thumbnailUrl: json['thumbnailUrl']);

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['albumId'] = this.albumId;
    data['id'] = this.id;
    data['title'] = this.title;
    data['url'] = this.url;
    data['thumbnailUrl'] = this.thumbnailUrl;
    return data;
  }

static List encondeToJson(List<Photo>list){
    List jsonList = List();
    list.map((item)=>
      jsonList.add(item.toJson())
    ).toList();
    return jsonList;
}
}