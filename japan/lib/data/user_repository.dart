import 'dart:async';

import 'package:learn_japanese/data/model/bookmark_response.dart';
import 'package:learn_japanese/data/model/change_pass_response.dart';
import 'package:learn_japanese/data/model/detail_response.dart';
import 'package:learn_japanese/data/model/question_response.dart';
import 'package:learn_japanese/data/model/result_response.dart';
import 'package:learn_japanese/data/model/school_menu.dart';
import 'package:learn_japanese/data/preferences.dart';
import 'package:learn_japanese/data/sharepref_name.dart';
import 'package:learn_japanese/di/dependency_injection.dart';
import 'package:learn_japanese/service/api_network_provider.dart';
import 'package:learn_japanese/service/network_service_response.dart';
import 'package:learn_japanese/ui/home/bookmarkpage/detailbookmark/detail_bookmark_model.dart';
import 'package:learn_japanese/ui/survey/answer_result.dart';
import 'package:learn_japanese/ui/survey/answer_result_response.dart';

import 'model/bookmar_item.dart';
import 'model/login_response.dart';

class UserRepository {
  final _apiNetworkProvider = Injector().apiService;
  Future<NetworkServiceResponse<LoginResponse>> login(username, password) =>
      _apiNetworkProvider.login(username, password);

  Future<bool> isSignedIn() async {
    await Preference.load();
    final currentUser = Preference.getString(SharePrefName.TOKEN);
    return currentUser != null;
  }

  Future<NetworkServiceResponse<LoginResponse>> register(String email,
          String username, String password, String verifyPassword) =>
      _apiNetworkProvider.register(email, username, password, verifyPassword);

  Future<NetworkServiceResponse<SchoolMenu>> getMenuItems() =>
      _apiNetworkProvider.getMenuItems();

  Future<NetworkServiceResponse<DetailResponse>> getdetail(int id) =>
      _apiNetworkProvider.getdetail(id);

  Future<NetworkServiceResponse<BookmarkItem>> getBookmarkItem(int id) =>
      _apiNetworkProvider.getBookmarkItem(id);

  Future<NetworkServiceResponse<ChangePassResponse>> changePass(
          String oldPass, String password, String verifyPassword) =>
      _apiNetworkProvider.changePass(oldPass, password, verifyPassword);

  Future<NetworkServiceResponse<QuestionResponse>> getListQuestion(
          int categoryId) =>
      _apiNetworkProvider.getListQuestion(categoryId);

  Future<NetworkServiceResponse<AnswerResultResponse>> postResult(
          List<AnswerResult> listResult) =>
      _apiNetworkProvider.postResult(listResult);

  Future<NetworkServiceResponse<ResultResponse>> getResult(int id) =>
      _apiNetworkProvider.getResult(id);
  Future<NetworkServiceResponse<BookmarkResponse>> postBookmark(int id) =>
      _apiNetworkProvider.postBookmark(id);
  Future<NetworkServiceResponse<DetailBookmarkResponse>> getDetailBookmark(
          int id) =>
      _apiNetworkProvider.getDetailBookmark(id);

      Future<NetworkServiceResponse<ChangePassResponse>> resetPass(
          String email) =>
      _apiNetworkProvider.resetPass(email);

}
